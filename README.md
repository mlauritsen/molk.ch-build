# molk.ch-build Project

molk.ch-build generates the molk.ch site from the ../molk.ch source files.

# Setup

See [documentation/git-setup.md]

- Location: /home/mlauritsen/projects/current/molk.ch-build
- Preconditions: MOLK_SRC, MOLK_TMP and MOLK_DEST must be set!
- Documentation:
 - documentation/user-guide.txt
 - documentation/developer-guide.txt

The build tests ensure that everything is working.
The build contains two types of tests:
- util tests: "Unit" tests for each utility script
- build tests: "Integration" tests for each phase of the site build

Run all tests ("." is the root of the molk.ch-build project):
  ./$ ./build.sh build-test
Run one test:
  ./utils/test$ rm -rf tmp/; time ./core-utils-test.sh
  ./test$ rm -rf tmp/; time ./setup-test.sh
Run utils from commandline:
  Set env vars from makefile (run from /molk.ch-build):
export MOLK_BUILD=$(pwd)
export MOLK_UTILS=${MOLK_BUILD}/utils
export MOLK_SRC_FRAGMENTS=${MOLK_SRC}/fragments
export MOLK_TMP_FRAGMENTS=${MOLK_TMP}/fragments
export MOLK_CONTENT=${MOLK_SRC}/content
export MOLK_LIBRARIES=${MOLK_BUILD}/libraries
export MOLK_TRANSFORM_PAGE=${MOLK_TMP}/transform-page
export MOLK_DOMAIN=molk.ch
export MOLK_URL=http://${MOLK_DOMAIN}
export MOLK_ONLINE=$(${MOLK_UTILS}/is-online.sh ${MOLK_URL})

# Requirements

- bash: All scripts (see **/*.sh)
- make: Controls the build (see build.sh and makefile)
- git: Source version control (see utils/is-git-clean.sh)
- xmlstarlet 1.5.0: XML (mainly XHTML) Validation, editing and querying (see utils/check-html.sh, utils/xml-select.sh, utils/update-xml.sh, utils/delete-xml.sh)
- curl 7.35.0: HTTP file retrieval (see utils/check-feed-online.sh)
- rhino 1.7R4: URL encoding (see utils/url-encode.sh)
- tidy 20091223cvs: Formatting and minifying HTML, CSS and XML (see utils/format-html.sh, utils/format-feed.sh, etc.)
- webcheck 1.10.4: Validating web pages (see utils/check-web.sh)
- csstidy 1.4-3: Formatting and minifying CSS files
 - sudo apt-get install csstidy
- pngcrush 1.7.65-0.1: Minifying images (see utils/minify-png.sh)
-  python3 Local server to browse with a proper "served from a server" experience.
- lynx 2.8.8pre4-1: Checking plaintext site accessibility (see .../molk.ch/src/readme.txt)
- sitecopy 1:0.16.6-7: Online publishing of the generated site (see utils/publish.sh)
- emacs 24.5.1: JavaScript source formatting
- phantomjs 2.1.1+dfsg-1: Running JavaScript tests headless
- libimage-exiftool-perl 10.10: Checking and cleaning up image metadata (EXIF) using exiftool
(npm 3.5.2-0ubuntu4)
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
nodejs 9.4.0-1nodesource1
sudo npm install http-server -g
npm module "http-server" used for more efficient (than python simple http server) web serving of directories
- time 1.7-25.1+b1: molk uses /usr/bin/time, not the bash builtin
 - sudo apt install time
- parallel 20161222-1-all: run a command on a list of arguments (similar to xargs) in parallel

# Libraries

molk.ch-build uses the following libraries, which must be downloaded separately (see the documentation):

- libraries/feedvalidator:          Used in utils/check-feed-offline.sh (see documentation/feedvalidator-readme.txt)
- libraries/cssembed:               Used in utils/css-embed-images.sh (see documentation/cssembed-readme.txt)
- libraries/css-validator:          Used in utils/check-css.sh (see documentation/css-validator-readme.txt)
- libraries/jslint:                 Used in utils/check-js.sh (see documentation/jslint-readme.txt)
- libraries/yuicompressor:          Used in utils/minify-js.sh (see documentation/yuicompressor-readme.txt)
- libraries/qunit-phantomjs-runner: Used in utils/test-js.sh (see documentation/qunit-phantomjs-runner-readme.txt)
- libraries/xml-validation:         Used in utils/check-html.sh and tests (see documentation/xml-validation-readme.txt)
