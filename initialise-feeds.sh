#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# feeds are defined in feed.xml files, and contain entries for all the pages below them in the site tree
# the sitemap is a special feed, in HTML format, which contains a list of all pages

feeds=($(cd ${MOLK_CONTENT} && find . -type f -name "feed.xml"))

# validation
if test ${#feeds[*]} == 0; then
  echo "No feeds found in ${MOLK_CONTENT}"
  exit 0
fi

echo -n "Initialising feeds"

# iterate over the feeds
for feed in "${feeds[@]}"; do
  # if the filename starts with "./", remove it
  feed=${feed#./}

  # ignore feeds containing MOLK-TODO
  if ! grep -q MOLK-TODO ${MOLK_CONTENT}/${feed}; then

    # transform feeds containing "<?molk "
    if grep -q "<?molk " ${MOLK_CONTENT}/${feed}; then
      if ! ${MOLK_UTILS}/initialise-feed.sh ${MOLK_CONTENT}/${feed}; then
        echo -e "\n\"${feed}\": Error while transforming" >&2
        exit 42
      fi
    else
      # simply copy feeds which do not contain "<?molk ",
      cp "${MOLK_CONTENT}/${feed}" "${MOLK_DEST}/${feed}"
    fi
    # feed was handled succesfully
    echo -n "."
  else
    # feed was ignored (ignored feeds are listed by name in ()'s)
    echo -n "(${feed})"
  fi
done

# draw the site map
