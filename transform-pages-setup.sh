#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

mkdir -p ${MOLK_TMP_FRAGMENTS}

fragments=($(cd ${MOLK_SRC_FRAGMENTS} && ls --format=single-column *.html 2> /dev/null | grep -v head.html | cut -d. -f-1))

# validation
if test ${#fragments[*]} -eq 0; then
  echo -e "\nNo fragments found in ${MOLK_SRC_FRAGMENTS}" >&2
  exit 42
fi

echo -n "Preparing fragments"

# Extract the HTML fragment bodies
for fragment in "${fragments[@]}"; do
  echo -n "."
  if ! ${MOLK_UTILS}/xml-select.sh ${MOLK_SRC_FRAGMENTS}/${fragment}.html copy xhtml "//xhtml:html/xhtml:body/node()" > ${MOLK_TMP_FRAGMENTS}/${fragment}.xml; then
    echo -e "\nInvalid fragment: ${MOLK_SRC_FRAGMENTS}/${fragment}.html" >&2
    exit 42
  fi
done

# Special treatment for the head.html fragment: Extract the HTML/HEAD contents
if ! ${MOLK_UTILS}/xml-select.sh ${MOLK_SRC_FRAGMENTS}/head.html copy xhtml "//xhtml:html/xhtml:head/node()" > ${MOLK_TMP_FRAGMENTS}/head.xml; then
  echo -e "\nInvalid fragment: ${MOLK_SRC_FRAGMENTS}/head.html" >&2
  exit 42
fi
if ! sed "/^[[:space:]]*<title>.*<\/title>[[:space:]]*$/d" --in-place ${MOLK_TMP_FRAGMENTS}/head.xml; then
  echo -e "\nCould not remove head title element from \"${MOLK_TMP_FRAGMENTS}/head.xml\"" >&2
  exit 42
fi
echo -n "."

# Generate subnav fragments
subnav_fragments=($(ls ${MOLK_CONTENT}))
if test ${#subnav_fragments[@]} -gt 0; then
  for file in "${subnav_fragments[@]}"; do
    if test -d "${MOLK_CONTENT}/${file}"; then
      ${MOLK_UTILS}/create-subnav.sh ${file} > ${MOLK_TMP_FRAGMENTS}/subnav-${file}.xml
      echo -n "."
    fi
  done
fi
