#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# check $MOLK_DEST files
declare -ar html_files=($(find ${MOLK_DEST} -type f -name "*.html"))
declare -ar css_files=($(find ${MOLK_DEST} -type f -name "*.css" | grep -v '/lib/'))
declare -ar js_files=($(find ${MOLK_DEST} -type f -name "*.js" | grep -v '/lib/'))
declare -ar feed_files=($(find ${MOLK_DEST} -type f -name feed.xml))
declare -ar all_files=(${html_files[@]:-} ${css_files[@]:-} ${js_files[@]:-} ${feed_files[@]:-})

# in the unlikely event that no files were found, there is nothing to do
if test "${#all_files[@]}" -eq 0; then
    echo "No destination files to check"
    exit 0
fi
echo -n "Checking ${#all_files[@]} destination files"

# check HTML files
if test ${#html_files[@]} -ge 1; then
    # files must contain a comment
    no_comment=$(grep --files-without-match '<!--' ${html_files[@]})
    if test "${no_comment}" != ""; then
	echo -e "\n${no_comment}: All HTML files should contain a comment" >&2
	exit 42
    fi
    unset no_comment
    echo -n "."

    # files must contain the word 'copyright'
    no_copyright=$(grep --files-without-match -i "copyright" ${html_files[@]})
    if test "${no_copyright}" != ""; then
	echo -e "\n${no_copyright}: All HTML files should contain the word 'copyright'" >&2
	exit 42
    fi
    unset no_copyright
    echo -n "."

    # files must contain the word 'licence'
    no_licence=$(grep --files-without-match -i "licence" ${html_files[@]})
    if test "${no_licence}" != ""; then
	echo -e "\n${no_licence}: All HTML files should contain the word 'licence'" >&2
	exit 42
    fi
    unset no_licence
    echo -n "."

    # accessibility checks
    if ! "${MOLK_UTILS}/check-html-accessibility.sh" ${html_files[@]} >> "${MOLK_LOG}" 2>&1; then
	echo -e "\nHTML accessibility checks failed" >&2
	exit 42
    fi
    echo -n "."

    # standard HTML checks
    if ! "${MOLK_UTILS}/check-html.sh" ${html_files[@]} >> "${MOLK_LOG}" 2>&1; then
	echo -e "\nDestination page validation failed" >&2
	exit 42
    fi
    echo -n "."

    # web checks (links, anchors, total page size...)
    if ! "${MOLK_UTILS}/check-web.sh" ${MOLK_DEST} >> "${MOLK_LOG}" 2>&1; then
	echo -e "\nWeb checks failed" >&2
	exit 42
    fi
    echo -n "."

    # javascript unit tests
    if ! "${MOLK_UTILS}/test-js.sh" ${html_files[@]} >> "${MOLK_LOG}" 2>&1; then
	echo -e "\nJS Unit tests failed" >&2
	exit 42
    fi
    echo -n "."
fi

# check CSS files
if test ${#css_files[@]} -ge 1; then
    # check-css disabled - does not understand the data: protocol...
    #if ! ${MOLK_UTILS}/check-css.sh ${css_files[@]}; then
    #  echo -e "\nCSS validation failed" >&2
    #  exit 42
    #fi
    #echo -n "."

    # files must contain a comment
    no_comment=$(grep --files-without-match '/\*' ${css_files[@]})
    if test "${no_comment}" != ""; then
	echo -e "\n${no_comment}: All CSS files should contain a comment" >&2
	exit 42
    fi
    unset no_comment
    echo -n "."

    # files must contain the word 'copyright'
    no_copyright=$(grep --files-without-match -i "copyright" ${css_files[@]})
    if test "${no_copyright}" != ""; then
	echo -e "\n${no_copyright}: All CSS files should contain the word 'copyright'" >&2
	exit 42
    fi
    unset no_copyright
    echo -n "."

    # files must contain the word 'licence'
    no_licence=$(grep --files-without-match -i "licence" ${css_files[@]})
    if test "${no_licence}" != ""; then
	echo -e "\n${no_licence}: All CSS files should contain the word 'licence'" >&2
	exit 42
    fi
    unset no_licence
    echo -n "."
fi

# check JS files
if test ${#js_files[@]} -ge 1; then
    # files must contain a comment
    no_comment=$(grep --files-without-match '/\*' ${js_files[@]})
    if test "${no_comment}" != ""; then
	echo -e "\n${no_comment}: All JS files should contain a comment" >&2
	exit 42
    fi
    unset no_comment
    echo -n "."

    # files must contain the word 'copyright'
    no_copyright=$(grep --files-without-match -i "copyright" ${js_files[@]})
    if test "${no_copyright}" != ""; then
	echo -e "\n${no_copyright}: All JS files should contain the word 'copyright'" >&2
	exit 42
    fi
    unset no_copyright
    echo -n "."

    # files must contain the word 'licence'
    no_licence=$(grep --files-without-match -i "licence" ${js_files[@]})
    if test "${no_licence}" != ""; then
	echo -e "\n${no_licence}: All JS files should contain the word 'licence'" >&2
	exit 42
    fi
    unset no_licence
    echo -n "."
fi

# check feed files
if test ${#feed_files[@]} -ge 1; then
    # files must contain a <rights> element
    no_rights=$(grep --files-without-match -E '<rights[^>]*>' ${feed_files[@]})
    if test "${no_rights}" != ""; then
	echo -e "\n${no_rights}: All feed files should contain a <rights>-element" >&2
	exit 42
    fi
    unset no_rights
    echo -n "."

    # files must contain the word 'copyright'
    no_copyright=$(grep --files-without-match -i "copyright" ${feed_files[@]})
    if test "${no_copyright}" != ""; then
	echo -e "\n${no_copyright}: All feed files should contain the word 'copyright'" >&2
	exit 42
    fi
    unset no_copyright
    echo -n "."

    # files must contain the word 'licence'
    no_licence=$(grep --files-without-match -i "licence" ${feed_files[@]})
    if test "${no_licence}" != ""; then
	echo -e "\n${no_licence}: All feed files should contain the word 'licence'" >&2
	exit 42
    fi
    unset no_licence
    echo -n "."
fi

# check that there are no .tmp files
tmp_files=$(find "${MOLK_DEST}" -type f -name "*.tmp")
if test "${tmp_files}" != ""; then
    echo -e "\n${tmp_files}: There should be no .tmp files in /dest" >&2
    exit 42
fi
unset tmp_files
echo -n "."

# ensure presence of index.html where appropriate
if ! "${MOLK_UTILS}/detect-index-html.sh" "${MOLK_DEST}" >> "${MOLK_LOG}" 2>&1; then
    echo -e "\nDetected missing indexes" >&2
    exit 42
fi
echo -n "."

# check that www.molk.ch is never mentionned
if test "${#all_files[@]}" -ge 1; then
    www_molk_ch=$(grep -i "www.molk.ch" ${all_files[@]} || true)
    if test "${www_molk_ch}" != ""; then
	echo -e "\nFound references to www.molk.ch:\n${www_molk_ch}:" >&2
	exit 42
    fi
    unset www_molk_ch
    echo -n "."
fi

# check that there are no symlinks
links=$(find "${MOLK_DEST}" -type l)
if test "${links}" != ""; then
    echo -e "\n${links}: There should be no symbolic links in /dest" >&2
    exit 42
fi
echo -n "."
