# molk build management

export MOLK_BUILD = $(realpath .)
export MOLK_BUILD_VERSION = 1.0.0
export MOLK_BUILD_TEST = ${MOLK_BUILD}/test
export MOLK_BUILD_UTILS = ${MOLK_BUILD}/utils
export MOLK_BUILD_UTILS_TEST = ${MOLK_BUILD_UTILS}/test
export MOLK_BUILD_DEP_TEST = ${MOLK_BUILD_UTILS_TEST}/dependencies
export MOLK_BUILD_VERBOSE = true

.PHONY: all clean build-statistics build-check build-run-tests build-package

# do everything
all: build-statistics build-check build-test build-package

# print statistics about the build
build-statistics:
	@./build-statistics.sh

# run the build QA checks
build-check:
	@./build-check.sh

# run all build tests
build-run-tests:
	@cd "${MOLK_BUILD_DEP_TEST}" && ${MOLK_BUILD}/build-run-tests.sh && \
	cd "${MOLK_BUILD_UTILS_TEST}" && ${MOLK_BUILD}/build-run-tests.sh && \
	cd "${MOLK_BUILD_TEST}" && ${MOLK_BUILD}/build-run-tests.sh

# package the build - prerequisite: checks and tests must pass
build-package:
	@./build-package.sh

debug:
	@echo "Variables: ${.VARIABLES}"
	@echo "Build: ${MOLK_BUILD}"
	@echo "Build test: ${MOLK_BUILD_TEST}"
	@echo "Utils: ${MOLK_BUILD_UTILS}"
	@echo "Utils test: ${MOLK_BUILD_UTILS_TEST}"
