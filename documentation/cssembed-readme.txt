Blog: http://www.nczonline.net/blog/2009/11/03/automatic-data-uri-embedding-in-css-files/
Documentation: http://wiki.github.com/nzakas/cssembed
Project: http://github.com/nzakas/cssembed

CSSEmbed is written in Java and requires Java 1.5+ to run. To get started, download the JAR file. All of the dependencies are included in that JAR file, so there is no need to mess around with setting up classpaths or downloading other dependencies. Basic usage is as follows:

java -jar cssembed-x.y.z.jar -o <output filename> <input filename>

For example:

java -jar cssembed-x.y.z.jar -o styles_new.css styles.css

When the -o flag is omitted, the output ends up on stdout, thus you can direct the output to a file directly:

java -jar cssembed-x.y.z.jar styles.css > styles_new.css

Complete usage instructions are available using the -h flag:

Usage: java -jar cssembed-x.y.z.jar [options] [input file]

Global Options
   -h, --help            Displays this information.
   --charset             Character set of the input file.
   -v, --verbose         Display informational messages and warnings.
   -root                 Prepends  to all relative URLs.
   -o                    Place the output into . Defaults to stdout.

CSSEmbed is smart in the way it identifies images. If the image location begins with -Y´http://¡, then the tool automatically downloads the file and converts it to a data URI. If the image location is a relative path (i.e., contains ´../¡), CSSEmbed looks for the file locally in relation to the style sheet file’s location. If the files is an absolute path without -Y΄http://‘ specified, such as ΄/images/image.png‘, you’ll need to provide a root via the --root option. When specified, the root gets prepended to all image locations that don’t already begin with -Y΄http://‘.

