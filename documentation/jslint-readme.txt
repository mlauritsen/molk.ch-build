Documentation for using jslint with rhino:
- http://www.jslint.com/rhino/index.html

Example with java:
- java org.mozilla.javascript.tools.shell.Main jslint.js myprogram.js

Example with the rhino js shell:
- install rhino apt package and run
- rhino jslint.js myprogram.js
