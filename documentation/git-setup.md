Links:
- Project: https://bitbucket.org/mlauritsen/workspace/projects/MLK
- molk.ch: https://bitbucket.org/mlauritsen/molk.ch/src/master/
- molk.ch-build: https://bitbucket.org/mlauritsen/molk.ch-build/src/master/

bitbucket credentials: App Password (see https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/)

Clone:
git clone https://mlauritsen@bitbucket.org/mlauritsen/molk.ch.git
git clone https://mlauritsen@bitbucket.org/mlauritsen/molk.ch-build.git
