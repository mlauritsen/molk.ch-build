Inspired by http://thecodetrain.co.uk/2009/02/running-the-w3c-css-validator-locally-from-the-command-line

Assuming the current dir is "mydir"

1. Get servlet.jar
1.1. Download the Jigsaw web server from http://www.w3.org/Jigsaw
1.2. Unzip it
1.3. Find servlet.jar and copy it to "mydir"
1.4. Delete the other jigsaw files

2. Get the css-validator
2.1. In a terminal, type:
     CVSROOT=:pserver:anonymous@dev.w3.org:/sources/public cvs login
     CVSROOT=:pserver:anonymous@dev.w3.org:/sources/public cvs checkout 2002/css-validator
     cd 2002/css-validator
     mkdir lib
     mv {your servlet.jar location} lib
     ant jar

3. Test the css-validator
3.1. in mydir/2002/css-validator, type:
     java -jar css-validator.jar --output=soap12 http://www.w3.org/
     if there are no java error messages, it works!

4. Configure the css-validator (see http://jigsaw.w3.org/css-validator/manual.html)
