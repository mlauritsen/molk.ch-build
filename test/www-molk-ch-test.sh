#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt

source ../utils/core-utils.sh
source ../utils/test/test-utils.sh

molk_check_script_syntax $0
molk_test_setup

# setup
build_scripts=($(ls -x ../*.sh | grep -v "../check-dest.sh"))
build_test_scripts=($(ls -x ./*.sh | grep -v "./www-molk-ch-test.sh"))
utils_scripts=($(ls -x ../utils/*.sh))
utils_test_scripts=($(ls -x ../utils/test/*.sh))

# check that www.molk.ch is never mentionned
www_molk_ch=$(grep -i "www.molk.ch" ${build_scripts[@]} ${build_test_scripts[@]} ${utils_scripts[@]} ${utils_test_scripts[@]} | grep -v 'licence.txt' || true)
if test "${www_molk_ch}" != ""; then
  echo -e "\nFound references to www.molk.ch:\n${www_molk_ch}" >&2
  exit 43
fi
unset www_molk_ch
echo -n "."

molk_test_teardown
