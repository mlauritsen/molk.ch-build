#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source ../utils/core-utils.sh
source ../utils/test/test-utils.sh

molk_check_script_syntax ../initialise-feeds.sh
molk_test_setup

cd ..
export MOLK_SRC="${MOLK_TEST_RESOURCES}/empty/src"

# test empty
if ! make initialise-feeds &>> "${MOLK_TEST_LOG}"; then
  echo "Empty: Initialise-Feeds failed"
  exit 43
fi

# re-test empty
if ! make initialise-feeds &>> "${MOLK_TEST_LOG}"; then
  echo "Empty: Unclean initialise-feeds failed"
  exit 43
fi

# test empty clean
if ! make clean &>> "${MOLK_TEST_LOG}"; then
  echo "Empty: Clean failed"
  exit 43
fi

cd -
molk_test_teardown
