#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source ../utils/core-utils.sh
source ../utils/test/test-utils.sh

molk_check_script_syntax ../check-dest.sh
molk_test_setup 40

export MOLK_SRC="${MOLK_TEST_RESOURCES}/basic/src"
cd ..

# test basic
if ! make check-src &>> "${MOLK_TEST_LOG}"; then
  echo "Basic: check-src failed"
  exit 43
fi

# re-test basic
if ! make check-src &>> "${MOLK_TEST_LOG}"; then
  echo "Basic: Unclean check-src failed"
  exit 43
fi

# test basic clean
if ! make clean &>> "${MOLK_TEST_LOG}"; then
  echo "Basic: Clean failed"
  exit 43
fi

cd -
molk_test_teardown
