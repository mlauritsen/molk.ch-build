#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source ../utils/core-utils.sh
source ../utils/test/test-utils.sh

molk_check_script_syntax $0
molk_test_setup 40

cd ..
cp -r "${MOLK_TEST_RESOURCES}"/basic/src "${MOLK_TMP}"
export MOLK_SRC="${MOLK_TMP}/src"

# test basic
if ! make transform-pages-setup &>> "${MOLK_TEST_LOG}"; then
  echo "Basic: transform-pages-setup failed"
  exit 43
fi

# re-test basic
if ! make transform-pages-setup &>> "${MOLK_TEST_LOG}"; then
  echo "Basic: Unclean transform-pages-setup failed"
  exit 43
fi

# test basic clean
if ! make clean &>> "${MOLK_TEST_LOG}"; then
  echo "Basic: Clean failed"
  exit 43
fi

cd -
molk_test_teardown
