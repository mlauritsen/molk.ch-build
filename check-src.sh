#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# check $MOLK_SRC files
declare -ar html_files=($(find "${MOLK_CONTENT}" -type f -name "*.html"))
declare -ar fragments=($(ls --format=single-column "${MOLK_SRC_FRAGMENTS}/*.html" 2> /dev/null))
declare -ar css_files=($(find "${MOLK_SRC}/style" -type f -name "*.css"))
declare -ar js_files=($(find "${MOLK_CONTENT}" -type f -name '*.js'))
declare -ar image_files=($(find "${MOLK_CONTENT}" -type f -regextype posix-egrep -regex '.*\.(jpg|png)$' | grep '/images/' | grep -v '/lib/'))
declare -ar feed_files=($(find "${MOLK_CONTENT}" -type f -name feed.xml))
total=$((${#html_files[@]} + ${#fragments[@]} + ${#css_files[@]} + ${#js_files[@]} + ${#feed_files[@]}))
echo -n "Checking ~${total} source files"

# check pages
if test ${#html_files[@]} -ge 1 -a "$(${MOLK_UTILS}/is-online.sh)" == "true"; then
  if ! ${MOLK_UTILS}/check-html.sh ${html_files[@]}; then
    echo -e "\nSource page validation failed" >&2
    exit 42
  fi
  echo -n "."
fi

# check fragments
if test ${#fragments[@]} -ge 1 -a "$(${MOLK_UTILS}/is-online.sh)" == "true"; then
  # validate against the XHTML w3c Schema
  if ! xmlstarlet val --err --net --list-bad --xsd http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd ${fragments[@]}; then
    echo -e "\nFragment validation failed" >&2
    exit 42
  fi
  echo -n "."
fi

# check CSS files
if test ${#css_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/check-css.sh ${css_files[@]}; then
    echo -e "\nCSS validation failed" >&2
    exit 42
  fi
  echo -n "."
fi

# check JS files
if test ${#js_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/check-js.sh ${js_files[@]}; then
    echo -e "\nJavaScript validation failed" >&2
    exit 42
  fi
  echo -n "."
fi

# check image files
if test ${#image_files[@]} -ge 1; then
    for file in "${image_files[@]}"; do
	if ! ${MOLK_UTILS}/check-image.sh "${file}"; then
	    echo -e "\n${file}: Image validation failed" >&2
	    exit 42
	fi
    done
    echo -n "."
fi

# check feed files
if test ${#feed_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/check-feed-online.sh ${feed_files[@]}; then
    echo -e "\nFeed validation failed" >&2
    exit 42
  fi
  echo -n "."
fi

# check that there are no .tmp files
tmp_files=$(find ${MOLK_CONTENT} -type f -name "*.tmp")
if test "${tmp_files}" != ""; then
  echo -e "\n${tmp_files}: There should be no .tmp files in /src/content" >&2
  exit 42
fi
echo -n "."
