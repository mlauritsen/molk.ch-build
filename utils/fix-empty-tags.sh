#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

if test $# -ne 1; then
  echo -e "\nUsage: $0 xml-file\nwas: $0 $*" >&2
  exit 42
fi

file=$1
tmp=${MOLK_TMP}/fix-empty-tags.tmp
declare -r file tmp

if ! test -w $file; then
  echo -e "\n\"$file\": Should be a writable file" >&2
  exit 42
fi

# replace "x/>" by "x />", where x is anything but space
sed 's/\([^ ]\)\/>/\1 \/>/g' $file > ${tmp} && \
mv ${tmp} ${file}
