#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of css files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# setup
tmp=${MOLK_TMP}/check-js-tmp.js
result=${MOLK_TMP}/check-js-result.txt
jslint=${MOLK_LIBRARIES}/jslint/jslint.js
jslint_options=${MOLK_UTILS}/resources/jslint_options.js
declare -r tmp result jslint jslint_options

for file in "$@"; do
  if ! test -r ${file}; then
   echo -e "\n\"${file}\": not a readable file" >&2
   exit 42
  fi

  # Do not check library files
  if echo "${file}" | grep -q '/lib/'; then
      continue;
  fi

  # construct the temporary file (which includes the jslint options)
  rm -f ${tmp};
  if test -r ${jslint_options}; then
    cat ${jslint_options} >> ${tmp};
    echo "" >> ${tmp};
  fi
  cat ${file} >> ${tmp}

  if ! rhino ${jslint} ${tmp} > ${result}; then
    echo -e "\n\"${file}\" - javascript check failed (line numbers are off by +2):" >&2
    cat "${result}" >&2
    exit 42
  fi
done
