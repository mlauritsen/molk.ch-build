#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

echo -e "\nOffline feed check not supported" >&2
exit 42

# input should be a list of atom feed files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-feed-files\nwas: $0 $*" >&2
  exit 42
fi

# ignore error code when grep doesn't find any
without_ignored=($(grep --files-without-match MOLK-TODO "${@}" || true))

# if there are no files left, do nothing
if test ${#without_ignored[@]} -eq 0; then
  exit 0
fi

# validate against the specification relaxng schema
# http://www.asahi-net.or.jp/~eb2m-mrt/atomextensions/atom.rng
if (xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${without_ignored[@]} |& grep -v "Unimplemented block at relaxng.c:3824"); then
  echo -e "\nInvalid atom feed" >&2
  exit 42
fi

# run feedvalidator (http://feedvalidator.org)
for file in "${without_ignored[@]}"; do
  # ignore error code when grep -v doesn't filter any messages
  errors=$(python ${MOLK_LIBRARIES}/feedvalidator/src/demo.py "${file}" AA \
     | grep -v "Validating file:" \
     | grep -v "Self reference doesn't match document location" || true)
  if test "${errors}" != ""; then
    echo -e "\n\"${file}\": Is not RFC-4287 compliant:\n${errors}" >&2
    exit 42
  fi
  unset errors
done
