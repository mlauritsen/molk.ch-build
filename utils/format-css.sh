#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of css files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# setup
tmp=${MOLK_TMP}/format-css.tmp
declare -r tmp

for file in "$@"; do
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": does not exist" >&2
    exit 42
  fi

  if ! csstidy ${file} --silent=true --lowercase_s=true --sort_properties=true --sort_selectors=true --preserve_css=true --optimise_shorthands=2 --template=low ${tmp}; then
    echo -e "\n\"${file}\": Formatting failed" >&2
    exit 42
  fi
  mv ${tmp} ${file}

  # cleanup whitespace
  if ! ${MOLK_UTILS}/clean-whitespace.sh ${file}; then
    echo -e "\n\"${file}\": Whitespace-cleaning failed" >&2
    exit 42
  fi
done
