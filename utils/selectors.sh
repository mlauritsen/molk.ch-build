#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# define functions to select various types of files
# this script is meant to be sourced

# input: should be a directory containing the javascript files to select
# answers: a space-delimited list of absolute names of javascript files
#         (no filtering applied - invoking script must decide what is relevant)
# usage: select_molk_js "${dir}"
function select_molk_js() {
  if test $# -ne 1; then
    echo -e "\nUsage: ${FUNCNAME[0]} directory\nwas: ${FUNCNAME[0]} $@" >&2
    return 42
  fi
  local directory=$1
  echo $(find ${directory} -type f -name "*.js" -print)
}
