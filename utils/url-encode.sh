#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be one argument which will be encoded on stdout
if test $# -ne 1; then
  echo -e "\nUsage: $0 url-to-encode\nwas: $0 $*" >&2
  exit 42
fi

url=$1
declare -r url

if [[ "${url}" =~ .*\".* ]]; then
  echo -e "\nURL cannot contain \": '${url}'" >&2
  exit 42
fi

#rhino -e "print(encodeURIComponent(\"${url}\"))";
rhino ${MOLK_UTILS}/resources/url-encode.js "${url}"
