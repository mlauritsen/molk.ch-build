#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

molk_create_html_item_link() {
    if test $# -ne 4; then
        echo -e "\nUsage: molk_create_html_link <class> <href> <title> <name>" >&2
        return 42
    fi

    local class=$1
    local href=$2
    local title=$3
    local name=$4
    local link='\n<li'
    
    if test -n "${class}"; then
	link="${link} class=\"${class}\""
    fi
    link="${link}><a href=\"${href}\""

    if test -n "${title}" && test "${title}" != "${name}"; then
	link="${link} title=\"${title}\""
    fi
    
    link="${link}>${name}</a></li>"
    echo "${link}"
}
