#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

if test $# -ne 3; then
  echo -e "\nUsage: $0 file xpath value\nwas: $0 $*" >&2
  exit 42
fi

# setup
file=$1
xpath=$2
value=$3
tmp=${MOLK_TMP}/update-xml.tmp
declare -r file xpath value tmp

xmlstarlet ed --pf \
  --update "${xpath}" \
  --value "${value}" \
  "${file}" 1> "${tmp}" \
&& mv "${tmp}" "${file}"
