#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# input should be a list of XHTML files - it's OK not to find any
without_ignored=($(grep --files-without-match MOLK-TODO "$@" || true))

# if there are no files left, do nothing
if test ${#without_ignored[@]} -eq 0; then
  exit 0
fi

# setup
tmp=${MOLK_TMP}/check-html-accessibility.tmp
declare -r tmp

for file in "${without_ignored[@]}"; do
  # ignore xsi:schema errors - tidy does not understand xhtml schema declaration
  tidy -quiet -errors -access 3 -utf8 -file ${tmp} ${file} || true
  sed --in-place --expression='/xmlns:xsi/d' ${tmp}
  sed --in-place --expression='/xsi:schema[lL]ocation/d' ${tmp}

  # ignore "missing dtd" error - molk.ch uses the xhtml xsd
  sed --in-place --expression='/\[3\.2\.1\.1\]/d' ${tmp}
  sed --in-place --expression='/Warning: missing <!DOCTYPE> declaration/d' ${tmp}

  # some warnings are ignored because they do not apply to molk.ch
  # because the site works with JS and CSS disabled.
  # see http://www.htmlpedia.org/wiki/Access for rule details:

  # ignore warnings about site accessibility without JS
  sed --in-place --expression='/\[6\.3\.1\.1\]/d' ${tmp}
  sed --in-place --expression='/\[6\.2\.2\.2\]/d' ${tmp}
  sed --in-place --expression='/\[8\.1\.1\.1\]/d' ${tmp}
  sed --in-place --expression='/\[7\.1\.1\.1\]/d' ${tmp}
  sed --in-place --expression='/\[2\.1\.1\.4\]/d' ${tmp}
  sed --in-place --expression='/\[1\.1\.10\.1\]/d' ${tmp}

  # ignore warnings about using tables for layout - molk.ch does not do that
  sed --in-place --expression='/\[5\.4\.1\.1\]/d' ${tmp}

  # ignore warnings about table captions being mandatory
  # molk.ch always includes the table summary attribute
  sed --in-place --expression='/\[5\.5\.2\.1\]/d' ${tmp}

  # ignore warnings about site accessibility without CSS
  sed --in-place --expression='/\[6\.1\.1\.1\]/d' ${tmp}
  sed --in-place --expression='/\[2\.1\.1\.1\]/d' ${tmp}

  # ignore warnings about empty <div>s and <span>s - they are used for style
  # which should be ignored when CSS is disabled
  sed --in-place --expression='/Warning: trimming empty <div>/d' ${tmp}
  sed --in-place --expression='/Warning: trimming empty <span>/d' ${tmp}

  # single-word link texts can be meaningful
  # ignore warning: "link text not meaningful."
  sed --in-place --expression='/\[13\.1\.1\.1\]/d' ${tmp}

  # this is more a guideline, not really a warning
  # ignore warning: "remove flicker (animated gif)."
  sed --in-place --expression='/\[7\.1\.1\.5\]/d' ${tmp}

  # most of the time, the ALT attribute is sufficient
  # ignore warning: "<img> missing 'longdesc' and d-link."
  sed --in-place --expression='/\[1\.1\.2\.1\]/d' ${tmp}

  # pre-sections are code snippets, not ascii art
  sed --in-place --expression='/\[1\.1\.12\.1\]/d' ${tmp}
  sed --in-place --expression='/\[13\.10\.1\.1\]/d' ${tmp}

  # molk.ch adds meta-data when generating the final output
  sed --in-place --expression='/\[13\.2\.1\.1\]/d' ${tmp}

  # ignore "potential error" "ensure information not conveyed through color alone (input)."
  # input comes from the user, often there is no sensible content to put here
  sed --in-place --expression='/\[2\.1\.1\.5\]/d' ${tmp}

  # complain if file contains <script...> but not <noscript...>
  if grep -qE '<script[^>]*>' "${file}" && ! grep -qE '<noscript[^>]*>' "${file}"; then
    echo -e "\n\"${file}\" contains '<script...>' but not '<noscript...>'" >&2
    exit 42
  fi

  # complain if file contains <noscript...> but not <script...>
  if grep -qE '<noscript[^>]*>' "${file}" && ! grep -qE '<script[^>]*>' "${file}"; then
    echo -e "\n\"${file}\" contains '<noscript...>' but not '<script...>'" >&2
    exit 42
  fi

  if test "$(cat ${tmp})" != ""; then
#  if test -n "${tmp}"; then
    echo -e "\n\"${file}\" accessibility errors: $(cat ${tmp})" >&2
    exit 42
  fi
done
