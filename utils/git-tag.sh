#!/bin/bash
# Copyright 2009-2016 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# Tag the git repo with the current date and a unique id derived from the Hour/Minute/Second

if test $# -ne 1; then
  echo -e "\nUsage: $0 directory\nwas: $0 $*" >&2
  exit 42
fi

declare -r dir="${1}"

if ! test -d "${dir}"; then
  echo -e "\nDirectory does not exist: '${dir}'" >&2
  exit 42
fi

cd "${dir}"

declare -r tag="v$(date +'%Y.%m.%d-%H.%M')"
declare -r message="molk.ch as published on $(date)"

if ! git tag --annotate "${tag}" --message "${message}"; then
    echo -e "\nCould not tag ${dir} as '${tag}' (${message})" >&2
    exit 42
fi
