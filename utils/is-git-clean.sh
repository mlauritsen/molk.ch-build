#!/bin/bash
# Copyright 2009-2016 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# if the directory is a git workspace which is in sync with the remote repo, return exit code 0
# otherwise, write an error message to stderr and return exit code 42

if test $# -ne 1; then
  echo -e "\nUsage: $0 directory\nwas: $0 $*" >&2
  exit 42
fi

declare -r dir="${1}"

if ! test -d "${dir}"; then
  echo -e "\nDirectory does not exist: '${dir}'" >&2
  exit 42
fi

declare DOTGIT="${dir}"/'.git'
if ! test -d "${DOTGIT}"; then
  echo -e "\n'${dir}' is not a git repository\n(${DOTGIT} not found)" >&2
  exit 42
fi
unset DOTGIT

cd "${dir}"

# Untracked => Not clean
if git status | grep -q 'untracked files present'; then
    echo -e "\nUntracked files present in '${dir}':" >&2
    git status -s >&2
  exit 42
fi

# Unstaged => Not clean
if git status | grep -q 'Changes not staged for commit'; then
    echo -e "\nUnstaged changes present in '${dir}':" >&2
    git status -s >&2
  exit 42
fi

# Uncommitted => Not clean
if git status | grep -q 'Changes to be committed'; then
    echo -e "\nUncommitted files present in '${dir}':" >&2
    git status -s >&2
  exit 42
fi

# Unpushed => Not clean
if ! git status | grep -q "Your branch is up-to-date with 'origin/master'"; then
    echo -e "\nUnpushed commits present in '${dir}':" >&2
    git status >&2
  exit 42
fi

# Unpulled => Not clean
if test -n "$(git pull --dry-run 2>&1)"; then
    echo -e "\nRemote repo has unpulled commits" >&2
    git pull --dry-run >&2
  exit 42
fi
