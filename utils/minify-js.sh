#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of js files

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# setup
tmp=${MOLK_TMP}/minify-js.tmp
declare -r tmp

for file in "$@"; do
  if ! test -r ${file}; then
    echo -e "\n\"${file}\" does not exist" >&2
    exit 42
  fi

  # Do not minify src files
  if echo "${file}" | grep -q '\.src\.js'; then
      continue;
  fi

  # Do not minify library files
  if echo "${file}" | grep -q '/lib/'; then
      continue;
  fi

  if ! java -jar ${MOLK_LIBRARIES}/yuicompressor/build/yuicompressor-2.4.2.jar --charset UTF-8 -o ${tmp} ${file}; then
    echo -e "\n\"${file}\": Minification failed" >&2
    exit 42
  fi
  mv ${tmp} ${file}
done
