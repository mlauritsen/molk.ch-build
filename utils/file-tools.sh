#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

function molk_file_exists() {
    if test $# -eq 0; then
	echo -e "\nUsage: molk_get_option filename(s)" >&2
	return 42
    fi

    for file in ${@}; do
	if ! test -f "${file}"; then
	    echo -e "\n'${file}': does not exist" >&2
	    return 42
	fi
    done
}
