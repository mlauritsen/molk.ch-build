#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be one argument which will be encoded on stdout
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

for file in "$@"; do
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": does not exist" >&2
    exit 42
  fi

  # make lines containing only whitespace empty
  if ! sed --regexp-extended --in-place --expression='s/^[[:blank:]]+$//g' ${file}; then
    echo -e "\nWhitespace cleaning failed on \"${file}\": Failed to make lines containing only whitespace empty" >&2
    exit 42
  fi

  # replace multiple empty lines with a single empty line
  if ! sed --regexp-extended --in-place --file=${MOLK_UTILS}/resources/remove-redundant-empty-lines.sed ${file}; then
    echo -e "\nWhitespace cleaning failed on \"${file}\": Failed to replace multiple empty lines with a single empty line" >&2
    exit 42
  fi

  # add a newline at the EOF if there isn't one
  if ! sed --regexp-extended --in-place --file=${MOLK_UTILS}/resources/add-missing-eof-newline.sed ${file}; then
    echo -e "\nWhitespace cleaning failed on \"${file}\": Failed to add a newline at the EOF if there isn''t one" >&2
    exit 42
  fi

  # remove the last line of the file if it is empty
  if ! sed --regexp-extended --in-place --file=${MOLK_UTILS}/resources/remove-empty-eof-line.sed ${file}; then
    echo -e "\nWhitespace cleaning failed on \"${file}\": Failed to remove the last line of the file if it is empty" >&2
    exit 42
  fi
done
