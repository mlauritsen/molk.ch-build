#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/back-link.sh"

if test $# -ne 2; then
  echo -e "\nUsage: $0 '<src-file> <dest-file>'\nwas: $0 $*" >&2
  exit 42
fi

# input
src_file=$1
dest_file=$2

# setup
relative_url=${dest_file#${MOLK_DEST}}
relative_url=${relative_url#/}
filename=$(basename "${relative_url}")
directory=${relative_url%${filename}}
directory=${directory%/}
dest_dir=${MOLK_DEST}/${directory}
directory=${directory}/
tmp_file="${MOLK_TRANSFORM_PAGE}/${filename}"
declare -r src_file dest_file relative_url filename directory dest_dir tmp_file
mkdir -p "${MOLK_TRANSFORM_PAGE}"

# source file must exist
if ! test -f ${src_file}; then
  echo -e "\n\"${src_file}\": Must exist" >&2
  exit 42
fi

# page-name must have an .html extension
if ! [[ "${filename}" =~ [^\.]+\.html$ ]]; then
  echo -e "\n\"${src_file}\": must have an .html extension" >&2
  exit 42
fi

function molk_append() {
  local source="$1"
  local destination="$2"
  if ! test -r "${source}"; then
    echo -e "\n\"${source}\": Cannot append non-existing file" >&2
    exit 42
  fi
  cat "${source}" >> "${destination}"
}

# init
cp ${MOLK_SRC_FRAGMENTS}/html.tag "${tmp_file}"
chmod u+w "${tmp_file}"

# head
echo "<head>" >> "${tmp_file}"

# heading (<body><h1>...)
heading=$(${MOLK_UTILS}/extract-molk-option.sh $src_file title)
if test $? -ne 0; then
  echo -e "\n\"${src_file}\": Error while extracting heading: '${heading}'" >&2
  exit 42
fi

# title (<head><title>...)
# TODO: should not be necessary to set MOLK_DOMAIN here...
export MOLK_DOMAIN='molk.ch'
title=$(${MOLK_UTILS}/window-title.sh "${relative_url}" "${heading}")
unset MOLK_DOMAIN
if test $? -ne 0; then
  echo -e "\n\"${src_file}\": Error while generating page title: '${title}'" >&2
  exit 42
fi
echo "<title>${title}</title>" >> "${tmp_file}"

molk_append "${MOLK_TMP_FRAGMENTS}/head.xml" "${tmp_file}"
${MOLK_UTILS}/page-js.sh $src_file >> "${tmp_file}"
${MOLK_UTILS}/page-css.sh $src_file >> "${tmp_file}"
echo "</head>" >> "${tmp_file}"

# copy body id attribute
#body_id_value=$(${MOLK_UTILS}/xml-select.sh "${src_file}" value xhtml "//xhtml:html/xhtml:body/@id")
#if test -n "${body_id_value}"; then
#    body_id_attribute="id=\"${body_id_value}\""
#else
#    body_id_attribute=""
#fi
#unset body_id_value

# copy body class attribute
body_class_value=$(${MOLK_UTILS}/xml-select.sh "${src_file}" value xhtml "//xhtml:html/xhtml:body/@class")
if test -n "${body_class_value}"; then
    body_class_attribute="class=\"${body_class_value}\""
else
    body_class_attribute=""
fi
unset body_class_value

# body with id and class attribute
#echo "<body ${body_id_attribute} ${body_class_attribute}>" >> "${tmp_file}"
#echo "<body ${body_class_attribute}>" >> "${tmp_file}"
#unset body_id_attribute
#unset body_class_attribute

echo "<body>" >> "${tmp_file}"

# skip to content
molk_append "${MOLK_TMP_FRAGMENTS}/skip.xml" "${tmp_file}"

# logo
molk_append "${MOLK_TMP_FRAGMENTS}/logo.xml" "${tmp_file}"

# crumbs
if ! ${MOLK_UTILS}/crumbs.sh "${relative_url}" >> "${tmp_file}"; then
  echo -e "\n\"${src_file}\": Crumbs failed" >&2
  exit 42
fi

# utils
molk_append "${MOLK_TMP_FRAGMENTS}/utils.xml" "${tmp_file}"

# nav
if ! ${MOLK_UTILS}/get-nav.sh "${directory}" >> "${tmp_file}"; then
  echo -e "\n\"${src_file}\": Failed to append nav (${directory})" >&2
  exit 42
fi

# hbar
molk_append "${MOLK_TMP_FRAGMENTS}/hbar.xml" "${tmp_file}"
  
# sidebar (including sub/sib-nav etc.)
if ! ${MOLK_UTILS}/append-sidebar.sh "${src_file}" "${directory}" "${filename}"; then
  echo -e "\n\"${src_file}\": Append sidebar failed" >&2
  exit 42
fi

# contents
if ! ${MOLK_UTILS}/xml-select.sh "${src_file}" copy xhtml "//xhtml:html/xhtml:body/node()" > "${MOLK_TRANSFORM_PAGE}/content.xml"; then
  echo -e "\n\"${src_file}\": Error while extracting content" >&2
  exit 42
fi
echo "<div id=\"content\" ${body_class_attribute}>" >>  "${tmp_file}"
unset body_class_attribute
echo "<h1>${heading}</h1>" >> "${tmp_file}"
cat "${MOLK_TRANSFORM_PAGE}/content.xml" >>  "${tmp_file}"
echo "</div>" >>  "${tmp_file}"

# noscript
if grep -qE '<script[^>]*>'  "${tmp_file}"; then
  molk_append "${MOLK_TMP_FRAGMENTS}/noscript.xml" "${tmp_file}"
fi

# footer
molk_append "${MOLK_TMP_FRAGMENTS}/footer.xml" "${tmp_file}"

# end file
echo "</body></html>" >> "${tmp_file}"

# feedback-link
mail="Morten Lauritsen Khodabocus<info@molk.ch>"
mail_encoded=$(${MOLK_UTILS}/url-encode.sh "${mail}")
subject_encoded=$(${MOLK_UTILS}/url-encode.sh "Feedback on: ${relative_url}")
mailto_url="mailto:${mail_encoded}?subject=${subject_encoded}"
if ! ${MOLK_UTILS}/update-xml.sh "${tmp_file}" "//*[@id=\"feedback-link\"]/@href" "${mailto_url}"; then
    echo -e "\n\"${src_file}\": Insertion of feedback-link failed" >&2
    exit 42
fi

# back-link
if ! molk_back_link "${relative_url}"; then
    echo -e "\n\"${src_file}\": back-link construction for '${relative_url}' failed" >&2
    exit 42
fi

if test -n "${back_href}"; then
    if ! ${MOLK_UTILS}/update-xml.sh "${tmp_file}" "//*[@id=\"back-link\"]/@href" "${back_href}"; then
	echo -e "\n\"${src_file}\": Insertion of back-link link (${back_href}) failed" >&2
	exit 42
    fi

    if ! ${MOLK_UTILS}/update-xml.sh "${tmp_file}" "//*[@id=\"back-link\"]" "${back_name}"; then
	echo -e "\n\"${src_file}\": Insertion of back-link name (${back_name}) failed" >&2
	exit 42
    fi
else # there is no back-link
    if ! ${MOLK_UTILS}/delete-xml.sh "${tmp_file}" "//*[@id=\"back-link-parent\"]"; then
	echo -e "\n\"${src_file}\": Error deleting back-link" >&2
	exit 42
    fi
fi

unset back_href
unset back_name

#if ! back_link=$(${MOLK_UTILS}/back-link.sh "${relative_url}"); then
#  echo -e "\n\"${src_file}\": back-link construction for '${relative_url}' failed" >&2
#  exit 42
#fi

#if ! ${MOLK_UTILS}/update-xml.sh "${tmp_file}" "//*[@id=\"back-link\"]" "${back_link}"; then

#back_link_esc="$(echo ${back_link} | sed --expression='s/\//\\\//')"
#back_link_expr='s/ id="back-link">/ id="back-link">'"${back_link_esc}/g"
#if ! sed --in-place --expression="${back_link_expr}" "${tmp_file}"; then
#  echo -e "\n\"${src_file}\": Insertion of back-link failed" >&2
#  exit 42
#fi

# creation date
created_option=$(${MOLK_UTILS}/extract-molk-option.sh $src_file created)
if test $? -ne 0; then
  echo -e "\n\"${src_file}\": create-date extraction terminated with error code $?" >&2
  exit 42
fi

if test "${created_option}" != "false"; then
  created_date=$(${MOLK_UTILS}/format-date.sh ${created_option})
  if test $? -ne 0; then
    echo -e "\n\"${src_file}\": create-date formatting terminated with error code $?" >&2
    exit 42
  fi

  if ! ${MOLK_UTILS}/update-xml.sh "${tmp_file}" "//*[@id=\"created\"]" "Created: ${created_date}"; then
    echo -e "\n\"${src_file}\": Insertion of create-date failed" >&2
    exit 42
  fi
else
  # there is no creation-date
  if ! ${MOLK_UTILS}/delete-xml.sh "${tmp_file}" "//*[@id=\"created\"]"; then
    echo -e "\n\"${src_file}\": Deleting creation-date element failed" >&2
    exit 42
  fi
fi

# updated date
updated_option=$(${MOLK_UTILS}/extract-molk-option.sh $src_file updated)
if test $? -ne 0; then
  echo -e "\n\"${src_file}\": update-date extraction terminated with error code $?" >&2
  exit 42
fi

if test "${updated_option}" != "false"; then
  updated_date=$(${MOLK_UTILS}/format-date.sh ${updated_option})
  if test $? -ne 0; then
    echo -e "\n\"${src_file}\": update-date formatting terminated with error code $?" >&2
    exit 42
  fi

  if ! ${MOLK_UTILS}/update-xml.sh "${tmp_file}" "//*[@id=\"updated\"]" "Updated: ${updated_date}"; then
    echo -e "\n\"${src_file}\": Insertion of update-date failed" >&2
    exit 42
  fi
else
  # there is no update-date
  if ! ${MOLK_UTILS}/delete-xml.sh "${tmp_file}" "//*[@id=\"updated\"]"; then
    echo -e "\n\"${src_file}\": Deleting update-date element failed" >&2
    exit 42
  fi
fi

# if there was neither a create-date nor an update-date, delete the #dates element
if test "${updated_option}" == "false" && test "${created_option}" == "false"; then
  if ! ${MOLK_UTILS}/delete-xml.sh "${tmp_file}" "//*[@id=\"dates\"]"; then
    echo -e "\n\"${src_file}\": Deleting dates element failed" >&2
    exit 42
  fi
fi

# if both create-date and update-date are set
if test "${updated_option}" != "false" && test "${created_option}" != "false"; then
    
    # then update-date is not before create-date
    if test "${created_option}" \> "${updated_option}"; then
	echo -e "\n\"${src_file}\": update-date (${updated_option}) cannot be before create-date (${created_option})" >&2
	exit 42
    fi

    # and update-date is not the same as create-date
    if test "${created_option}" = "${updated_option}"; then
	echo -e "\n\"${src_file}\": update-date (${updated_option}) should not be the same as create-date (${created_option})" >&2
	exit 42
    fi
fi

# set selection: utils, nav and subnav
if ! ${MOLK_UTILS}/set-active.sh "${relative_url}"; then
  echo -e "\n\"${src_file}\": Set active failed" >&2
  exit 42
fi

if ! ${MOLK_UTILS}/fix-empty-tags.sh ${tmp_file}; then
  echo -e "\n\"${src_file}\": Error while fixing empty tags" >&2
  exit 42
fi

# create destination directory
mkdir -p "${dest_dir}"

# move tmp file to dest file
mv "${tmp_file}" "${dest_file}"
