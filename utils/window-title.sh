#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# given the relative URL of a page, and the title, write the page (window) title to stdout
if (($# != 2)); then
  echo -e "\nUsage: $0 relative-url title\nwas: $0 $*" >&2
  exit 42
fi

# input: title
declare -r title=$2
if test -z "${title}"; then
  echo -e '\nTitle is mandatory' >&2
  exit 42
fi

# input: relative_url
url=${1#/}
# if the url is "index.html", just use the title
if test "${url}" = "index.html"; then
  echo -n "${title} (${MOLK_DOMAIN})"
  exit 0
fi
if [[ ! "${url}" =~ .+\.html$ ]]; then
  echo -e "\nrelative_url must end in .html - was: '@{relative_url}'" >&2
  exit 42
fi

# throw away last node - it is described in the title
url=${url%.html}
url=${url%/index}
if [[ "${url}" =~ ^(.+)/(.+)$ ]]; then
  url="${BASH_REMATCH[1]}"
else
  echo -n "${title} (${MOLK_DOMAIN})"
  exit 0
fi

# iteratively prettyprint the path to the page
result=""
while [[ "${url}" =~ ^(.+)/(.+)$ ]]; do
  url="${BASH_REMATCH[1]}"
  node="${BASH_REMATCH[2]}"
  result=" - ${node}${result}"
done

echo -n "${title} (${MOLK_DOMAIN}: ${url}${result})"
