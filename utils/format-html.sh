#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of XHTML files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# it's OK not to find any
without_ignored=($(grep --files-without-match MOLK-TODO $@ || true))

# if there are no files left, do nothing
if test ${#without_ignored[@]} -eq 0; then
  exit 0
fi

for file in "$@"; do
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": does not exist" >&2
    exit 42
  fi

  # check the file before formatting it
  #if ! ${MOLK_UTILS}/check-html.sh "${file}"; then
  #  echo -e "\n\"${file}\": Cannot format - checks failed" >&2
  #  exit 42
  #fi

  # format the file
  if ! tidy -quiet -indent -wrap 0 -xml -utf8 --preserve-entities 1 -modify ${file}; then
    echo -e "\n\"${file}\": Formatting failed" >&2
    exit 42
  fi

  # reinsert spaces after end-tags followed by alphabetic characters
  # see also minify-html.sh
  if ! sed --in-place --regexp-extended --expression='s/(<\/[[:alpha:]]+>)([[:alpha:]]|\/|\()/\1 \2/g' "${file}"; then
    echo -e "\n\"${file}\": Format-fixing failed" >&2
    exit 42
  fi    
done
