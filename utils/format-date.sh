#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

if test $# -ne 1; then
  echo -e "\nUsage: $0 YYYY-MM-DD\nwas: $0 $*" >&2
  exit 42
fi

date=$1
declare -r date

function month_name() {
  case $1 in
    01) echo "January";;
    02) echo "February";;
    03) echo "March";;
    04) echo "April";;
    05) echo "May";;
    06) echo "June";;
    07) echo "July";;
    08) echo "August";;
    09) echo "September";;
    10) echo "October";;
    11) echo "November";;
    12) echo "December";;
     *) echo -e "\nInvalid month: \"${1}\"" >&2
        exit 42
        ;;
  esac
}

if [[ "${date}" =~ ^(20[0-1][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$ ]]; then
  year=${BASH_REMATCH[1]}
  month=${BASH_REMATCH[2]}
  day=${BASH_REMATCH[3]}
  echo -n "$(month_name ${month}) ${year}"
  exit 0
fi

echo -e "\n\"${date}\": Not in the expected YYYY-MM-DD format" >&2
exit 42
