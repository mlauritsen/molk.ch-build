#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# if an internet connection is available, write "true" to stdout
# otherwise, write something else
if test $# -gt 1; then
  echo -e "\nUsage: $0 url\nwas: $0 $*" >&2
  exit 42
elif test $# -eq 1; then
  url="${1}"
else
  url='http://www.google.com'
fi
declare -r url

if wget --quiet --tries=1 --timeout=1 --spider "${url}" &> /dev/null; then
  echo "true"
else
  echo "false"
fi
