#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup 20

# setup

if ! xmlstarlet val --err --list-bad --xsd http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd  $(find resources/set-active -name "*.html"); then
  echo "Invalid test files"
  exit 43
fi

cp -r resources/set-active/* tmp
#util setup
cp tmp/transform-page/test-active-utils.html tmp/transform-page/test-inactive-utils.html

# test
export MOLK_TMP=$(pwd)/tmp
export MOLK_CONTENT=$(pwd)/tmp/pages
export MOLK_TRANSFORM_PAGE=$(pwd)/tmp/transform-page

# test KO

if ../set-active.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Argument is mandatory"
  exit 43
fi

if ../set-active.sh nonexisting.html &>> "${MOLK_TEST_LOG}"; then
  echo "Nonexisting page should not be accepted"
  exit 43
fi

# test OK

# test active subnav
if ! ../set-active.sh test-subnav/subnav-active/test-subnav-active.html &>> "${MOLK_TEST_LOG}"; then
  echo "set-active-subnav: should not fail"
  exit 43
fi

if grep -q "class=\"activatable\"" ${MOLK_TRANSFORM_PAGE}/test-subnav-active.html; then
  echo "set-active-subnav: class \"activatable\" should not occur"
  exit 43
fi

# test inactive subnav
if ../set-active.sh test-subnav/subnav-active/test-subnav-inactive.html &>> "${MOLK_TEST_LOG}"; then
  echo "set-inactive-subnav: should fail"
  exit 43
fi

# test active utils
if ! ../set-active.sh /test-active-utils.html &>> "${MOLK_TEST_LOG}"; then
  echo "set-active-utils: should not fail"
  exit 43
fi

if ! grep -q "class=\"active\"" ${MOLK_TRANSFORM_PAGE}/test-active-utils.html; then
  echo "set-active-utils: class \"active\" should occur"
  exit 43
fi

if grep -q "class=\"activatable\"" ${MOLK_TRANSFORM_PAGE}/test-active-utils.html; then
  echo "set-active-utils: class \"activatable\" should not occur"
  exit 43
fi

# test inactive utils
if ! ../set-active.sh /test-inactive-utils.html &>> "${MOLK_TEST_LOG}"; then
  echo "set-inactive-utils: should not fail"
  exit 43
fi

if grep -q "class=\"active\"" ${MOLK_TRANSFORM_PAGE}/test-inactive-utils.html; then
  echo "set-inactive-utils: class \"active\" should not occur"
  exit 43
fi

if grep -q "class=\"activatable\"" ${MOLK_TRANSFORM_PAGE}/test-inactive-utils.html; then
  echo "set-inactive-utils: class \"activatable\" should not occur"
  exit 43
fi

molk_test_teardown
