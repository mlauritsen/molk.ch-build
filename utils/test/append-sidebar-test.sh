#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 
export MOLK_TMP=$(pwd)/tmp/append-sidebar
export MOLK_TRANSFORM_PAGE=${MOLK_TMP}/transform-page
export MOLK_TMP_FRAGMENTS=${MOLK_TMP}/fragments
declare -r SRC_FILE=tmp/append-sidebar/with-sibling/src-file-with-sibling.html
declare -r SRC_FILE_NO_SIBLING=tmp/append-sidebar/no-sibling/src-file-no-sibling.html
declare -r subnav_token='<div id="subnav"\/>'

cp -r resources/append-sidebar tmp

# test KO

if ../append-sidebar.sh &>> "${MOLK_TEST_LOG}"; then
  echo "fail on no arguments"
  exit 43
fi

if ../append-sidebar.sh a &>> "${MOLK_TEST_LOG}"; then
  echo "fail on one argument"
  exit 43
fi

if ../append-sidebar.sh a b &>> "${MOLK_TEST_LOG}"; then
  echo "fail on two arguments"
  exit 43
fi

if ../append-sidebar.sh a b c d &>> "${MOLK_TEST_LOG}"; then
  echo "fail on four arguments"
  exit 43
fi

if ../append-sidebar.sh "${SRC_FILE}" testNav non-existing.html &>> "${MOLK_TEST_LOG}"; then
  echo "non-existing file should fail"
  exit 43
fi

if ../append-sidebar.sh "${SRC_FILE}" nonExistingFragment/something/something/ nonExistingFragment.html &>> "${MOLK_TEST_LOG}"; then
  echo "non-existing subnav fragment should fail"
  exit 43
fi

# test OK

# files in root-dir gets sidebar but no subnav
if ! ../append-sidebar.sh "${SRC_FILE}" / rootFileNoSubNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "rootFileNoSubNav should not fail"
  exit 43
fi

if ! grep -q test-sidebar ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo "rootFileNoSubNav should include sidebar.xml"
  exit 43
fi

if ! grep -q rootFileNoSubNav-contents ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo "rootFileNoSubNav should include original contents"
  exit 43
fi

if grep -q ' id="subnav"' ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo "rootFileNoSubNav should not include id=subnav"
  exit 43
fi

if grep -q ' id="subnav-parent"' ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo 'rootFileNoSubNav should not include id="subnav-parent"'
  exit 43
fi

if ! grep -q ' id="sibnav"' ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo "rootFileNoSubNav should include id=sibnav"
  exit 43
fi

if test $(grep ' id="sibnav"' ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html | wc -l) != 1; then
  echo "rootFileNoSubNav should include id=sibnav exactly once"
  exit 43
fi

if grep -q '<?xml' ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo 'rootFileNoSubNav should not include "<?xml..." header'
  exit 43
fi

if grep -q "${subnav_token}" ${MOLK_TRANSFORM_PAGE}/rootFileNoSubNav.html; then
  echo "rootFileNoSubNav should not include the subnav token"
  exit 43
fi

# files in subdirs without subdirs (fragment is empty) gets sidebar but no subnav
if ! ../append-sidebar.sh "${SRC_FILE}" testEmptyFragment/ subFileNoSubNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "subFileNoSubNav should not fail"
  exit 43
fi

if ! grep -q test-sidebar ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo "subFileNoSubNav should include sidebar.xml"
  exit 43
fi

if ! grep -q subFileNoSubNav-contents ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo "subFileNoSubNav should include original contents"
  exit 43
fi

if grep -q 'id="subnav"' ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo "subFileNoSubNav should not include id=subnav"
  exit 43
fi

if grep -q 'id="subnav-parent"' ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo 'subFileNoSubNav should not include id="subnav-parent"'
  exit 43
fi

if ! grep -q 'id="sibnav"' ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo "subFileNoSubNav should include id=sibnav"
  exit 43
fi

if grep -q '<?xml' ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo 'subFileNoSubNav should not include "<?xml..." header'
  exit 43
fi

if grep -q "${subnav_token}" ${MOLK_TRANSFORM_PAGE}/subFileNoSubNav.html; then
  echo "subFileNoSubNav should not include the subnav token"
  exit 43
fi

# file with subnav
if ! ../append-sidebar.sh "${SRC_FILE}" testNav/testSubNav/notMe/ withSubNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "withSubNav should not fail"
  exit 43
fi

if ! grep -q test-sidebar ${MOLK_TRANSFORM_PAGE}/withSubNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "withSubNav should include sidebar.xml"
  exit 43
fi

if ! grep -q test-subnav ${MOLK_TRANSFORM_PAGE}/withSubNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "withSubNav should include subnav-testSubNav.xml"
  exit 43
fi

if ! grep -q withSubNav-contents ${MOLK_TRANSFORM_PAGE}/withSubNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "withSubNav should include original contents"
  exit 43
fi

if ! grep -q 'id="subnav"' ${MOLK_TRANSFORM_PAGE}/withSubNav.html; then
  echo "withSubNav should include id=subnav"
  exit 43
fi

if ! grep -q 'id="sibnav"' ${MOLK_TRANSFORM_PAGE}/withSubNav.html; then
  echo "withSubNav should include id=sibnav"
  exit 43
fi

if grep -q "${subnav_token}" ${MOLK_TRANSFORM_PAGE}/withSubNav.html; then
  echo "withSubNav should not include the subnav token"
  exit 43
fi


# file without siblings
if ! ../append-sidebar.sh "${SRC_FILE_NO_SIBLING}" / noSibNav.html &>> "${MOLK_TEST_LOG}"; then
  echo "no siblings should not fail"
  exit 43
fi

if grep -q sibnav ${MOLK_TRANSFORM_PAGE}/noSibNav.html; then
  echo "noSibNav should not contain 'sibnav'"
  exit 43
fi

molk_test_teardown
