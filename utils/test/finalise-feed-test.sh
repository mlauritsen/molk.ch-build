#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup 20

# setup 

export MOLK_TMP=$(pwd)/tmp
export MOLK_CONTENT=$(pwd)/tmp/content
export MOLK_DEST=$(pwd)/tmp/dest
export MOLK_SRC_FRAGMENTS=${MOLK_TMP}/src-fragments
export MOLK_TMP_FRAGMENTS=${MOLK_TMP}/tmp-fragments
export MOLK_URL=http://molk-test.org

cp -r resources/finalise-feed/* tmp
src_feed=${MOLK_CONTENT}/feed.xml
dest_feed=${MOLK_DEST}/feed.xml
src_html_feed=${MOLK_CONTENT}/feed.html
dest_html_feed=${MOLK_DEST}/feed.html

src_feed_html_missing=${MOLK_CONTENT}/html-missing/feed.xml
src_feed_ungenerated=${MOLK_CONTENT}/ungenerated/feed.xml

# test KO

if ../finalise-feed.sh >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Finalisation should fail without arguments"
  exit 43
fi

if ../finalise-feed.sh non-existing.xml >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Finalisation should fail on non-existing feed"
  exit 43
fi

if ../finalise-feed.sh ${src_feed_html_missing} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${src_feed_html_missing}: should fail when src html feed is missing"
  exit 43
fi

if ../finalise-feed.sh ${src_feed_ungenerated} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${src_feed_html_missing}: should fail when src html feed exists but was not generated"
  exit 43
fi

# test OK

if ! ../finalise-feed.sh ${src_feed} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Feed finalisation should succeed"
  exit 43
fi

if ! xmlstarlet val --err --list-bad --xsd http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd "${dest_html_feed}" 2>> "${MOLK_TEST_LOG}"; then
  echo "${dest_html_feed} should be valid"
  exit 43
fi

if grep -q 'atom-cert' ${dest_html_feed}; then
  echo "${dest_html_feed} should not contain the atom token"
  exit 43
fi

if ! grep -q 'valid-atom-test.png' ${dest_html_feed}; then
  echo "${dest_html_feed} should contain atom certificate image from fragment"
  exit 43
fi

if ! grep -q "url=${MOLK_URL}/feed.xml" ${dest_html_feed}; then
  echo "${dest_html_feed} should contain the atom referer validation link: ${MOLK_URL}/feed.xml"
  exit 43
fi

if ! diff ${src_feed} resources/finalise-feed/content/feed.xml; then
  echo "${src_feed} should not have changed"
  exit 43
fi

if test -e ${src_html_feed}; then
  echo "${src_html_feed} should no longer exist"
  exit 43
fi

molk_test_teardown
