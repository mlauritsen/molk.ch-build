#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax

source ../molk-option-tools.sh

molk_test_setup

# molk_is_molk

if molk_is_molk &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_is_molk' should fail without args"
  exit 43
fi

if ! molk_is_molk resources/molk-option-tools/molk-page.html &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_is_molk' should answer 0"
  exit 43
fi

if molk_is_molk resources/molk-option-tools/molk-page-not.html &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_is_molk' should answer 1"
  exit 43
fi

# molk_get_title

if molk_get_title &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_get_title' should fail without args"
  exit 43
fi

if molk_get_title does-not-exist &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_get_title' should fail on non-existing file"
  exit 43
fi

molk_assert "molk title" "test-title" "$(molk_get_title resources/molk-option-tools/molk-title.html)"
molk_assert "file name as title" "file-name-as-title" "$(molk_get_title resources/molk-option-tools/file-name-as-title.html)"

# molk_shorten_title

if molk_shorten_title &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_shorten_title' should fail without args"
  exit 43
fi

molk_assert "no title"                   ""                     "$(molk_shorten_title '')"
molk_assert "short"                      "short-title"          "$(molk_shorten_title short-title)"
molk_assert "long"                       "1234567890123456789…" "$(molk_shorten_title 123456789012345678901234567890)"
molk_assert "exactly 20"                 "12345678901234567890" "$(molk_shorten_title 12345678901234567890)"
molk_assert "just under limit"           "1234567890123456789"  "$(molk_shorten_title 1234567890123456789)"
molk_assert "just over limit"            "1234567890123456789…" "$(molk_shorten_title 123456789012345678901)"
molk_assert "… on limit"                 "1234567890123456789…" "$(molk_shorten_title '1234567890123456789…')"
molk_assert "&#x2026; on limit"          "1234567890123456789…" "$(molk_shorten_title '1234567890123456789&#x2026;')"
molk_assert "&#x2026; passing limit"     "12345678901234567…"   "$(molk_shorten_title '12345678901234567&#x2026;')"
molk_assert "remove lowercase vowels"    "12345678901234567890" "$(molk_shorten_title '1234567890a1e2i3o4u5y67890')"
molk_assert "remove uppercase vowels"    "12345678901234567890" "$(molk_shorten_title '1234567890A1E2I3O4U5Y67890')"
molk_assert "remove mixed case vowels"   "12345678901234567890" "$(molk_shorten_title '1234567890aAa1eEe2iIi3oOo4uUu5yYy67890')"
molk_assert "under limit without vowels" "w w ww w w"           "$(molk_shorten_title 'wooo ooow waeiouyw ooow ooow')"
molk_assert "over limit without vowels"  "www www www www www…" "$(molk_shorten_title 'wowow wowow wowow wowow wowow wowow')"
molk_assert "trailing spaces"            "123456789012345"      "$(molk_shorten_title '123456789012345     ')"
molk_assert "leading spaces"             "123456789012345"      "$(molk_shorten_title '     123456789012345')"

# molk_get_feed_type

if molk_get_feed_type &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_get_feed_type' should fail without args"
  exit 43
fi

if molk_get_feed_type resources/molk-option-tools/molk-page-not.html &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_get_feed_type' should fail on non-molk page"
  exit 43
fi

molk_assert "missing is 'update'" "update" "$(molk_get_feed_type resources/molk-option-tools/feed-missing.html)"
molk_assert "empty is 'update'" "update" "$(molk_get_feed_type resources/molk-option-tools/feed-empty.html)"
molk_assert "answer value" "something" "$(molk_get_feed_type resources/molk-option-tools/feed-something.html)"

# molk_get_option

declare -r OPTIONS=resources/molk-option-tools/molk_get_option/extract-molk-option.xml
declare -r OPTIONS_DUPLICATE=resources/molk-option-tools/molk_get_option/extract-molk-option-duplicate-processing-instruction.xml
declare -r OPTIONS_MISSING=resources/molk-option-tools/molk_get_option/extract-molk-option-missing.xml

molk_file_exists "${OPTIONS}" "${OPTIONS_DUPLICATE}" "${OPTIONS_MISSING}"

if molk_get_option wrong number of arguments &>> "${MOLK_TEST_LOG}"; then
  echo "wrong number of arguments should not succeed"
  exit 43
fi

if molk_get_option does-not-exist.xml test &>> "${MOLK_TEST_LOG}"; then
  echo "Nonexisting file should not succeed"
  exit 43
fi

if molk_get_option "${OPTIONS}" double &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail on duplicated attribute"
  exit 43
fi

if molk_get_option "${OPTIONS_MISSING}" something &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail on absent '<?molk... ?>' instruction"
  exit 43
fi

if molk_get_option "${OPTIONS_DUPLICATE}" something &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail on absent '<?molk... ?>' instruction"
  exit 43
fi

non_existing=$(molk_get_option "${OPTIONS}" nonexisting)
if test $? -ne 0; then
  echo "Nonexisting options should not cause an error"
  exit 43
fi

if test "${non_existing}" != "false"; then
  echo "Nonexisting options should have the value \"false\" - was: \"${non_existing}\""
  exit 43
fi

empty=$(molk_get_option "${OPTIONS}" empty)
if test $? -ne 0; then
  echo "Present but empty options should not cause an error"
  exit 43
fi

if test "${empty}" != "false"; then
  echo "Present but empty options should have the value \"false\" - was: \"${empty}\""
  exit 43
fi

name=$(molk_get_option "${OPTIONS}" name)
if test $? -ne 0; then
  echo "Present options should not cause an error"
  exit 43
fi

if test "$name" != "test"; then
  echo "Present options should be parsed correctly - was \"${name}\""
  exit 43
fi

molk_test_teardown
