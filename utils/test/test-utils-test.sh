#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh

molk_test_setup

molk_check_script_syntax test-utils.sh

molk_test_teardown
