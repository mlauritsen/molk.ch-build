#!/bin/bash
# Copyright 2009-2016 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# Setup repo
declare -r repo="${MOLK_TEST_TMP}"
git init "${MOLK_TEST_TMP}" &>> "${MOLK_TEST_LOG}"
cd "${repo}"

git config user.name "test"
git config user.email "test@test.test"
git config push.default simple

echo "First" > first.txt
git add first.txt &>> "${MOLK_TEST_LOG}"
git commit -m "first" &>> "${MOLK_TEST_LOG}"
cd --

if "${MOLK_UTILS}/git-tag.sh" &>> "${MOLK_TEST_LOG}"; then
    echo "Should fail on missing argument"
    exit 43
fi

if "${MOLK_UTILS}/git-tag.sh" doesNotExist &>> "${MOLK_TEST_LOG}"; then
    echo "Should fail on non-existing directory"
    exit 43
fi

if ! "${MOLK_UTILS}/git-tag.sh" "${repo}" &>> "${MOLK_TEST_LOG}"; then
    echo "Should succeed"
    exit 43
fi

if "${MOLK_UTILS}/git-tag.sh" "${repo}" &>> "${MOLK_TEST_LOG}"; then
    date
    echo "Should fail when tagging in quick succession"
    exit 43
fi

if test "$(cd ${repo} && git tag --list | wc -l)" -ne "1"; then
    echo "Should create exactly 1 tag"
    exit 43
fi

molk_test_teardown
