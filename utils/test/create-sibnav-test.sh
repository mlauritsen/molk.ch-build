#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 
export LOCATION=$(pwd)/resources/create-sibnav

# test KO
if ../create-sibnav.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without args"
  exit 43
fi

if ../create-sibnav.sh nav1 nav2 &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with too many args"
  exit 43
fi

if ../create-sibnav.sh "" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with empty argument"
  exit 43
fi

if ../create-sibnav.sh create-sibnav.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with non-directory argument"
  exit 43
fi

if ../create-sibnav.sh "${LOCATION}"/testnav/ &>> "${MOLK_TEST_LOG}"; then
  echo "should not end in /"
  exit 43
fi

# test OK

NO_SIBLINGS='tmp/no-siblings.xml'
if ! ../create-sibnav.sh "${LOCATION}"/no-siblings/test.html > "${NO_SIBLINGS}"; then
    echo "no-siblings failed"
    exit 43
fi

if grep -q ' href="index.html"' "${NO_SIBLINGS}"; then
  echo "index.html should not be included as a sibling, was: '$(cat ${NO_SIBLINGS})'"
  exit 43
fi

if grep -q "test" "${NO_SIBLINGS}"; then
  echo "if test.html has no siblings, it should not be included, was: '$(cat ${NO_SIBLINGS})'"
  exit 43
fi
unset NO_SIBLINGS

SIBLINGS='tmp/siblings.xml'
if ! ../create-sibnav.sh "${LOCATION}"/siblings/page.html > "${SIBLINGS}"; then
  echo "siblings failed"
  exit 43
fi

if ! test -s "${SIBLINGS}"; then
  echo "siblings should not be empty"
  exit 43
fi

if grep -q ' href="index.html"' "${SIBLINGS}"; then
  echo "index.html should not be included as a sibling, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q ' href="delta-page.html"' "${SIBLINGS}"; then
  echo "Pages with feed type 'delta' should not be included as a sibling, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q ' href="meta-page.html"' "${SIBLINGS}"; then
  echo "Pages with feed type 'meta' should not be included as a sibling, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep -q ' href="page.html"' "${SIBLINGS}"; then
  echo "if page.html has siblings, it should be included itself, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep ' href="page.html"' "${SIBLINGS}" | grep -q ' class="active"'; then
  echo "page.html should have class 'active', was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -v ' href="page.html"' "${SIBLINGS}" | grep -q 'class="active"'; then
  echo "no siblings should have class 'active', was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep ' class="' "${SIBLINGS}" | grep -q 'activatable'; then
  echo 'sibnav links should not be activatable, active page is already classified - set-active.sh not required'
  exit 43
fi

if ! grep -q '>Test Molk Page Title<' "${SIBLINGS}"; then
  echo "Should include page title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep -q '>Short Title<' "${SIBLINGS}"; then
  echo "Should include short title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep '>Short Title<' "${SIBLINGS}" | grep -q ' title="'; then
  echo "Should not include hover title for short title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep ' href="long-title.html"' "${SIBLINGS}" | grep -q 'V…<'; then
  echo "Should include shortened long title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep -q ' title="Very Very Very Very Very Very Very Very Very Very Very Very Long Title"' "${SIBLINGS}"; then
  echo "Should include long hover title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep -q '>not-a-molk-page<' "${SIBLINGS}"; then
  echo "Should include not-a-molk-page as non-molk title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q '>Subdir Index Title<' "${SIBLINGS}"; then
  echo "Should not include subdir index title, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q '>with-non-molk-index<' "${SIBLINGS}"; then
  echo "Should not include non-molk subdir name, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q 'not-a-page' "${SIBLINGS}"; then
  echo "Should not include not-a-page, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q 'no-index' "${SIBLINGS}"; then
  echo "Should not include no-index, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if ! grep -q 'feed-update.html' "${SIBLINGS}"; then
  echo "Should include feed-update.html, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q 'feed-meta.html' "${SIBLINGS}"; then
  echo "Should not include feed-meta.html, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

if grep -q 'feed-delta.html' "${SIBLINGS}"; then
  echo "Should not include feed-delta.html, was: '$(cat ${SIBLINGS})'"
  exit 43
fi

molk_test_teardown
