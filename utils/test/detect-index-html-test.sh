#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

if ../detect-index-html.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without args"
  exit 43
fi

if ../detect-index-html.sh tmp1 tmp2 &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail with too many arguments"
  exit 43
fi

# KO - only contains other file
mkdir -p tmp/only-other
touch tmp/only-other/other.html
if ../detect-index-html.sh tmp/only-other &>> "${MOLK_TEST_LOG}"; then
  echo "tmp/only-other contains a .html-file but no index.html"
  exit 43
fi

# OK - only contains index.html
mkdir -p tmp/only-index
touch tmp/only-index/index.html
if ! ../detect-index-html.sh tmp/only-index &>> "${MOLK_TEST_LOG}"; then
  echo "tmp/only-index contains a .html-file"
  exit 43
fi

# OK - contains index.html and other.html
mkdir -p tmp/index-and-other
touch tmp/index-and-other/index.html
touch tmp/index-and-other/other.html
if ! ../detect-index-html.sh tmp/index-and-other &>> "${MOLK_TEST_LOG}"; then
  echo "tmp/index-and-other contains a .html-file and an index.html"
  exit 43
fi

# OK - contains no files
mkdir -p tmp/no-files
if ! ../detect-index-html.sh tmp/no-files &>> "${MOLK_TEST_LOG}"; then
  echo "tmp/no-files contains no html files, which is fine"
  exit 43
fi

molk_test_teardown
