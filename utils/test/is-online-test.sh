#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

offline_url='http://does-not-exist'
online_url='http://www.google.com'

# tests

if ! ../is-online.sh &> /dev/null; then
  echo "Should use default without arguments"
  exit 43
fi

if ../is-online.sh a b &> /dev/null; then
  echo "Should fail with too many arguments"
  exit 43
fi

if ! ../is-online.sh "${offline_url}" &> /dev/null; then
  echo "Should not fail when offline"
  exit 43
fi

if test $(../is-online.sh "${offline_url}") == "true"; then
  echo "Should indicate offline when URL is unreachable"
  exit 43
fi

if ! ../is-online.sh "${online_url}" &> /dev/null; then
  echo "Should not fail when online"
  exit 43
fi

if test $(../is-online.sh "${online_url}") != "true"; then
  echo "Should indicate online when URL is reachable"
  exit 43
fi

molk_test_teardown
