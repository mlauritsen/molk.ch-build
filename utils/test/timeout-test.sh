#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh

function molk_timeout_assert() {
    if test $# -ne 4; then
	echo -e "\nUsage: molk_timeout_assert <<timeout>> <<duration>> <<expected-return-code>> <<description>>" >&2
	return 43
    fi

    local timeout=$1
    local duration=$2
    local expected_rc=$3
    local description=$4

    MOLK_TEST_START_TIME="$((SECONDS - duration))"
    MOLK_TEST_TIMEOUT=0
    molk_test_setup ${timeout}

    if ! molk_test_teardown &>> "${MOLK_TEST_LOG}"; then
	actual_rc=1
    else
	actual_rc=0
    fi
    molk_assert "${description}: start ${MOLK_TEST_START_TIME} timeout ${timeout} duration ${duration}" "${expected_rc}" "${actual_rc}"
}

molk_timeout_assert -1 3  0 'OK: No timeout, duration <5'
molk_timeout_assert -1 7  0 'OK: No timeout, duration >5'
molk_timeout_assert '' 3  0 'OK: Default timeout, duration <5'
molk_timeout_assert '' 7  1 'KO: Default timeout, duration >5'
molk_timeout_assert  3 1  0 'OK: Explicit timeout, duration <'
molk_timeout_assert  3 3  0 'OK: Explicit timeout, duration ='
molk_timeout_assert  3 5  1 'KO: Explicit timeout, duration >'

MOLK_TEST_START_TIME="$((SECONDS))"
MOLK_TEST_TIMEOUT=0
molk_test_teardown
