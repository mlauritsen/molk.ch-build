#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

png=tmp/minify-png/unminified.png
test=tmp/minify-png/test.png
cp -r resources/minify-png tmp

# test KO

if ../minify-png.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../minify-png.sh non-existing &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing dir"
  exit 43
fi

# test OK

unminified_size=$(stat --format=%s ${png})

if ! ../minify-png.sh "${test}" "${png}" &>> "${MOLK_TEST_LOG}"; then
  echo "first minify should not fail"
  exit 43
fi

minified_size=$(stat --format=%s ${png})

if test $minified_size -gt $unminified_size; then
  echo 'Minified size should be smaller than unminified'
  exit 43
fi

if ! ../minify-png.sh "${png}" "${test}" &>> "${MOLK_TEST_LOG}"; then
  echo "reminify should not fail"
  exit 43
fi

reminified_size=$(stat --format=%s ${png})

if test $reminified_size -ne $minified_size; then
  echo 'Reminified size should be the same as minified'
  exit 43
fi

molk_test_teardown
