#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 

js=tmp/format-js.js
cp resources/format-js.js ${js}

# test KO

if ../format-js.sh >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "should fail without arguments"
  exit 43
fi

if ../format-js.sh non-existing.js >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "should fail on non-existing file"
  exit 43
fi

# test OK

if ! ../format-js.sh ${js} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "formatting ${js} should not fail"
  exit 43
fi

if ! ../format-js.sh ${js} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "already formatted ${js} should not fail"
  exit 43
fi

if grep -q "          " ${js}; then
  echo "${js} should no longer contain many consecutive spaces"
  exit 43
fi

if ! grep -q "comment" ${js}; then
  echo "${js}: comments should be retained"
  exit 43
fi

size=$(stat --format=%s ${js})
if test ${size} -gt 500; then
  echo "formatted file should be less than 500 bytes, was $size"
  exit 43
fi

molk_test_teardown
