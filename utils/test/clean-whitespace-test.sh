#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

cp resources/clean-whitespace/* tmp
# these files need to be in a format where Emacs does not automatically append a missing newline at the end
with_whitespace=tmp/with-whitespace.sh
expected=tmp/expected.txt

trailing_newlines=tmp/trailing-newlines.txt
cp ${with_whitespace} ${trailing_newlines}
for i in $(seq 0 1 9); do
  echo "" >> ${trailing_newlines};
done

# test KO

if ../clean-whitespace.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail when invoked without arguments"
  exit 43
fi

if ../clean-whitespace.sh non-existing.txt &>> "${MOLK_TEST_LOG}"; then
  echo "should fail when invoked on non-existing file"
  exit 43
fi

# test OK

if ! ../clean-whitespace.sh ${with_whitespace} &>> "${MOLK_TEST_LOG}"; then
  echo "${with_whitespace}: should not fail"
  exit 43
fi

# clean-whitespace should also add the missing newline before the EOF
if ! diff -q ${with_whitespace} ${expected} &>> "${MOLK_TEST_LOG}"; then
  echo "${with_whitespace}: formatting should be identical to ${expected}"
  exit 43
fi

../clean-whitespace.sh ${with_whitespace}
if ! diff -q ${with_whitespace} ${expected} &>> "${MOLK_TEST_LOG}"; then
  echo "${with_whitespace}: reformatting should still be identical to ${expected}"
  exit 43
fi

if ! ../clean-whitespace.sh ${trailing_newlines} &>> "${MOLK_TEST_LOG}"; then
  echo "${trailing_newlines}: should not fail"
  exit 43
fi

if ! diff -q ${trailing_newlines} ${expected} &>> "${MOLK_TEST_LOG}"; then
  echo "${trailing_newlines}: formatting should be identical to ${expected}"
  exit 43
fi

molk_test_teardown
