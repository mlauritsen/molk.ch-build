#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# test KO

if ../url-encode.sh "abc\"def" &> /dev/null; then
  echo "should fail if the url contains a \""
  exit 43
fi

if ../url-encode.sh &> /dev/null; then
  echo "should fail without arguments"
  exit 43
fi

if ../url-encode.sh a b &> /dev/null; then
  echo "should fail with too many arguments"
  exit 43
fi

# test OK

if ! ../url-encode.sh "" &> /dev/null; then
  echo "should not fail on empty argument"
  exit 43
fi

if ! test x$(../url-encode.sh "")x == xx; then
  echo "Empty string should encode to empty string"
  exit 43
fi

if ! ../url-encode.sh "abc" &> /dev/null; then
  echo "should not fail on valid input: 'abc'"
  exit 43
fi

if ! test "$(../url-encode.sh 'abc.html')" == "abc.html"; then
  echo "'abc.html' should not change when encoded"
  exit 43
fi

if ! test x$(../url-encode.sh "a a")x == xa%20ax; then
  echo "Space should be encoded"
  exit 43
fi

if ! ../url-encode.sh " / a / b / c . h" &> /dev/null; then
  echo 'should not fail on valid input: (" / a / b / c . h")'
  exit 43
fi

molk_test_teardown
