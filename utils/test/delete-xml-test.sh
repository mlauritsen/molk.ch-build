#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup
export MOLK_TMP=$(pwd)/tmp

cp resources/delete-xml.xml tmp

# test KO

if ../delete-xml.sh &>> "${MOLK_TEST_LOG}"; then
  echo "no arguments should fail"
  exit 43
fi

# test OK

if ! ../delete-xml.sh tmp/delete-xml.xml "//*[@id=\"e\"]/@f"; then
  echo "Deleting attribute should not fail"
  exit 43
fi

if grep -q "f=" tmp/delete-xml.xml; then
  echo "The f attribute should be gone"
  exit 43
fi

if ! ../delete-xml.sh tmp/delete-xml.xml "//*[@id=\"e\"]/@DoesNotExist"; then
  echo "Deleting non-existing attribute should not fail"
  exit 43
fi

if ! ../delete-xml.sh tmp/delete-xml.xml "//*[@id=\"e\"]"; then
  echo "Deleting element should not fail"
  exit 43
fi

if grep -q "<d" tmp/delete-xml.xml; then
  echo "The d element should be gone"
  exit 43
fi

if ! ../delete-xml.sh tmp/delete-xml.xml "//*[@id=\"DoesNotExist\"]"; then
  echo "Deleting non-existing element should not fail"
  exit 43
fi

if ! grep -q "<a>" tmp/delete-xml.xml; then
  echo "The '<a>' element should still be there"
  exit 43
fi

molk_test_teardown
