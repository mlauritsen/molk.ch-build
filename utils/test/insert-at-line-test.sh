#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

cp -r resources/insert-at-line/* tmp
base=tmp
to_insert=${base}/to_insert.txt
file=${base}/file.txt
before=${base}/before.txt
middle=${base}/middle.txt
after=${base}/after.txt
cp ${file} ${before}
cp ${file} ${middle}
cp ${file} ${after}

# test KO

if ../insert-at-line.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Missing options should fail"
  exit 43
fi

if ../insert-at-line.sh non-existing.txt ${file} 1 &>> "${MOLK_TEST_LOG}"; then
  echo "file-to-insert must exist"
  exit 43
fi

if ../insert-at-line.sh ${to_insert} non-existing.txt 1 &>> "${MOLK_TEST_LOG}"; then
  echo "file must exist"
  exit 43
fi

if ../insert-at-line.sh ${to_insert} ${file} abc &>> "${MOLK_TEST_LOG}"; then
  echo "line must be a number"
  exit 43
fi

# test OK

# before
if ! ../insert-at-line.sh ${to_insert} ${before} 0 &>> "${MOLK_TEST_LOG}"; then
  echo "${before}: should succeed"
  exit 43
fi

if ! head -1 ${before} | grep -q 'INSERTED' &>> "${MOLK_TEST_LOG}"; then
  echo "${before}: INSERTED should be the first line"
  exit 43
fi

if ! grep -q "a1" ${before} &>> "${MOLK_TEST_LOG}"; then
  echo "${before}: a1 should still be in there"
  exit 43
fi

# middle
if ! ../insert-at-line.sh ${to_insert} ${middle} 1 &>> "${MOLK_TEST_LOG}"; then
  echo "${middle}: should succeed"
  exit 43
fi

if ! head -1 ${middle} | grep -q 'a1' &>> "${MOLK_TEST_LOG}"; then
  echo "${middle}: a1 should be the first line"
  exit 43
fi

if ! tail -1  ${middle} | grep -q 'b2' &>> "${MOLK_TEST_LOG}"; then
  echo "${middle}: b2 should still be in there"
  exit 43
fi

if ! grep -q 'INSERTED' ${middle} &>> "${MOLK_TEST_LOG}"; then
  echo "${middle}: INSERTED should be the first line"
  exit 43
fi

# after
if ! ../insert-at-line.sh ${to_insert} ${after} 20 &>> "${MOLK_TEST_LOG}"; then
  echo "${after}: should succeed"
  exit 43
fi

if ! head -1 ${after} | grep -q 'a1' &>> "${MOLK_TEST_LOG}"; then
  echo "${after}: a1 should be the first line"
  exit 43
fi

if ! tail -1  ${after} | grep -q 'INSERTED' &>> "${MOLK_TEST_LOG}"; then
  echo "${after}: INSERTED should still be in there"
  exit 43
fi

molk_test_teardown
