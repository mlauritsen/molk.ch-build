#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax

# setup
# This test is a special case, cannot use generic test setup/teardown

if test $# -eq 0; then
    test='all'
    port='8042'
elif test $# -eq 2; then
    test="$1"
    port="${2}"
else
    echo "Invalid $# arguments: ${@}" >&2
    exit 43
fi
declare -r test
declare -ri port

export MOLK_TMP="$(pwd)/tmp/${port}"
export MOLK_LOG="$(pwd)/tmp/molk-${port}.log"
if ! mkdir -p "${MOLK_TMP}"; then
    echo "Could not create ${MOLK_TMP}"
    exit 43
fi

base="$(pwd)/resources/check-web"
valid=${base}/valid
file=${base}/file
does_not_exist=${base}/does-not-exist
#invalid_anchor=${base}/invalid-anchor
invalid_a_link=${base}/invalid-a-link
invalid_css_link=${base}/invalid-css-link
invalid_image=${base}/invalid-image
invalid_script_src=${base}/invalid-script-src

# test KO

function molk_test_ko() {
    if ../check-web.sh "${port}" &> /dev/null; then
	echo "should not pass without arguments"
	exit 43
    fi

    if ../check-web.sh $does_not_exist "${port}" &> /dev/null; then
	echo "${does_not_exist} should not pass"
	exit 43
    fi

    if ../check-web.sh $file "${port}" &> /dev/null; then
	echo "${file} should not pass"
	exit 43
    fi

    if ../check-web.sh $valid $valid "${port}" &> /dev/null; then
	echo "multiple arguments should not pass"
	exit 43
    fi
}

function molk_test_invalid_css() {
    if ../check-web.sh $invalid_css_link "${port}" &> /dev/null; then
	echo "${invalid_css_link} should not pass"
	exit 43
    fi
}

function molk_test_invalid_image() {
    if ../check-web.sh $invalid_image "${port}" &> /dev/null; then
	echo "${invalid_image} should not pass"
	exit 43
    fi
}

function molk_test_invalid_script() {
    if ../check-web.sh $invalid_script_src "${port}" &> /dev/null; then
	echo "${invalid_script_src} should not pass"
	exit 43
    fi
}

# test OK with Warning

function molk_clean_tmp() {
    rm -rf tmp || exit 43
    mkdir tmp || exit 43
}

function molk_test_invalid_link() {
    if ! ../check-web.sh $invalid_a_link "${port}" &> /dev/null; then
	echo "${invalid_a_link} should pass"
	exit 43
    fi

    if ! grep -q 'contains bad links' "${MOLK_LOG}"; then
	echo "${invalid_a_link} should cause warning"
	exit 43
    fi
}

# test OK

function molk_test_ok() {
    if ! ../check-web.sh ${valid} "${port}"; then # &> /dev/null; then
	echo "${valid} should pass"
	exit 43
    fi
}

# run tests
if test $test = 'ok'; then
    molk_test_ok;
elif test $test = 'ko'; then
    molk_test_ko;
elif test $test = 'invalid_css'; then
    molk_test_invalid_css;
elif test $test = 'invalid_image'; then
    molk_test_invalid_image;
elif test $test = 'invalid_link'; then
    molk_test_invalid_link;
elif test $test = 'invalid_script'; then
    molk_test_invalid_script;
elif test $test = 'all'; then
    time parallel -P 10 --no-notice --xapply ./check-web-test.sh {1} {2} ::: ok ko invalid_css invalid_image invalid_link invalid_script ::: $(seq -w 8042 8047)

    # teardown

    if ! rm -rf tmp; then
	echo "Could not remove tmp directory"
	exit 43
    fi
else
    echo "Unknown test: $test (port=$port)" >&2
    exit 43
fi

echo "tests OK: $0 $test $port"
