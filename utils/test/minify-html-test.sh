#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 

cp resources/minify-html/* tmp
html=tmp/minify-html.html

# test KO

if ../minify-html.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../minify-html.sh non-existing.html &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file"
  exit 43
fi

# test OK

if ! ../minify-html.sh "${html}" &>> "${MOLK_TEST_LOG}"; then
  echo "${html} should not fail on one file"
  exit 43
fi

if grep -q "</a>more" "${html}"; then
  echo "${html}: spaces after closing tags should not be truncated"
  exit 43
fi

if ! grep -q "&amp;" "${html}"; then
  echo "Named entity should be preserved"
  exit 43
fi

if ! grep -q "&#8217;" "${html}"; then
  echo "Numeric entity should be preserved"
  exit 43
fi

if grep -q "          " "${html}"; then
  echo "${html} should no longer contain many consecutive spaces"
  exit 43
fi

if test $(cat ${html} | wc -l) -gt 50; then
  echo "${html} should be less than 50 lines after minifying"
  exit 43
fi

if grep -q "unretained_comment" ${html}; then
  echo "${html}: should not retain normal comments"
  exit 43
fi

if ! grep -q "<!-- retained_comment" ${html}; then
  echo "${html}: should retain special comments"
  exit 43
fi

size=$(stat --format=%s ${html})
if test ${size} -gt 1000; then
  echo "minified file should be less than 1kb, was $size"
  exit 43
fi

if ! ../minify-html.sh ${html} ${html} ${html} ${html} &>> "${MOLK_TEST_LOG}"; then
  echo "should not fail on many files"
  exit 43
fi

molk_test_teardown
