#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 

cp resources/minify-feed.xml tmp
file=tmp/minify-feed.xml

# test KO

if ../minify-feed.sh &> /dev/null; then
  echo "should fail without arguments"
  exit 43
fi

if ../minify-feed.sh non-existing-feed.xml &> /dev/null; then
  echo "should fail on non-existing feed"
  exit 43
fi

# test OK

if ! ../minify-feed.sh ${file} &> /dev/null; then
  echo "${file} should not fail on one file"
  exit 43
fi

if grep -q "          " ${file}; then
  echo "${file} should no longer contain many consecutive spaces"
  exit 43
fi

if test $(cat ${file} | wc -l) -gt 50; then
  echo "${file} should be less than 50 lines after minifying"
  exit 43
fi

size=$(stat --format=%s "${file}")
if test ${size} -gt 1000; then
  echo "minified file should be less than 1kb, was $size"
  exit 43
fi

if ! ../minify-feed.sh ${file} ${file} ${file} ${file} &> /dev/null; then
  echo "should not fail on many files"
  exit 43
fi

molk_test_teardown
