#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

export MOLK_LIBRARIES=$(pwd)/../../libraries

cp -r resources/minify-js/* tmp/
to_minify=tmp/to-minify.js
source=tmp/not-to-minify.src.js
library=tmp/lib/not-to-minify.js

# test KO

if ../minify-js.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../minify-js.sh non-existing.js &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file"
  exit 43
fi

# test OK

if ! ../minify-js.sh "${to_minify}" &>> "${MOLK_TEST_LOG}"; then
  echo "${to_minify} should not fail"
  exit 43
fi

if grep -q "          " "${to_minify}"; then
  echo "${to_minify} should no longer contain many consecutive spaces"
  exit 43
fi

if grep -q "unretained-comment" "${to_minify}"; then
  echo "${to_minify} should no longer contain "unretained" comments"
  exit 43
fi

if ! grep -q "/\* retained_comment_1" "${to_minify}"; then
  echo "${to_minify} should retain single-line 'retained' comment 1"
  exit 43
fi

if ! grep -q "/\* retained_comment_2" "${to_minify}"; then
  echo "${to_minify} should retain multi-line 'retained' comment 2"
  exit 43
fi

if test $(cat "${to_minify}" | wc -l) -gt 5; then
  echo "${to_minify} should be less than 5 lines after minifying"
  exit 43
fi

size=$(stat --format=%s "${to_minify}")
if test ${size} -gt 100; then
  echo "minified file should be less than 100 bytes, was $size"
  exit 43
fi

if ! ../minify-js.sh "${to_minify}" "${to_minify}" &>> "${MOLK_TEST_LOG}"; then
  echo "Minifying several files and the same file over again should not fail"
  exit 43
fi

# Do not minify src-files

if ! ../minify-js.sh "${source}" &>> "${MOLK_TEST_LOG}"; then
  echo "${source} should not fail"
  exit 43
fi

if ! grep -q "/\* src-comment" "${source}"; then
  echo "${source} should retain src-comment"
  exit 43
fi

# Do not minify library files

if ! ../minify-js.sh "${library}" &>> "${MOLK_TEST_LOG}"; then
  echo "${library} should not fail"
  exit 43
fi

if ! grep -q "/\* lib-comment" "${library}"; then
  echo "${library} should retain lib-comment"
  exit 43
fi

molk_test_teardown
