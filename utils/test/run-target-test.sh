#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax

molk_test_setup

# setup 

existing_script=resources/run-target/existing.sh
nonexisting_script=resources/run-target/non-existing.sh
marker=${MOLK_TMP}/targets/existing
result=${MOLK_TMP}/existing-ran.txt

existing_script=resources/run-target/existing.sh
failing_script=resources/run-target/failing.sh
argument_script=resources/run-target/with-argument.sh
nonexisting_script=resources/run-target/non-existing.sh

# test KO

if ../run-target.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../run-target.sh ${nonexisting_script} &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing script"
  exit 43
fi

# test OK

if ../run-target.sh ${failing_script} &>> "${MOLK_TEST_LOG}"; then
  echo "'run-target ${failing_script}' should fail"
  exit 43
fi

if test -f ${marker}; then
  echo "run-target should not touch ${marker} when the script fails"
  exit 43
fi

if ! ../run-target.sh ${argument_script} test &>> "${MOLK_TEST_LOG}"; then
  echo "'run-target ${argument_script}' should not fail"
  exit 43
fi

if ! ../run-target.sh ${existing_script} &>> "${MOLK_TEST_LOG}"; then
  echo "'run-target ${existing_script}' should not fail"
  exit 43
fi

if ! test -f ${result}; then
  echo "run-target should have run (and created ${result})"
  exit 43
fi

if ! test -f ${marker}; then
  echo "run-target should touch ${marker} when it runs"
  exit 43
fi

rm ${result}
if ! ../run-target.sh ${existing_script} &>> "${MOLK_TEST_LOG}"; then
  echo "rerunning 'run-target ${existing_script}' should not fail"
  exit 43
fi

if test -f ${result}; then
  echo "rerunning 'run-target ${existing_script}' should not run the script"
  exit 43
fi

molk_test_teardown
