#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax

echo -e "\nOffline feed check not supported" >&2
exit 0

#setup

export MOLK_LIBRARIES=$(pwd)/../../libraries
base=resources/check-feed
invalid=${base}/invalid.xml
valid=${base}/valid.xml
not_wellformed=${base}/not-wellformed.xml
not_rfc_conformant=${base}/not-rfc-conformant.xml
ignored=${base}/ignored.xml

# pre-check validation

if ! xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${valid} &> /dev/null; then
  echo "${valid} should pass validation"
  exit 43
fi

if xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${invalid} &> /dev/null; then
  echo "${invalid} should not pass validation"
  exit 43
fi

if xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${not_wellformed} &> /dev/null; then
  echo "${not_wellformed} should not pass validation"
  exit 43
fi

if xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${ignored} &> /dev/null; then
  echo "${ignored} should not pass validation"
  exit 43
fi

if ! xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${not_rfc_conformant} &> /dev/null; then
  echo "${not_rfc_conformant} should pass validation"
  exit 43
fi

# test KO

if ../check-feed.sh $not_wellformed &> /dev/null; then
  echo "${not_wellformed} should not pass"
  exit 43
fi

if ../check-feed.sh $invalid &> /dev/null; then
  echo "${invalid} should not pass"
  exit 43
fi

if ../check-feed.sh $not_rfc_conformant &> /dev/null; then
  echo "${not_rfc_conformant} should not pass"
  exit 43
fi

if ../check-feed.sh $valid $invalid &> /dev/null; then
  echo "Validating several files should fail if one is invalid"
  exit 43
fi

# test OK

if ! ../check-feed.sh $valid; then
  echo "${valid} should pass"
  exit 43
fi

if ! ../check-feed.sh $ignored; then
  echo "${ignored} is invalid but should be ignored"
  exit 43
fi

if ! (xmlstarlet val --err --relaxng  ${MOLK_UTILS}/resources/atom.rng ${valid} |& grep -q "Unimplemented block at relaxng.c:3824"); then
  echo -e "'Unimplemented block at relaxng.c:3824' message no longer present in output"
  exit 42
fi

molk_test_teardown
