#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

touch tmp/test.txt

# tests

if ../readonly.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail without arguments"
  exit 43
fi

if ../readonly.sh tmp/1.txt tmp/2.txt &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail with too many arguments"
  exit 43
fi

if ../readonly.sh tmp/test.txt &>> "${MOLK_TEST_LOG}"; then
  echo "Made file readonly - should only work for directories"
  exit 43
fi

if ! ../readonly.sh tmp; then
  echo "Could not make tmp readonly"
  exit 43
fi

if rm -f tmp/test.txt &>> "${MOLK_TEST_LOG}"; then
  echo "Could delete test.txt in directory"
  exit 43
fi

if touch tmp/test2.txt &>> "${MOLK_TEST_LOG}"; then
  echo "Could create new file in directory"
  exit 43
fi

if ! find tmp -exec chmod u+w {} \; ; then
  echo "Could not make directory writable again"
  exit 43
fi

molk_test_teardown
