#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

export MOLK_DOMAIN=mydomain

# KO

if ../window-title.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without args"
  exit 43
fi

if ../window-title.sh "abc.html" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with just the url argument"
  exit 43
fi

if ../window-title.sh "abc.html" "" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on empty title"
  exit 43
fi

if ../window-title.sh "abc/def" "Test" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on url not ending in .html"
  exit 43
fi

# OK
if ! ../window-title.sh "index.html" "title" &>> "${MOLK_TEST_LOG}"; then
  echo "should return OK with index.html and title arguments"
  exit 43
fi

# root cases
molk_assert "index.html" "mytitle (mydomain)" "$(../window-title.sh index.html mytitle)"
molk_assert "something.html" "mytitle (mydomain)" "$(../window-title.sh something.html mytitle)"
molk_assert "something-index.html" "mytitle (mydomain)" "$(../window-title.sh something-index.html mytitle)"

# one level down
molk_assert "a/index.html" "mytitle (mydomain)" "$(../window-title.sh a/index.html mytitle)"
molk_assert "a/something.html" "mytitle (mydomain: a)" "$(../window-title.sh a/something.html mytitle)"
molk_assert "a/something-index.html" "mytitle (mydomain: a)" "$(../window-title.sh a/something-index.html mytitle)"

# two level down
molk_assert "a/b/index.html" "mytitle (mydomain: a)" "$(../window-title.sh a/b/index.html mytitle)"
molk_assert "a/b/something.html" "mytitle (mydomain: a - b)" "$(../window-title.sh a/b/something.html mytitle)"
molk_assert "a/b/something-index.html" "mytitle (mydomain: a - b)" "$(../window-title.sh a/b/something-index.html mytitle)"

# three level down
molk_assert "a/b/c/index.html" "mytitle (mydomain: a - b)" "$(../window-title.sh a/b/c/index.html mytitle)"
molk_assert "a/b/c/something.html" "mytitle (mydomain: a - b - c)" "$(../window-title.sh a/b/c/something.html mytitle)"
molk_assert "a/b/c/something-index.html" "mytitle (mydomain: a - b - c)" "$(../window-title.sh a/b/c/something-index.html mytitle)"

molk_test_teardown
