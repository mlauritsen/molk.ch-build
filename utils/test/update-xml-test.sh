#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

export MOLK_TMP=$(pwd)/tmp

cp resources/update-xml.xml tmp

# test KO

if ../update-xml.sh &>> "${MOLK_TEST_LOG}"; then
  echo "no arguments should fail"
  exit 43
fi

# test OK

if ! ../update-xml.sh tmp/update-xml.xml "//*[@id=\"has-attribute\"]/@my-attribute" '<new-attribute-value>'; then
  echo "Updating attribute should not fail"
  exit 43
fi

if ! grep -q '<d id="has-attribute" my-attribute="&lt;new-attribute-value&gt;">' tmp/update-xml.xml; then
  echo "my-attribute should be set (and the new value should be encoded)"
  exit 43
fi

if ! ../update-xml.sh tmp/update-xml.xml "//*[@id=\"DoesNotExist\"]/@f" 'new-attribute-value'; then
  echo "Updating attribute of non-existing element should not fail"
  exit 43
fi

if ! ../update-xml.sh tmp/update-xml.xml "//*[@id=\"my-element\"]" '<new-element-value>'; then
  echo "Updating element should not fail"
  exit 43
fi

if ! grep -q '<e id="my-element">&lt;new-element-value&gt;</e>' tmp/update-xml.xml; then
  echo "my-element should have new contents (and it should be encoded)"
  exit 43
fi

molk_test_teardown
