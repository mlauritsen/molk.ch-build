#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup 20

# setup

cp -r resources/format-html/* tmp
base=tmp
valid=${base}/valid.html
code=${base}/code.html
whitespace=${base}/whitespace.html
entities=${base}/entities.html
amp=${base}/amp-error.html

invalid=${base}/invalid.html
invalid_bck=${base}/invalid-bck.html
cp "${invalid}" "${invalid_bck}"

not_wellformed=${base}/not-wellformed.html
not_wellformed_bck=${base}/not-wellformed-bck.html
cp "${not_wellformed}" "${not_wellformed_bck}"

ignored=${base}/ignored.html
if ! xmlstarlet val --err --list-bad --xsd http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd "${valid}"; then
  echo "Invalid test file: ${valid}"
  exit 43
fi

# test KO

if ../format-html.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail without arguments"
  exit 43
fi

if ../format-html.sh $not_wellformed &>> "${MOLK_TEST_LOG}"; then
  echo "${not_wellformed} should not pass - do not format file which is not wellformed"
  exit 43
fi

if ! diff "${not_wellformed}" "${not_wellformed_bck}"; then
  echo "File which is not wellformed should not be modified"
  exit 43
fi

if ! ../format-html.sh $invalid &>> "${MOLK_TEST_LOG}"; then
  echo "${invalid} can still be formatted"
  exit 43
fi

if ../format-html.sh $valid $not_wellformed &>> "${MOLK_TEST_LOG}"; then
  echo "Formatting several files should fail if one is not wellformed"
  exit 43
fi

# test OK

if ! ../format-html.sh $ignored; then
  echo "${ignored} is invalid but should be ignored"
  exit 43
fi

if ! ../format-html.sh $valid; then
  echo "${valid} should pass"
  exit 43
fi

if ! grep -q "        " ${valid}; then
  echo "${valid} should contain many consecutive spaces"
  exit 43
fi

if test $(cat ${valid} | wc -l) -lt 10; then
  echo "${valid} should be >10 lines after formatting"
  exit 43
fi

if ! ../format-html.sh $code; then
  echo "${code} should pass"
  exit 43
fi

lines=$(wc -l $code | cut -d' ' -f-1)
if test $lines -lt 50; then
  echo "${code} should be >50 lines, was $lines"
  exit 43
fi

size=$(stat --format=%s ${code})
if test $size -lt 5000; then
  echo "${code} should be >5k bytes, was ${size}"
  exit 43
fi

if ! ../format-html.sh $whitespace; then
  echo "${whitespace} should pass"
  exit 43
fi

if ! grep -q "</a> after-a" $whitespace; then
  echo "whitespace after </a> should be preserved"
  exit 43
fi

# e.g.: more</span> fun
if ! grep -q "</span> after-span" $whitespace; then
  echo "whitespace after </span> should be preserved"
  exit 43
fi

# e.g.: Dusse</span> (conclurator)
if ! grep -q "</span> (" $whitespace; then
  echo "whitespace between '</xxx>' and '(' should be preserved"
  exit 43
fi

# e.g.: either</span> / or
if ! grep -q "</span> /" $whitespace; then
  echo "whitespace between '</xxx>' and '/' should be preserved"
  exit 43
fi

# e.g.: wikipedia</a>)
if ! grep -q "</span>)" $whitespace; then
  echo "whitespace between '</xxx>' and ')' should be removed"
  exit 43
fi

# if one day, 'tidy --vertical-space yes' actually does something
#if ! tail --lines=+10 $whitespace | grep -qE "^$"; then
#  echo "Empty lines should be added for readability"
#  exit 43
#fi

if ! grep -qE "do not wrap long lines of {100,200}code" $whitespace; then
  echo "Long lines of preformatted code should not be wrapped"
  exit 43
fi

if ! ../format-html.sh $entities; then
  echo "${entities} should pass"
  exit 43
fi

#if grep -q "&quot;" $entities; then
#  echo "&quot; should be replaced by \""
#  exit 43
#fi

#if grep -q "&#39;" $entities; then
#  echo "&#39; should be replaced by '"
#  exit 43
#fi

if grep -q " > " $entities; then
  echo "Unmatched ' > ' should be replaced by &lt;"
  exit 43
fi

if ! grep -q "&amp;" $entities; then
  echo "'&amp;' should be preserved"
  exit 43
fi

if ! grep -q "&deg;" $entities; then
  echo "Named entity should be preserved"
  exit 43
fi

if ! grep -q "&#8217;" $entities; then
  echo "Numeric entity should be preserved"
  exit 43
fi

if ../format-html.sh $amp &>> "${MOLK_TEST_LOG}"; then
  echo "${amp} should not pass"
  exit 43
fi

molk_test_teardown
