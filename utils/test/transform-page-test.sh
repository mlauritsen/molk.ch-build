#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup 50

# setup 

export MOLK_CONTENT=${MOLK_TMP}/content
export MOLK_DEST=${MOLK_TMP}/dest
export MOLK_SRC_FRAGMENTS=${MOLK_TMP}/src-fragments
export MOLK_TMP_FRAGMENTS=${MOLK_TMP}/tmp-fragments
export MOLK_TRANSFORM_PAGE=${MOLK_TMP}/transform-page

cp -r resources/transform-page/* "${MOLK_TMP}"

src_page=${MOLK_CONTENT}/page1.html
dest_page=${MOLK_DEST}/page1.html

src_index=${MOLK_CONTENT}/index.html
dest_index=${MOLK_DEST}/index.html

if ! ${MOLK_UTILS}/check-html.sh ${src_page}; then
  echo "${src_page} should be valid"
  exit 43
fi

# test KO

if ../transform-page.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Transformation should fail without arguments"
  exit 43
fi

if ../transform-page.sh non-existing.html &>> "${MOLK_TEST_LOG}"; then
  echo "Transformation should fail on non-existing page"
  exit 43
fi

# test OK

if ! ../transform-page.sh ${src_page} ${dest_page} &>> "${MOLK_TEST_LOG}"; then
  echo "Transformation should succeed"
  exit 43
fi

if ! ${MOLK_UTILS}/check-html.sh ${dest_page}; then
  echo "${dest_page} should be valid"
  exit 43
fi

if ! grep -q "UTF-8" ${dest_page}; then
  echo "Page should contain html.tag"
  exit 43
fi

if ! grep -q "<h1>Test-html-title</h1>" ${dest_page}; then
  echo "Page should contain H1"
  exit 43
fi

if ! grep -q "<title>Test-html-title" ${dest_page}; then
  echo "Page should contain head/title"
  exit 43
fi

if ! grep -q ' content="text/html;' ${dest_page}; then
  echo "Page should contain head.xml"
  exit 43
fi

if ! grep -q "Test-html-body" ${dest_page}; then
  echo "Page should contain Test-html-body"
  exit 43
fi

if ! grep -q "Test-noscript" ${dest_page}; then
  echo "Page should contain Test-noscript"
  exit 43
fi

if grep -q "test-not-retained" ${dest_page}; then
  echo "Page should not contain test-not-retained"
  exit 43
fi

if ! grep -q "test-head" ${dest_page}; then
  echo "Page should contain test-head"
  exit 43
fi

if ! grep -q 'id="skip"' ${dest_page}; then
  echo "Page should contain skip-to-content link"
  exit 43
fi

if ! grep -q "test-logo" ${dest_page}; then
  echo "Page should contain test-logo"
  exit 43
fi

if ! grep -q ' id="crumbs"' ${dest_page}; then
  echo "Page should contain crumbs"
  exit 43
fi

if ! grep -q ' id="nav"' ${dest_page}; then
  echo "Page should contain a nav-element"
  exit 43
fi

if ! grep -q "test-nav-name" ${dest_page}; then
  echo "Page should contain test-nav-name"
  exit 43
fi

if ! grep -q ' id="hbar"' ${dest_page}; then
  echo 'Page should contain id="hbar"'
  exit 43
fi

if ! grep -q "test-sidebar" ${dest_page}; then
  echo "Page should contain test-sidebar"
  exit 43
fi

if ! grep -q "test-util-link" ${dest_page}; then
  echo "Page should contain test-util-link"
  exit 43
fi

if ! grep -q "test-util-name" ${dest_page}; then
  echo "Page should contain test-util-name"
  exit 43
fi

if grep -q "test-body-id" ${dest_page}; then
  echo "Page should not contain test-body-id"
  exit 43
fi

if ! grep -q "test-body-class-1" ${dest_page}; then
  echo "Page should contain test-body-class-1"
  exit 43
fi

if ! grep -q "test-body-class-2" ${dest_page}; then
  echo "Page should contain test-body-class-2"
  exit 43
fi

if ! grep -q "<p>Test-noscript" ${dest_page}; then
  echo "Page should contain no script fragment"
  exit 43
fi

if ! grep -q "test-footer" ${dest_page}; then
  echo "Page should contain test-footer"
  exit 43
fi

if ! grep -q "subject=Feedback" ${dest_page}; then
  echo "Page should contain feedback link"
  exit 43
fi

if ! grep -q '<a id="back-link" href="index.html">' ${dest_page}; then
  echo "Page should contain back-link"
  exit 43
fi

if ! grep -q '>Test-index-html-title<' ${dest_page}; then
  echo "Page should contain back-link title"
  exit 43
fi

if ! grep -q "Created: " ${dest_page}; then
  echo "Page should contain 'Created' info"
  exit 43
fi

if ! grep -q "Updated: " ${dest_page}; then
  echo "Page should contain 'Updated' info"
  exit 43
fi

# transforming the index

if ! ../transform-page.sh ${src_index} ${dest_index} &>> "${MOLK_TEST_LOG}"; then
  echo "Index transformation should succeed"
  exit 43
fi

if ! ${MOLK_UTILS}/check-html.sh ${dest_index}; then
  echo "${dest_index} should be valid"
  exit 43
fi

if grep -q 'id="back-link' ${dest_index}; then
  echo "Index should not contain 'back-link'"
  exit 43
fi

if grep -q 'created' ${dest_index}; then
  echo "Index should not contain 'created'"
  exit 43
fi

if grep -q 'updated' ${dest_index}; then
  echo "Index should not contain 'updated'"
  exit 43
fi

molk_test_teardown
