#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup
function assert_ok() {
  local date=$1
  local expected=$2
  local actual=$(../format-date.sh ${date})
  if test $? -ne 0; then
    echo "Failed on \"${date}\""
    exit 43
  fi

  if test "${expected}" != "${actual}"; then
    echo "Expected \"${expected}\", actual \"${actual}\""
    exit 43
  fi
}

function assert_ko() {
  local date=$1
  if ../format-date.sh ${date} &>> "${MOLK_TEST_LOG}"; then
    echo "\"${date}\": should fail"
    exit 43
  fi
}

# test KO

if ../format-date.sh &>> "${MOLK_TEST_LOG}"; then
  echo "No input should not succeed"
  exit 43
fi

if ../format-date.sh test &>> "${MOLK_TEST_LOG}"; then
  echo "Invalid input should not succeed"
  exit 43
fi

# year KO
assert_ko 0009-01-01
assert_ko 2909-01-01
assert_ko 2090-01-01
assert_ko 9999-01-01

# month KO
assert_ko 2000-00-01
assert_ko 2000-21-01
assert_ko 2000-19-01

# day KO
assert_ko 2000-01-00
assert_ko 2000-01-32

# test OK

assert_ok 2000-01-01 "January 2000"

assert_ok 2001-01-11 "January 2001"
assert_ok 2002-02-12 "February 2002"
assert_ok 2003-03-13 "March 2003"
assert_ok 2004-04-14 "April 2004"
assert_ok 2005-05-15 "May 2005"
assert_ok 2006-06-16 "June 2006"
assert_ok 2007-07-17 "July 2007"
assert_ok 2008-08-18 "August 2008"
assert_ok 2009-09-19 "September 2009"
assert_ok 2010-10-20 "October 2010"
assert_ok 2011-11-21 "November 2011"
assert_ok 2012-12-22 "December 2012"

assert_ok 2019-12-31 "December 2019"

molk_test_teardown
