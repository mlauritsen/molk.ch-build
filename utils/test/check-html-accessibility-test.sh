#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

base=resources/check-html-accessibility
ok=${base}/ok.html
ok_img=${base}/ok-img.html
ok_link=${base}/ok-link.html
ok_noscript=${base}/ok-noscript.html
ok_empty_tags=${base}/ok-empty-tags.html
ok_refs=${base}/ok-refs.html
ko=${base}/ko.html
ko_img=${base}/ko-img.html
ko_link=${base}/ko-link.html
ko_noscript=${base}/ko-noscript.html
ko_script=${base}/ko-script.html

# test KO

if ../check-html-accessibility.sh &>> "${MOLK_TEST_LOG}"; then
  echo "List of files is required"
  exit 43
fi

if ../check-html-accessibility.sh $ko &>> "${MOLK_TEST_LOG}"; then
  echo "${ko}: Should not pass"
  exit 43
fi

if ../check-html-accessibility.sh $ko_script &>> "${MOLK_TEST_LOG}"; then
  echo "${ko_script}: Should not pass"
  exit 43
fi

if ../check-html-accessibility.sh $ko_img &>> "${MOLK_TEST_LOG}"; then
  echo "${ko_img}: Should not pass"
  exit 43
fi

if ../check-html-accessibility.sh $ko_link &>> "${MOLK_TEST_LOG}"; then
  echo "${ko_link}: Should not pass"
  exit 43
fi

if ../check-html-accessibility.sh $ko_noscript &>> "${MOLK_TEST_LOG}"; then
  echo "${ko_noscript}: Should not pass"
  exit 43
fi

# test OK

if ! ../check-html-accessibility.sh $ok &>> "${MOLK_TEST_LOG}"; then
  echo "${ok}: Should pass"
  exit 43
fi

if ! ../check-html-accessibility.sh $ok_img &>> "${MOLK_TEST_LOG}"; then
  echo "${ok_img}: Should pass"
  exit 43
fi

if ! ../check-html-accessibility.sh $ok_link &>> "${MOLK_TEST_LOG}"; then
  echo "${ok_link}: Should pass"
  exit 43
fi

if ! ../check-html-accessibility.sh $ok_noscript &>> "${MOLK_TEST_LOG}"; then
  echo "${ok_noscript}: Should pass"
  exit 43
fi

if ! ../check-html-accessibility.sh $ok_empty_tags &>> "${MOLK_TEST_LOG}"; then
  echo "${ok_empty_tags}: Should pass"
  exit 43
fi

if ! ../check-html-accessibility.sh $ok_refs &>> "${MOLK_TEST_LOG}"; then
  echo "${ok_refs}: Should pass"
  exit 43
fi

molk_test_teardown
