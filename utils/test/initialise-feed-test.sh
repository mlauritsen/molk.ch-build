#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup 100

# setup 

export MOLK_CONTENT=$(pwd)/tmp/content
export MOLK_DEST=$(pwd)/tmp/dest
export MOLK_URL=http://molk-test.org
export MOLK_LIBRARIES=$(pwd)/../../libraries

cp -r resources/initialise-feed/* tmp
src_feed=${MOLK_CONTENT}/feed.xml
dest_feed=${MOLK_DEST}/feed.xml
src_html_feed=${MOLK_CONTENT}/feed.html
dest_html_feed=${MOLK_DEST}/feed.html
src_feed2=${MOLK_CONTENT}/excluded/feed.xml
dest_feed2=${MOLK_DEST}/excluded/feed.xml
src_empty_feed=${MOLK_CONTENT}/empty-feed/feed.xml
dest_empty_feed=${MOLK_DEST}/empty-feed/feed.xml
no_html_feed=${MOLK_CONTENT}/no-html/feed.xml
html_feed_exists_generated=${MOLK_CONTENT}/html-exists-generated/feed.xml
html_feed_exists_not_generated=${MOLK_CONTENT}/html-exists-not-generated/feed.xml
incomplete_html_feed=${MOLK_CONTENT}/incomplete-html-feed/feed.xml

if ! ${MOLK_UTILS}/check-feed-online.sh ${src_feed}; then
  echo "${src_feed} should be valid"
  exit 43
fi

# test KO

if ../initialise-feed.sh >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Transformation should fail without arguments"
  exit 43
fi

if ../initialise-feed.sh non-existing.xml >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Transformation should fail on non-existing feed"
  exit 43
fi

if ../initialise-feed.sh ${MOLK_CONTENT}/rejected-feed.xml >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Transformation should fail on feed which is not called feed.xml"
  exit 43
fi

# test OK

if ! ../initialise-feed.sh ${src_feed} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Feed initialisation should succeed"
  exit 43
fi

if ! ${MOLK_UTILS}/check-feed-online.sh ${dest_feed}; then
  echo "${dest_feed} should be valid"
  exit 43
fi

if ! grep -q test-feed-title-1 ${dest_feed}; then
  echo "Feed should contain test-feed-title-1"
  exit 43
fi

if ! grep -q Test-feed-description-1 ${dest_feed}; then
  echo "Feed should contain Test-feed-description-1"
  exit 43
fi

if ! grep -q test-entry-title-1 ${dest_feed}; then
  echo "Feed should contain test-entry-title-1"
  exit 43
fi

if ! grep -q test-entry-summary-1 ${dest_feed}; then
  echo "Feed should contain test-entry-summary-1"
  exit 43
fi

if ! grep -q "src=\"http://molk-test.org/test1.html\"" ${dest_feed}; then
  echo "Feed should contain test1 URL"
  exit 43
fi

if ! grep -q "xml:base=\"http://molk-test.org\"" ${dest_feed}; then
  echo "Feed should contain test1 xml:base"
  exit 43
fi

if ! head ${dest_feed} | grep -q "<updated>2002-02-20"; then
  echo "Feed update should be the update date of the entry"
  exit 43
fi

if ! grep -q test-entry-title-2 ${dest_feed}; then
  echo "Feed should contain the subdir page title 'test-entry-title-2'"
  exit 43
fi

if ! grep -q "src=\"http://molk-test.org/subdir/test2.html\"" ${dest_feed}; then
  echo "Feed should contain test2 URL"
  exit 43
fi

if ! grep -q "xml:base=\"http://molk-test.org/subdir\"" ${dest_feed}; then
  echo "Feed should contain test2 xml:base"
  exit 43
fi

if grep -q test-entry-title-3 ${dest_feed}; then
  echo "Feed should not contain the excluded page title 'test-entry-title-3'"
  exit 43
fi

if grep -q test-ignored-todo-entry-title ${dest_feed}; then
  echo "Feed should not contain the ignored page title 'test-ignored-todo-entry-title'"
  exit 43
fi

if grep -q test-ignored-todo-entry-summary ${dest_feed}; then
  echo "Feed should not contain the ignored page summary 'test-ignored-todo-entry-summary'"
  exit 43
fi

if grep -q test-ignored-todo-entry-content ${dest_feed}; then
  echo "Feed should not contain the ignored page content 'test-ignored-todo-entry-content'"
  exit 43
fi

if grep -q test-ignored-nonmolk-entry-title ${dest_feed}; then
  echo "Feed should not contain the ignored page title 'test-ignored-nonmolk-entry-title'"
  exit 43
fi

if grep -q test-ignored-nonmolk-entry-summary ${dest_feed}; then
  echo "Feed should not contain the ignored page summary 'test-ignored-nonmolk-entry-summary'"
  exit 43
fi

if grep -q test-ignored-nonmolk-entry-content ${dest_feed}; then
  echo "Feed should not contain the ignored page content 'test-ignored-nonmolk-entry-content'"
  exit 43
fi

if grep -q test-ignored-meta-entry-title ${dest_feed}; then
  echo "Feed should not contain the ignored page title 'test-ignored-meta-entry-title'"
  exit 43
fi

if grep -q test-ignored-meta-entry-summary ${dest_feed}; then
  echo "Feed should not contain the ignored page summary 'test-ignored-meta-entry-summary'"
  exit 43
fi

if grep -q test-ignored-meta-entry-content ${dest_feed}; then
  echo "Feed should not contain the ignored page content 'test-ignored-meta-entry-content'"
  exit 43
fi

if grep -q -e 'class="[^"]*summary[^"]*"' ${dest_feed}; then
  echo "Feed should not contain any elements with the class 'summary'"
  exit 43
fi

if ! test -s ${src_html_feed}; then
  echo "src HTML feed should exist and be non-empty"
  exit 43
fi

if test -e ${dest_html_feed}; then
  echo "dest HTML feed should not exist"
  exit 43
fi

if ! grep -q -e "<?molk.*generated=\"true\".*?>" ${src_html_feed}; then
  echo 'src HTML feed should contain generated="true" molk attribute'
  exit 43
fi

if ! grep -q "<title>test-feed-title-1</title>" ${src_html_feed}; then
  echo "src HTML feed should contain feed metadata (title)"
  exit 43
fi

if ! grep -q test-entry-title-1 ${src_html_feed}; then
  echo "src HTML feed should contain feed entries (test-entry-title-1)"
  exit 43
fi

if ! ../initialise-feed.sh ${src_feed2} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Feed2 initialisation should succeed"
  exit 43
fi

if ! ${MOLK_UTILS}/check-feed-online.sh ${dest_feed2}; then
  echo "${dest_feed2} should be valid"
  exit 43
fi

if ! grep -q test-feed-title-2 ${dest_feed2}; then
  echo "Feed2 should contain test-feed-title-2"
  exit 43
fi

if grep -q test-entry-title-1 ${dest_feed2}; then
  echo "Feed2 should not contain test-entry-title-1"
  exit 43
fi

if grep -q test-entry-title-2 ${dest_feed2}; then
  echo "Feed2 should not contain test-entry-title-2"
  exit 43
fi

if ! grep -q test-entry-title-3 ${dest_feed2}; then
  echo "Feed2 should contain test-entry-title-3"
  exit 43
fi

if ! grep -q "src=\"http://molk-test.org/excluded/test3.html\"" ${dest_feed2}; then
  echo "Feed2 should contain test3 URL"
  exit 43
fi

if ! grep -q "xml:base=\"http://molk-test.org/excluded\"" ${dest_feed2}; then
  echo "Feed2 should contain test3 xml:base"
  exit 43
fi

if ! grep -q test-entry-title-4 ${dest_feed2}; then
  echo "Feed2 should contain test-entry-title-4"
  exit 43
fi

if ! grep -q "src=\"http://molk-test.org/excluded/subdir2/test4.html\"" ${dest_feed2}; then
  echo "Feed2 should contain test4 URL"
  exit 43
fi

if ! grep -q "xml:base=\"http://molk-test.org/excluded/subdir2\"" ${dest_feed2}; then
  echo "Feed2 should contain test4 xml:base"
  exit 43
fi

if ! ../initialise-feed.sh ${src_empty_feed} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${src_empty_feed} should not fail"
  exit 43
fi

if ! grep -q "2000-06-25T12:29:29Z" ${dest_empty_feed}; then
  echo "${dest_empty_feed}: should retain original updated-date"
  exit 43
fi

if grep -q "<entry>" ${dest_empty_feed}; then
  echo "dest empty feed should not remain empty"
  exit 43
fi

if ../initialise-feed.sh ${no_html_feed} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${no_html_feed} should fail"
  exit 43
fi

if ! ../initialise-feed.sh ${html_feed_exists_generated} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${html_feed_exists_generated} should not fail"
  exit 43
fi

if ../initialise-feed.sh ${html_feed_exists_not_generated} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${html_feed_exists_not_generated} should fail"
  exit 43
fi

if ! ls ${MOLK_TMP}/*.html-* >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${html_feed_exists_not_generated} should be backed up to tmp"
  exit 43
fi

if ../initialise-feed.sh ${incomplete_html_feed} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${incomplete_html_feed} should fail"
  exit 43
fi

export MOLK_CONTENT=$(pwd)/tmp/feed-with-duplicate-entries
feed_with_duplicate_entries=${MOLK_CONTENT}/feed.xml
if ! ../initialise-feed.sh ${feed_with_duplicate_entries} |& grep -q 'Duplicate entry found on update date 2002-01-01T12:34:56Z' >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${feed_with_duplicate_entries} should fail"
  exit 43
fi

export MOLK_CONTENT=$(pwd)/tmp/feed-with-generated-duplicate
feed_with_generated_duplicate=${MOLK_CONTENT}/feed.xml
if ! ../initialise-feed.sh ${feed_with_generated_duplicate} >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "${feed_with_generated_duplicate} should pass"
  exit 43
fi

molk_test_teardown
