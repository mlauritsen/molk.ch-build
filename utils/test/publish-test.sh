#!/bin/bash
# Copyright 2009-2016 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

TMP="$(pwd)/tmp"
export MOLK_UTILS="$(pwd)/.."
export MOLK_LOG="${TMP}/log.txt"

# Setup test new site
export MOLK_DEST="${TMP}/molk_dest"
mkdir "${MOLK_DEST}"
echo "<html><head> <title>Test</title></head><body>Tested on $(date)...</body></html>" > "${MOLK_DEST}/test.html"

# Override sitecopy sitename
export MOLK_DOMAIN='molk.ch-test'
# Override remote dir to the test location
export MOLK_FTP_REMOTE_DIR='/test/'

(
    unset MOLK_FTP_USERNAME
    if "${MOLK_UTILS}/publish.sh" &>> "${MOLK_TEST_LOG}"; then
	echo "Should fail on missing ftp username"
	exit 43
    fi
)

(
    unset MOLK_FTP_PASSWORD
    if "${MOLK_UTILS}/publish.sh" &>> "${MOLK_TEST_LOG}"; then
	echo "Should fail on missing ftp password"
	exit 43
    fi
)

(
    unset MOLK_FTP_SERVER
    if "${MOLK_UTILS}/publish.sh" &>> "${MOLK_TEST_LOG}"; then
	echo "Should fail on missing ftp servername"
	exit 43
    fi
)

(
    unset MOLK_FTP_REMOTE_DIR
    if "${MOLK_UTILS}/publish.sh" &>> "${MOLK_TEST_LOG}"; then
	echo "Should fail on missing remote dir"
	exit 43
    fi
)

# success
if ! "${MOLK_UTILS}/publish.sh"; then
    echo "Should publish test site"
    exit 43
fi

molk_test_teardown
