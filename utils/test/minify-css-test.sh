#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

cp resources/minify-css.css tmp
css=tmp/minify-css.css

# test KO

if ../minify-css.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../minify-css.sh non-existing.css &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file"
  exit 43
fi

# test OK

if ! ../minify-css.sh ${css} &>> "${MOLK_TEST_LOG}"; then
  echo "${css} should not fail"
  exit 43
fi

if grep -q "          " ${css}; then
  echo "${css} should no longer contain many consecutive spaces"
  exit 43
fi

if grep -q "unretained_comment" ${css}; then
  echo "${css} should no longer contain ordinary comments"
  exit 43
fi

if ! grep -q "/* retained_comment" ${css}; then
  echo "${css} should retain licence comment"
  exit 43
fi

if grep -q "non-existing-property" ${css}; then
  echo "${css} should no longer contain \"non-existing-property\""
  exit 43
fi

if grep -q "empty-selector" ${css}; then
  echo "${css} should no longer contain \"empty-selector\""
  exit 43
fi

if test $(cat ${css} | wc -l) -gt 5; then
  echo "${css} should be less than 5 lines after minifying"
  exit 43
fi

size=$(stat --format=%s ${css})
if test ${size} -gt 500; then
  echo "minified file should be less than 500 bytes, was $size"
  exit 43
fi

molk_test_teardown
