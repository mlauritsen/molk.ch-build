#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 
export MOLK_TMP=$(pwd)/tmp

cp resources/format-css.css tmp
css=tmp/format-css.css

# test KO

if ../format-css.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../format-css.sh ${css} non-existing.css &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file"
  exit 43
fi

# test OK

if ! ../format-css.sh ${css} &>> "${MOLK_TEST_LOG}"; then
  echo "${css} should not fail"
  exit 43
fi

if grep -q "          " ${css}; then
  echo "${css} should no longer contain many consecutive spaces"
  exit 43
fi

if ! grep -q "comment" ${css}; then
  echo "${css}: comments should be retained"
  exit 43
fi

if ! grep -q "non-existing-property" ${css}; then
  echo "${css}: \"non-existing-property\" should be retained"
  exit 43
fi

if grep -q "DIV" ${css}; then
  echo "${css}: \"div\"-case should be lowered"
  exit 43
fi

if ! grep -q "empty-selector" ${css}; then
  echo "${css}: \"empty-selector\" should be retained"
  exit 43
fi

if ! grep -q "padding-bottom:" ${css}; then
  echo "${css}: \"padding-bottom:\" property should be retained"
  exit 43
fi

if test $(cat ${css} | wc -l) -gt 20; then
  echo "${css} should be less than 20 lines after formatting"
  exit 43
fi

size=$(stat --format=%s ${css})
if test ${size} -gt 500; then
  echo "formatted file should be less than 500 bytes, was $size"
  exit 43
fi

molk_test_teardown
