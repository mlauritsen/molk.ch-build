#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

source ../html-tools.sh

# molk_create_html_item_link

if molk_create_html_item_link &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_create_html_item_link' should fail without args"
  exit 43
fi

if ! molk_create_html_item_link a b c d &>> "${MOLK_TEST_LOG}"; then
  echo "'molk_create_html_item_link' should succeed"
  exit 43
fi

molk_assert "answer link" '\n<li class="a"><a href="b" title="c">d</a></li>' "$(molk_create_html_item_link a b c d)"
molk_assert "answer link without class" '\n<li><a href="b" title="c">d</a></li>' "$(molk_create_html_item_link '' b c d)"
molk_assert "answer link with identical name and title" '\n<li class="a"><a href="b">c</a></li>' "$(molk_create_html_item_link a b c c)"
molk_assert "answer link without title" '\n<li class="a"><a href="b">c</a></li>' "$(molk_create_html_item_link a b '' c)"

molk_test_teardown
