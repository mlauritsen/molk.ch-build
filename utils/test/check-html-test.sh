#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup 20

# setup
base=resources/check-html
invalid=${base}/invalid.html
valid=${base}/valid.html
valid_with_dtd=${base}/valid-with-dtd.html
valid_without_xsd=${base}/valid-without-xsd.html
not_wellformed=${base}/not-wellformed.html
ignored=${base}/ignored.html
closing_start_tag_ok=${base}/closing-start-tag-ok.html
closing_start_tag_ko=${base}/closing-start-tag-ko.html
style_attribute=${base}/style-attribute.html
style_element=${base}/style-element.html
non_empty_script_element=${base}/non-empty-script-element.html
many_h1s=${base}/many-h1s.html
has_h5=${base}/has-h5.html
has_h6=${base}/has-h6.html
quote_start_tag=${base}/quote-start-tag.html
end_tag_quote=${base}/end-tag-quote.html
missing_summary=${base}/missing-summary.html
body_is_summary=${base}/body-is-summary.html
nested_summary=${base}/nested-summary.html
h2_missing_anchor=${base}/h2-missing-anchor.html
h3_missing_anchor=${base}/h3-missing-anchor.html
h4_missing_anchor=${base}/h4-missing-anchor.html
table_missing_summary=${base}/table-missing-summary.html
h1_period=${base}/h1-period.html
h2_period=${base}/h2-period.html
h3_period=${base}/h3-period.html
h4_period=${base}/h4-period.html
p_punctuation_ok=${base}/p-punctuation-ok.html
p_punctuation_ko=${base}/p-punctuation-ko.html
dd_punctuation_ok=${base}/dd-punctuation-ok.html
dd_punctuation_ko=${base}/dd-punctuation-ko.html
first_capital_ok=${base}/first-capital-ok.html
first_number_ok=${base}/first-number-ok.html
p_first_capital_ko=${base}/p-first-capital-ko.html
h1_first_capital_ko=${base}/h1-first-capital-ko.html
h2_first_capital_ko=${base}/h2-first-capital-ko.html
h3_first_capital_ko=${base}/h3-first-capital-ko.html
h4_first_capital_ko=${base}/h4-first-capital-ko.html
redirected_index_link_1=${base}/redirected-index-link-1.html
redirected_index_link_2=${base}/redirected-index-link-2.html
empty_link=${base}/empty-link.html
empty_anchor=${base}/empty-anchor.html

# For running individual tests, copy line from below here:
#<<test to run>>
# exit 0

# Parallel speeds execution up from 200s to 15s. Nice! Each invocation of xmlstarlet takes ~15s regardless of input, so this is the best that can be done without getting rid of xmlstarlet.
parallel --jobs 100 ::: <<EOF
# check test files
if ! xmlstarlet val --err --list-bad --xsd "${MOLK_LIBRARIES}/xml-validation/xhtml1-strict.xsd" ${valid} ${valid_without_xsd} ${closing_start_tag_ko} ${many_h1s} ${has_h5} ${has_h6} ${style_element} ${non_empty_script_element} ${quote_start_tag} ${end_tag_quote} ${missing_summary} ${body_is_summary} ${nested_summary} ${h2_missing_anchor} ${h3_missing_anchor} ${h4_missing_anchor} ${table_missing_summary} ${h1_period} ${h2_period} ${h3_period} ${h4_period} ${p_punctuation_ok} ${p_punctuation_ko} ${dd_punctuation_ko} ${dd_punctuation_ko} ${first_capital_ok} ${first_number_ok} ${p_first_capital_ko} ${h1_first_capital_ko} ${h2_first_capital_ko} ${h3_first_capital_ko} ${h4_first_capital_ko} ${redirected_index_link_1} ${redirected_index_link_2} ${empty_link} ${empty_anchor}; then echo "Validation of (XSD) test files failed"; exit 43; fi
if ! xmlstarlet val --err --list-bad --dtd "${MOLK_LIBRARIES}/xml-validation/xhtml1-strict.dtd" ${valid_with_dtd} >> "${MOLK_TEST_LOG}" 2>&1; then echo "Validation of (DTD) test files failed"; exit 43; fi
if   xmlstarlet val --err --list-bad --xsd "${MOLK_LIBRARIES}/xml-validation/xhtml1-strict.xsd" ${invalid} >> "${MOLK_TEST_LOG}" 2>&1;        then echo "XSD validation of bad file should fail"; exit 43; fi
if   xmlstarlet val --err --list-bad --dtd "${MOLK_LIBRARIES}/xml-validation/xhtml1-strict.dtd" ${invalid} >> "${MOLK_TEST_LOG}" 2>&1;        then echo "DTD validation of bad file should fail"; exit 43; fi
if ! ../check-html.sh $valid; then echo "${valid} should pass (was '${?}')"; exit 43; fi
# test KO
if ../check-html.sh >> "${MOLK_TEST_LOG}" 2>&1;                             then echo "should not pass without arguments"; exit 43; fi
if ../check-html.sh $not_wellformed >> "${MOLK_TEST_LOG}" 2>&1;             then echo "${not_wellformed} should not pass"; exit 43; fi
if ../check-html.sh $valid_with_dtd >> "${MOLK_TEST_LOG}" 2>&1;             then echo "${valid_with_dtd} should fail (was '${?}') - molk pages should not contain DOCTYPE declarations"; exit 43; fi
if ../check-html.sh $valid_without_xsd >> "${MOLK_TEST_LOG}" 2>&1;          then echo "${valid_without_xsd} should fail - molk pages should use XHTML XSD namespace"; exit 43; fi
if ../check-html.sh $invalid >> "${MOLK_TEST_LOG}" 2>&1;                    then echo "${invalid} should not pass"; exit 43; fi
if ../check-html.sh $valid $invalid >> "${MOLK_TEST_LOG}" 2>&1;             then echo "Validating several files should fail if one is invalid"; exit 43; fi
if ../check-html.sh ${closing_start_tag_ko} >> "${MOLK_TEST_LOG}" 2>&1;     then echo '"<abc/>" closing start tags should not be allowed'; exit 43; fi
if ../check-html.sh ${style_attribute} >> "${MOLK_TEST_LOG}" 2>&1;          then echo "style attributes should not be allowed"; exit 43; fi
if ../check-html.sh ${style_element} >> "${MOLK_TEST_LOG}" 2>&1;            then echo "style elements should not be allowed"; exit 43; fi
if ../check-html.sh ${non_empty_script_element} >> "${MOLK_TEST_LOG}" 2>&1; then echo "non-empty script elements should not be allowed"; exit 43; fi
if ../check-html.sh ${many_h1s} >> "${MOLK_TEST_LOG}" 2>&1;                 then echo "multiple h1 elements should not be allowed"; exit 43; fi
if ../check-html.sh ${has_h5} >> "${MOLK_TEST_LOG}" 2>&1;                   then echo "h5 elements should not be allowed"; exit 43; fi
if ../check-html.sh ${has_h6} >> "${MOLK_TEST_LOG}" 2>&1;                   then echo "h6 elements should not be allowed"; exit 43; fi
if ../check-html.sh ${quote_start_tag} >> "${MOLK_TEST_LOG}" 2>&1;          then echo '"<tag should be avoided'; exit 43; fi
if ../check-html.sh ${end_tag_quote} >> "${MOLK_TEST_LOG}" 2>&1;            then echo '</tag>" should be avoided'; exit 43; fi
if ../check-html.sh ${missing_summary} >> "${MOLK_TEST_LOG}" 2>&1;          then echo 'all pages should include at least one element with the "summary" class'; exit 43; fi
if ../check-html.sh ${h2_missing_anchor} >> "${MOLK_TEST_LOG}" 2>&1;        then echo 'all H2s should have an id so they can be linked to'; exit 43; fi
if ../check-html.sh ${h3_missing_anchor} >> "${MOLK_TEST_LOG}" 2>&1;        then echo 'all H3s should have an id so they can be linked to'; exit 43; fi
if ../check-html.sh ${h4_missing_anchor} >> "${MOLK_TEST_LOG}" 2>&1;        then echo 'all H4s should have an id so they can be linked to'; exit 43; fi
if ../check-html.sh ${table_missing_summary} >> "${MOLK_TEST_LOG}" 2>&1;    then echo 'tables should have a summary attribute describing the content'; exit 43; fi
if ../check-html.sh ${h1_period} >> "${MOLK_TEST_LOG}" 2>&1;                then echo 'H1 should not end in "."'; exit 43; fi
if ../check-html.sh ${h2_period} >> "${MOLK_TEST_LOG}" 2>&1;                then echo 'H2 should not end in "."'; exit 43; fi
if ../check-html.sh ${h3_period} >> "${MOLK_TEST_LOG}" 2>&1;                then echo 'H3 should not end in "."'; exit 43; fi
if ../check-html.sh ${h4_period} >> "${MOLK_TEST_LOG}" 2>&1;                then echo 'H4 should not end in "."'; exit 43; fi
if ../check-html.sh ${p_punctuation_ko} >> "${MOLK_TEST_LOG}" 2>&1;         then echo 'Paragraphs should end in punctuation'; exit 43; fi
if ../check-html.sh ${dd_punctuation_ko} >> "${MOLK_TEST_LOG}" 2>&1;        then echo 'DDs should end in punctuation'; exit 43; fi
if ../check-html.sh ${p_first_capital_ko} >> "${MOLK_TEST_LOG}" 2>&1;       then echo 'Paragraphs should start with a capital letter'; exit 43; fi
if ../check-html.sh ${h1_first_capital_ko} >> "${MOLK_TEST_LOG}" 2>&1;      then echo 'H1s should start with a capital letter'; exit 43; fi
if ../check-html.sh ${h2_first_capital_ko} >> "${MOLK_TEST_LOG}" 2>&1;      then echo 'H2s should start with a capital letter'; exit 43; fi
if ../check-html.sh ${h3_first_capital_ko} >> "${MOLK_TEST_LOG}" 2>&1;      then echo 'H3s should start with a capital letter'; exit 43; fi
if ../check-html.sh ${h4_first_capital_ko} >> "${MOLK_TEST_LOG}" 2>&1;      then echo 'H4s should start with a capital letter'; exit 43; fi
if ../check-html.sh ${redirected_index_link_1} >> "${MOLK_TEST_LOG}" 2>&1;  then echo 'Redirected link 1 should fail'; exit 43; fi
if ../check-html.sh ${redirected_index_link_2} >> "${MOLK_TEST_LOG}" 2>&1;  then echo 'Redirected link 2 should fail'; exit 43; fi
if ../check-html.sh ${empty_link} >> "${MOLK_TEST_LOG}" 2>&1;               then echo 'Empty link should fail'; exit 43; fi
if ../check-html.sh ${empty_anchor} >> "${MOLK_TEST_LOG}" 2>&1;             then echo 'Empty anchor should fail'; exit 43; fi
# test OK
if ! ../check-html.sh ${closing_start_tag_ok} >> "${MOLK_TEST_LOG}" 2>&1; then echo '"<abc />" closing start tags should be allowed'; exit 43; fi
if ! ../check-html.sh $valid;                                            then echo "${valid} should pass"; exit 43; fi
if ! ../check-html.sh $ignored;                                          then echo "${ignored} is invalid but should be ignored"; exit 43; fi
if ! ../check-html.sh ${body_is_summary} >> "${MOLK_TEST_LOG}" 2>&1;      then echo 'the body can be the summary'; exit 43; fi
if ! ../check-html.sh ${nested_summary} >> "${MOLK_TEST_LOG}" 2>&1;       then echo 'the summary can be arbitrarily deep inside the xhtml-tree'; exit 43; fi
if ! ../check-html.sh ${p_punctuation_ok} >> "${MOLK_TEST_LOG}" 2>&1;     then echo 'Paragraphs terminated by punctuation is ok'; exit 43; fi
if ! ../check-html.sh ${dd_punctuation_ok} >> "${MOLK_TEST_LOG}" 2>&1;    then echo 'DDs terminated by punctuation is ok'; exit 43; fi
if ! ../check-html.sh ${first_capital_ok} >> "${MOLK_TEST_LOG}" 2>&1;     then echo 'Paragraphs and headers starting with capital letter is ok'; exit 43; fi
if ! ../check-html.sh ${first_number_ok} >> "${MOLK_TEST_LOG}" 2>&1;      then echo 'Paragraphs and headers starting with number is ok'; exit 43; fi
EOF

molk_test_teardown
