#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# DEPRECATED - use molk-option-tools.sh!
exit 0

source test-utils.sh
check_script_under_test_syntax

# test KO
if ../extract-molk-option.sh resources/extract-molk-option-nonexisting.xml test &> /dev/null; then
  echo "Nonexisting input should not succeed"
  exit 43
fi

if ../extract-molk-option.sh resources/extract-molk-option.xml &> /dev/null; then
  echo "Option name is mandatory"
  exit 43
fi

if ../extract-molk-option.sh resources/extract-molk-option.xml double &> /dev/null; then
  echo "Should fail on duplicated attribute"
  exit 43
fi

if ../extract-molk-option.sh resources/extract-molk-option-missing.xml something &> /dev/null; then
  echo "Should fail on absent '<?molk... ?>' instruction"
  exit 43
fi

# test OK
non_existing=$(../extract-molk-option.sh resources/extract-molk-option.xml nonexisting)
if test $? -ne 0; then
  echo "Nonexisting options should not cause an error"
  exit 43
fi

if test "${non_existing}" != "false"; then
  echo "Nonexisting options should have the value \"false\" - was: \"${non_existing}\""
  exit 43
fi

empty=$(../extract-molk-option.sh resources/extract-molk-option.xml empty)
if test $? -ne 0; then
  echo "Present but empty options should not cause an error"
  exit 43
fi

if test "${empty}" != "false"; then
  echo "Present but empty options should have the value \"false\" - was: \"${empty}\""
  exit 43
fi

name=$(../extract-molk-option.sh resources/extract-molk-option.xml name)
if test $? -ne 0; then
  echo "Present options should not cause an error"
  exit 43
fi

if test "$name" != "test"; then
  echo "Present options should be parsed correctly - was \"${name}\""
  exit 43
fi

echo "tests OK: $0"
