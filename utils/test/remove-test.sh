#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

dir=tmp/remove
mkdir -p "${dir}"

# test KO
if ../remove.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Argument directory is mandatory"
  exit 43
fi

if ! ../remove.sh tmp/non-existing; then
  echo "Non-existing directory should be silent NOP"
  exit 43
fi

# test OK

echo "Hello world!" > "${dir}/test.txt"
chmod u-rwx "${dir}/test.txt"

if ! ../remove.sh "${dir}" &>> "${MOLK_TEST_LOG}"; then
  echo "Remove failed"
  exit 43
fi

if test -d "${dir}"; then
  echo "directory was not removed"
  exit 43
fi

molk_test_teardown
