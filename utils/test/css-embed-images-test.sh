#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup
export MOLK_TMP=$(pwd)/tmp
export MOLK_LIBRARIES=$(pwd)/../../libraries

dir=tmp/css-embed-images
image=${dir}/test.png
valid=${dir}/valid.css
invalid=${dir}/invalid.css
cp -r resources/css-embed-images tmp

# test KO

if ../css-embed-images.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../css-embed-images.sh non-existing.css &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file"
  exit 43
fi

if ../css-embed-images.sh "${invalid}" &>> "${MOLK_TEST_LOG}"; then
  echo "${invalid} should fail (missing image)"
  exit 43
fi

# test OK

image_size=$(stat --format=%s ${image})
css_size=$(stat --format=%s ${valid})

if ! ../css-embed-images.sh "${valid}" &>> "${MOLK_TEST_LOG}"; then
  echo "${valid} should not fail"
  exit 43
fi

embedded_size=$(stat --format=%s ${valid})

if test $embedded_size -lt $((image_size+css_size)); then
  echo "embedded css (${embedded_size}) should be larger than image (${image_css}) + css (${css_size})"
  exit 43
fi

molk_test_teardown
