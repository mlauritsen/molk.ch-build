#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

base=$(pwd)/resources/check-js
errors=${base}/errors.js
ok=${base}/ok.js
malformed=${base}/malformed.js
library=${base}/lib/errors.js
export MOLK_LIBRARIES=$(pwd)/../../libraries
export MOLK_TMP=$(pwd)/tmp

# test KO

if ../check-js.sh &>> "${MOLK_TEST_LOG}"; then
  echo "no arguments should fail"
  exit 43
fi

if ../check-js.sh $malformed &>> "${MOLK_TEST_LOG}"; then
  echo "${malformed} should not pass"
  exit 43
fi

if ../check-js.sh $errors &>> "${MOLK_TEST_LOG}"; then
  echo "${errors} should not pass"
  exit 43
fi

if ../check-js.sh $ok $errors &>> "${MOLK_TEST_LOG}"; then
  echo "Validating several files should fail if one is invalid"
  exit 43
fi

# test OK

if ! ../check-js.sh "${library}" &>> "${MOLK_TEST_LOG}"; then
  echo "Library file ${library} should pass despite errors"
  exit 43
fi

if ! ../check-js.sh "${ok}" &>> "${MOLK_TEST_LOG}"; then
  echo "${ok} should pass"
  exit 43
fi

molk_test_teardown
