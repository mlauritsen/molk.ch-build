#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 
export MOLK_SRC=resources/run-hook
success_script=success.sh
failure_script=failure.sh
nonexisting_script=nonexisting.sh
unreadable_script=unreadable.sh
unexecutable_script=unexecutable.sh

# test KO

if ../run-hook.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

# unreadable files are a pain to back up,
chmod ugo-r "${MOLK_SRC}/hooks/${unreadable_script}"
if ../run-hook.sh ${unreadable_script} &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on ${unreadable_script}"
  exit 43
fi

if ../run-hook.sh ${unexecutable_script} &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on ${unexecutable_script}"
  exit 43
fi
# so leave it readable when the tests are not running
chmod u+r "${MOLK_SRC}/hooks/${unreadable_script}"

# test OK

if ! ../run-hook.sh ${nonexisting_script} &>> "${MOLK_TEST_LOG}"; then
  echo "should succeed on ${nonexisting_script}, was ${?}"
  exit 43
fi

actual_output="$(../run-hook.sh ${nonexisting_script})"
if test "${actual_output}" != ""; then
  echo "should ignore ${nonexisting_script}, was: '${actual_output}'"
  exit 43
fi
unset actual_output

if ! ../run-hook.sh ${success_script} &>> "${MOLK_TEST_LOG}"; then
  echo "'${success_script}' should succeed, was $?"
  exit 43
fi

actual_output="$(../run-hook.sh ${success_script})"
expected_pattern="Executed: .*${success_script}$"
if [[ "${actual_output}" =~ "${expected_pattern}" ]]; then
  echo "${success_script} output - Expected: '${expected_pattern}' Actual: '${actual_output}'"
  exit 43
fi
unset actual_output
unset expected_pattern

if ../run-hook.sh ${failure_script} &>> "${MOLK_TEST_LOG}"; then
  echo "'${failure_script}' should fail, was $?"
  exit 43
fi

molk_test_teardown
