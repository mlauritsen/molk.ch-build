#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

cp -r resources/format-feed/* tmp
MOLK_UTILS=$(pwd)/..
base=tmp
invalid=${base}/invalid.xml
valid=${base}/valid.xml
not_wellformed=${base}/not-wellformed.xml
ignored=${base}/ignored.xml

# test KO

if ../format-feed.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail without arguments"
  exit 43
fi

if ../format-feed.sh $not_wellformed &>> "${MOLK_TEST_LOG}"; then
  echo "${not_wellformed} should not pass - cannot be formatted"
  exit 43
fi

if ! ../format-feed.sh $invalid &>> "${MOLK_TEST_LOG}"; then
  echo "${invalid} should pass - can be formatted anyway"
  exit 43
fi

if ../format-feed.sh $valid $not_wellformed &>> "${MOLK_TEST_LOG}"; then
  echo "Formatting several files should fail if one is not wellformed"
  exit 43
fi

# test OK

if ! ../format-feed.sh $ignored; then
  echo "${ignored} is invalid but should be ignored"
  exit 43
fi

if ! ../format-feed.sh $valid; then
  echo "${valid} should pass"
  exit 43
fi

if ! grep -q "        " ${valid}; then
  echo "${valid} should contain many consecutive spaces"
  exit 43
fi

molk_test_teardown
