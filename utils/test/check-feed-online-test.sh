#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

export MOLK_LIBRARIES=$(pwd)/../../libraries
export MOLK_TMP=$(pwd)/tmp
base=resources/check-feed
invalid=${base}/invalid.xml
valid=${base}/valid.xml
not_wellformed=${base}/not-wellformed.xml
not_rfc_conformant=${base}/not-rfc-conformant.xml
ignored=${base}/ignored.xml

# pre-check validation

if ! xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${valid} &>> "${MOLK_TEST_LOG}"; then
  echo "${valid} should pass validation"
  exit 43
fi

if xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${invalid} &>> "${MOLK_TEST_LOG}"; then
  echo "${invalid} should not pass validation"
  exit 43
fi

if xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${not_wellformed} &>> "${MOLK_TEST_LOG}"; then
  echo "${not_wellformed} should not pass validation"
  exit 43
fi

if xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${ignored} &>> "${MOLK_TEST_LOG}"; then
  echo "${ignored} should not pass validation"
  exit 43
fi

if ! xmlstarlet val --err --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${not_rfc_conformant} &>> "${MOLK_TEST_LOG}"; then
  echo "${not_rfc_conformant} should pass validation"
  exit 43
fi

# test KO

if ../check-feed-online.sh $not_wellformed &>> "${MOLK_TEST_LOG}"; then
  echo "${not_wellformed} should not pass"
  exit 43
fi

if ../check-feed-online.sh $invalid &>> "${MOLK_TEST_LOG}"; then
  echo "${invalid} should not pass"
  exit 43
fi

if ../check-feed-online.sh $not_rfc_conformant &>> "${MOLK_TEST_LOG}"; then
  echo "${not_rfc_conformant} should not pass"
  exit 43
fi

if ../check-feed-online.sh $valid $invalid &>> "${MOLK_TEST_LOG}"; then
  echo "Validating several files should fail if one is invalid"
  exit 43
fi

# test OK

if ! ../check-feed-online.sh $valid; then
  echo "${valid} should pass"
  exit 43
fi

if ! ../check-feed-online.sh $ignored; then
  echo "${ignored} is invalid but should be ignored"
  exit 43
fi

#TODO test that $invalid passes in offline mode...
if ! ../check-feed-online.sh $valid 2>/dev/null; then
  echo "${valid} should pass in offline mode"
  exit 43
fi

molk_test_teardown
