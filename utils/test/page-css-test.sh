#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

dir=resources/page-resources

# test KO

if ../page-css.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without args"
  exit 43
fi

if ../page-css.sh ${dir}/non-existing.html &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with non-existing argument"
  exit 43
fi

# test OK

if ! ../page-css.sh ${dir}/no-resources.html > tmp/result.xml; then
  echo "should not fail on no-resources.html"
  exit 43
fi

if test -s tmp/result.xml; then
  echo "no-resources.html: empty output expected"
  exit 43
fi

if ! ../page-css.sh ${dir}/has-css.html > tmp/result.xml; then
  echo "should not fail on has-css.html"
  exit 43
fi

if ! grep -q "\"has-css.css\"" tmp/result.xml; then
  echo "has-css.html: resource should be included"
  exit 43
fi

molk_test_teardown
