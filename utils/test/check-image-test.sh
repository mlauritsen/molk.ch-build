#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# Setup

base=$(pwd)/resources/check-image
non_existing="${base}/does-not-exist.png"
invalid_extension="${base}/invalid-extension.txt"
invalid_contents="${base}/invalid-contents.png"
clean_jpg="${base}/clean.jpg"
clean_png="${base}/clean.png"
phone="${base}/phone.jpg"

# test KO

if ../check-image.sh &>> "${MOLK_TEST_LOG}"; then
  echo "no arguments should fail"
  exit 43
fi

if ../check-image.sh $non_existing &>> "${MOLK_TEST_LOG}"; then
  echo "${non_existing} should not pass"
  exit 43
fi

if ../check-image.sh $invalid_extension &>> "${MOLK_TEST_LOG}"; then
  echo "${invalid_extension} should not pass"
  exit 43
fi

if ../check-image.sh $invalid_contents &>> "${MOLK_TEST_LOG}"; then
  echo "${_contentsinvalid} should not pass"
  exit 43
fi

if ../check-image.sh $phone &>> "${MOLK_TEST_LOG}"; then
  echo "${phone} should not pass"
  exit 43
fi

# test OK

if ! ../check-image.sh $clean_jpg &>> "${MOLK_TEST_LOG}"; then
  echo "${clean_jpg} should pass"
  exit 43
fi

if ! ../check-image.sh $clean_png &>> "${MOLK_TEST_LOG}"; then
  echo "${clean_png} should pass"
  exit 43
fi

molk_test_teardown
