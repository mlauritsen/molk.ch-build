#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 
export MOLK_TMP_FRAGMENTS=${MOLK_TMP}/get-nav/fragments

cp -r resources/get-nav "${MOLK_TMP}"

# test KO

# tmp file must exist
if ../get-nav.sh &>> "${MOLK_TEST_LOG}"; then
  echo "get-nav.sh expects an argument"
  exit 43
fi

if ../get-nav.sh 'non-existing/' &>> "${MOLK_TEST_LOG}"; then
  echo "non-existing section should fail"
  exit 43
fi

# test OK

top_level=$(../get-nav.sh '/')
if test $? -ne 0; then
  echo 'top-level directory should succeed'
  exit 43
fi

if test "${top_level}" != "<testinactivenav/>"; then
  echo 'top-level directory should output inactive nav'
  exit 43
fi
unset top_level

test_section=$(../get-nav.sh 'test/')
if test $? -ne 0; then
  echo '"testsection" should succeed'
  exit 43
fi

if test "${test_section}" != "<testnav/>"; then
  echo "'testsection' should output '<testnav/>', was: ${test_section}"
  exit 43
fi
unset test_section

rm "${MOLK_TMP_FRAGMENTS}/nav-inactive.xml" || exit 43
if ../get-nav.sh '/' &>> "${MOLK_TEST_LOG}"; then
  echo 'top-level directory should fail when there is no inactive fragment'
  exit 43
fi

molk_test_teardown
