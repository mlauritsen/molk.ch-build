#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax

source ../file-tools.sh

molk_test_setup

# KO

if molk_file_exists &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if molk_file_exists does-not-exist &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on single non-existing file"
  exit 43
fi

if molk_file_exists $0 does-not-exist &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on any non-existing file"
  exit 43
fi

# OK

if ! molk_file_exists $0 &>> "${MOLK_TEST_LOG}"; then
  echo "should succeed on existing file"
  exit 43
fi

if ! molk_file_exists $0 $0 &>> "${MOLK_TEST_LOG}"; then
  echo "should succeed on existing files"
  exit 43
fi

molk_test_teardown
