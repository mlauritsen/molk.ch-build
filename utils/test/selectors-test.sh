#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

base=$(pwd)/resources/selectors/javascript

empty=${base}/empty
ok_dir=${base}/ok
ok_file=${ok_dir}/ok.js
not_molk_dir=${base}/not-molk
not_molk_file=${not_molk_dir}/jquery-test.js

# test OK

if ! source ${MOLK_UTILS}/selectors.sh &>> "${MOLK_TEST_LOG}"; then
  echo "source ${MOLK_UTILS}/selectors.sh should pass"
  exit 43
fi

ok_selected=$(select_molk_js ${ok_dir})
if (($? != 0)); then
  echo "${ok_dir} should pass"
  exit 43
fi

if test "${ok_file}" != "${ok_selected}"; then
  echo "${ok_dir}: should select ${ok_file}, was: ${ok_selected}"
  exit 43
fi
unset ok_selected

empty_selected=$(select_molk_js ${empty})
if (($? != 0)); then
  echo "${empty} should pass"
  exit 43
fi

if test -n "${empty_selected}"; then
  echo "${empty}: should select nothing, was: '${empty_selected}'"
  exit 43
fi
unset empty_selected

not_molk_selected=$(select_molk_js ${not_molk_dir})
if (($? != 0)); then
  echo "${not_molk_dir} should pass"
  exit 43
fi

if test "${not_molk_file}" != "${not_molk_selected}"; then
  echo "${not_molk_dir}: should select ${not_molk_file}, was: '${not_molk_selected}'"
  exit 43
fi
unset not_molk_selected

molk_test_teardown
