#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# check the syntax of the SUT
source test-utils.sh
molk_check_script_syntax
molk_test_setup

# disable the options to test that core-utils.sh sets them
set +o nounset
set +o errexit
trap - ERR

if test -n "${abc}"; then
  echo 'Unbound variable should not fail before sourcing core-utils.sh'
  exit 43
fi

exec 2> /dev/null
if test -n "$(source ../core-utils.sh; echo xxx$xxx_unset_xxx 2>&1)";  then
  echo 'Should fail on reference to unbound variable'
  exit 43
fi

# the cd should cause an error message to be written and the subshell to exit, because errexit is enabled
unchecked_command_output=$({ source ../core-utils.sh; cd does_not_exist; echo 'should_not_get_this_far'; } 2>&1)

# if the trap was executed, a line containing "Error on line" appears on stdout (which is redirected to stdout)
if ! echo "${unchecked_command_output}" | grep -qE '^Error on line'; then
  echo 'Should write error message on unchecked simple command failure'
  exit 43
fi

# echo 'should_not_get_this_far' is not executed
if echo "${unchecked_command_output}" | grep -qE '^should_not_get_this_far'; then
  echo 'Should not continue executing after unchecked simple command failure'
  exit 43
fi

# log
molk_assert "log default" "MOLK-LOG ./core-utils-test.sh: Ignoring empty log entry" "$(molk_log 2>&1)"
molk_assert "log message" "MOLK-LOG ./core-utils-test.sh: abc" "$(molk_log abc 2>&1)"

# warn
molk_assert "log warning" "MOLK-LOG ./core-utils-test.sh: WARN - abc" "$(molk_warn abc 2>&1)"
molk_assert "log warning" "MOLK-LOG ./core-utils-test.sh: WARN - a b c" "$(molk_warn a b c 2>&1)"

# error
molk_assert "log error" "MOLK-LOG ./core-utils-test.sh: ERROR - abc" "$(molk_error abc 2>&1 || true)"
molk_assert "log error" "MOLK-LOG ./core-utils-test.sh: ERROR - a b c" "$(molk_error a b c 2>&1 || true)"

if ! "$(molk_error abc)" && test "$?" == 42; then
  echo 'molk_error should exit with rc 42'
  exit 43
fi

molk_test_teardown
