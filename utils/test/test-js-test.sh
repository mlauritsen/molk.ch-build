#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# Relative to absolute path
function mlk_absolute() {
    echo "$(cd $(dirname ${1}); pwd)/$(basename ${1})"
}

# Placeholder substitution
function mlk_substitute() {
    local file=$1
    local placeholder=$2
    local value=$(echo "$3" | sed --expression='s/\//\\\//g' | sed --expression='s/\./\\\./g')
     sed --in-place --expression="s/${placeholder}/${value}/g" "${file}"
}

# setup 

export MOLK_LOG="${MOLK_TMP}/molk.log"
export MOLK_LIBRARIES=../../libraries
MOLK_JAVASCRIPT="${MOLK_LIBRARIES}"/javascript

declare -r TEST_LOG="${MOLK_TMP}/test-js.log"
declare -r JQUERY_JS="file://$(mlk_absolute ${MOLK_JAVASCRIPT}/jquery.js)"
declare -r QUNIT_JS="file://$(mlk_absolute ${MOLK_JAVASCRIPT}/qunit.js)"

cp -r resources/test-js/* tmp/
non_existing="$(pwd)/tmp/non-existing.html"
non_absolute="tmp/non-absolute.html"
no_qunit="$(pwd)/tmp/no-qunit.html"
no_tests="$(pwd)/tmp/no-tests.html"
test_ok="$(pwd)/tmp/test-ok.html"
url="file://${test_ok}"
test_ko="$(pwd)/tmp/test-ko.html"
test_error="$(pwd)/tmp/test-error.html"

mlk_substitute "${test_ok}" 'JQUERY_PLACEHOLDER' "${JQUERY_JS}"
mlk_substitute "${test_ok}" 'QUNIT_PLACEHOLDER' "${QUNIT_JS}"

mlk_substitute "${test_ko}" 'JQUERY_PLACEHOLDER' "${JQUERY_JS}"
mlk_substitute "${test_ko}" 'QUNIT_PLACEHOLDER' "${QUNIT_JS}"

mlk_substitute "${test_error}" 'JQUERY_PLACEHOLDER' "${JQUERY_JS}"
mlk_substitute "${test_error}" 'QUNIT_PLACEHOLDER' "${QUNIT_JS}"

# test KO

if ../test-js.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../test-js.sh "${non_existing}" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file (${non_existing})"
  exit 43
fi

if ../test-js.sh "${non_absolute}" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-absolute (${non_absolute})"
  exit 43
fi

if ../test-js.sh "${url}" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on url (${url})"
  exit 43
fi

# test OK

if ! ../test-js.sh "${no_qunit}" &>> "${MOLK_TEST_LOG}"; then
  echo "should ignore ${no_qunit}"
  exit 43
fi

if ! ../test-js.sh "${test_ok}" &>> "${MOLK_TEST_LOG}"; then
  echo "tests in ${test_ok} should pass"
  exit 43
fi

if ../test-js.sh "${test_ko}" &>> "${MOLK_TEST_LOG}"; then
  echo "tests in ${test_ko} should not pass"
  exit 43
fi

if ! grep -q 'Test should fail' "${TEST_LOG}"; then
  echo "Test failure message 'Test should fail' should be in the log"
  exit 43
fi

if ../test-js.sh "${test_error}" &>> "${MOLK_TEST_LOG}"; then
  echo "tests in ${test_error} should not pass"
  exit 43
fi

if ! grep -q 'Test should throw an exception' "${TEST_LOG}"; then
  echo "Test exception message 'Test should throw an exception' should be in the log"
  exit 43
fi

molk_test_teardown
