#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

source ../back-link.sh

index=
index_html=index.html
other=other.html
subdir_index=subdir
subdir_index_html=subdir/index.html
subdir_other=subdir/other.html

# test KO

if molk_back_link &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail without arguments"
  exit 43
fi

( # local invalid CONTENT test
    export MOLK_CONTENT=$(pwd)/resources/back-link/invalid

    if molk_back_link "${other}" &>> "${MOLK_TEST_LOG}"; then
	echo "invalid (missing index) other should not pass"
	exit 43
    fi
)

# test valid
export MOLK_CONTENT=$(pwd)/resources/back-link/valid

if ! molk_back_link "${index}"; then
  echo "'${index}' should pass"
  exit 43
fi

if test -n "${back_href}"; then
  echo "'${index}' back link should be empty, was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "/${index}"; then
  echo "'/${index}' should pass"
  exit 43
fi

if test -n "${back_href}"; then
  echo "'/${index}' back link should be empty, was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "${index_html}"; then
  echo "'${index_html}' should pass"
  exit 43
fi

if test -n "${back_href}"; then
  echo "'${index_html}' back link should be empty, was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "/${index_html}"; then
  echo "'/${index_html}' should pass"
  exit 43
fi

if test -n "${back_href}"; then
  echo "'/${index_html}' back link should be empty, was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "${other}"; then
  echo "'${other}' should pass"
  exit 43
fi

if test "${back_name}" != 'Index Test'; then
  echo "'${other}' back link should contain 'Index Test', was: '${back_name}'"
  exit 43
fi

if test "${back_href}" != 'index.html'; then
  echo "'${other}' back link should contain '\"index.html\"', was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "/${other}"; then
  echo "'/${other}' should pass"
  exit 43
fi

if test "${back_name}" != 'Index Test'; then
  echo "'/${other}' back link name should be 'Index Test', was: '${back_name}'"
  exit 43
fi

if test "${back_href}" != 'index.html'; then
  echo "'/${other}' back link href should be 'index.html', was: '${back_name}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "${subdir_index}"; then
  echo "'${subdir_index}' should pass"
  exit 43
fi

if test "${back_name}" != 'Index Test'; then
  echo "'${subdir_index}' back link name should be 'Index Test', was: '${back_name}'"
  exit 43
fi

if test "${back_href}" != '../index.html'; then
  echo "'${subdir_index}' back link href should be '../index.html', was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "${subdir_index}/"; then
  echo "'${subdir_index}/' should pass"
  exit 43
fi

if test "${back_name}" != 'Index Test'; then
  echo "'${subdir_index}/' back link href should be 'Index Test', was: '${back_name}'"
  exit 43
fi

if test "${back_href}" != '../index.html'; then
  echo "'${subdir_index}/' back link name should be '../index.html', was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "${subdir_index_html}"; then
  echo "'${subdir_index_html}' should pass"
  exit 43
fi

if test "${back_name}" != 'Index Test'; then
  echo "'${subdir_index_html}' back link name should be 'Index Test', was: '${back_name}'"
  exit 43
fi

if test "${back_href}" != '../index.html'; then
  echo "'${subdir_index_html}' back link href should be '../index.html', was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

if ! molk_back_link "${subdir_other}"; then
  echo "'${subdir_other}' should pass"
  exit 43
fi

if test "${back_name}" != 'Subdir Index Test'; then
  echo "'${subdir_other}' back link name should be 'Subdir Index Test', was: '${back_name}'"
  exit 43
fi

if test "${back_href}" != 'index.html'; then
  echo "'${subdir_other}' back link name should be 'index.html', was: '${back_href}'"
  exit 43
fi
unset back_href
unset back_name

molk_test_teardown
