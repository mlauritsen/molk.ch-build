#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Locate utils
molk_pwd="$(realpath .)"
if [[ "${molk_pwd}" =~ utils\/test\/dependencies ]]; then
    MOLK_UTILS="${molk_pwd}/../.."
elif [[ "${molk_pwd}" =~ utils\/test ]]; then
    MOLK_UTILS="$(pwd)/.."
elif [[ "${molk_pwd}" =~ src\/test ]]; then
    MOLK_UTILS="$(pwd)/../utils"
else
    echo -e "\nDon't know how to run tests in ${molk_pwd}" >&2
    return 43
fi
MOLK_UTILS="$(realpath $MOLK_UTILS)"
export MOLK_UTILS

export MOLK_LIBRARIES="${MOLK_UTILS}"/../../libraries
molk_tmp="$(pwd)/tmp"
export MOLK_TMP="${molk_tmp}/molk-tmp"
export MOLK_DEST="${molk_tmp}/dest"
export MOLK_LOG="${molk_tmp}/molk-test.log"

export MOLK_TEST_TMP="${molk_tmp}/test-tmp"
export MOLK_TEST_LOG="${MOLK_LOG}"
export MOLK_TEST_RESOURCES="$(pwd)/resources"

# This is the time when molk_test_setup was last invoked
export MOLK_TEST_START_TIME
declare -i MOLK_TEST_START_TIME
# This is the max #seconds duration allowed for the test (default 5, -1 means "no timeout")
export MOLK_TEST_TIMEOUT
declare -i MOLK_TEST_TIMEOUT

source "${MOLK_UTILS}/core-utils.sh"

function molk_deprecated() {
    echo "$0 - deprecated: $1" >> "${MOLK_TEST_LOG}"
}

# check that the expected ($2) and the actual ($3) output matches
# otherwise display an error message ($1) and return an error rc (43)
function molk_assert() {
    if test $# -ne 3; then
	echo -e "\nUsage: molk_assert <<description>> <<expected>> <<actual>>" >&2
	return 43
    fi

    local description=$1
    local expected=$2
    local actual=$3
    if test "$expected" != "$actual"; then
	echo "Assertion \"$description\" failed:"
	echo "  Expected: \"$expected\""
	echo "  Actual:   \"$actual\""
	return 43
    fi
}

# check the bash syntax of the script without running it
function molk_check_script_syntax() {
    # check the executing script (e.g. myscript-test.sh)
    if ! eval '(bash -n "${0}")'; then
	echo "Invalid syntax: $0" >&2
	return 43
    fi

    # check the argument script (by default ../myscript.sh)
    if test $# == 1; then
	if test "${0}" == "${1}"; then
	    return 0;
	fi
	script="${1}"
    else
	base="$(basename $0)"
	script="../${base%-test.sh}.sh"
    fi
    if ! eval '(bash -n "${script}")'; then
	echo "Invalid syntax: ${script}" >&2
	return 43
    fi
}

# Setup the tmp dir, and direct logging there
function molk_test_setup() {
    # First create the TMP dirs so the log can be written
    if ! mkdir -p "${molk_tmp}"; then
	echo "Could not create molk_tmp: ${molk_tmp}" >&2
	return 43
    fi
    if ! mkdir -p "${MOLK_TMP}"; then
	echo "Could not create MOLK_TMP: ${MOLK_TMP}" >&2
	return 43
    fi
    if ! mkdir -p "${MOLK_TEST_TMP}"; then
	echo "Could not create MOLK_TEST_TMP: ${MOLK_TEST_TMP}" >&2
	return 43
    fi

    # log setup
    echo "molk_tmp=${molk_tmp}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_UTILS=${MOLK_UTILS}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_LIBRARIES=${MOLK_LIBRARIES}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_TMP=${MOLK_TMP}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_LOG=${MOLK_LOG}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_TEST_TMP=${MOLK_TEST_TMP}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_TEST_LOG=${MOLK_TEST_LOG}" >> "${MOLK_TEST_LOG}"
    echo "MOLK_TEST_RESOURCES=${MOLK_TEST_RESOURCES}" >> "${MOLK_TEST_LOG}"

    # Timeout setup - allowing for both start-time and timeout to be set before calling this function
    if test -z "${MOLK_TEST_START_TIME:-}" || test "${MOLK_TEST_START_TIME}" == 0; then
	MOLK_TEST_START_TIME="${SECONDS}"
    fi
    echo "Test start: ${MOLK_TEST_START_TIME}" >> "${MOLK_TEST_LOG}"

    if test -z "${MOLK_TEST_TIMEOUT:-}" || test "${MOLK_TEST_TIMEOUT}" == 0; then
	if test $# == 1; then
	    MOLK_TEST_TIMEOUT="${1}"
	else
	    MOLK_TEST_TIMEOUT=5
	fi
    fi
    echo "Test timeout: ${MOLK_TEST_TIMEOUT}" >> "${MOLK_TEST_LOG}"
    echo "Running $0 in ${MOLK_TEST_TMP} at $(date +%Y.%m.%d-%H.%M)" >> "${MOLK_TEST_LOG}"
}

# Delete the tmp dir and report test success
function molk_test_teardown() {
    if ! rm -rf "${molk_tmp}"; then
	echo "Could not remove molk-tmp: ${molk_tmp}" >&2
	return 43
    fi

    # Fail the test if it took longer than it's MOLK_TEST_TIMEOUT
    DURATION="$((${SECONDS} - ${MOLK_TEST_START_TIME}))"
    if test "${MOLK_TEST_TIMEOUT}" != -1; then
	if test "${DURATION}" -gt "${MOLK_TEST_TIMEOUT}"; then
	    echo "test timed out: $0 (duration: ${DURATION} seconds > ${MOLK_TEST_TIMEOUT} timeout)" >&2
	    return 43
	fi
    fi
    
    echo "tests OK: $0 (duration: ${DURATION} seconds)"
}
