#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup 
export MOLK_CONTENT=$(pwd)/resources/create-subnav/content

# test KO
if ../create-subnav.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without args"
  exit 43
fi

if ../create-subnav.sh nav1 nav2 &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with too many args"
  exit 43
fi

if ../create-subnav.sh "" &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with empty argument"
  exit 43
fi

if ../create-subnav.sh create-subnav.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail with non-directory argument"
  exit 43
fi

# test OK

if ! ../create-subnav.sh testnav > tmp/subnav-test.xml; then
  echo "resources/subnav failed"
  exit 43
fi

if ! test -s "tmp/subnav-test.xml"; then
  echo "result should not be empty"
  exit 43
fi

if ! grep -q " id=\"subnav\"" tmp/subnav-test.xml; then
  echo "the id of the ul should be 'subnav'"
  exit 43
fi

if ! grep -q '>Test1 Title<' tmp/subnav-test.xml; then
  echo "The title of the test1 index should be included"
  exit 43
fi

if ! grep -q ' href="/testnav/test1/index.html"' tmp/subnav-test.xml; then
  echo 'test1 should be an absolute link: href="/testnav/test1/index.html"'
  exit 43
fi

if ! grep ' class="' tmp/subnav-test.xml | grep -q 'activatable'; then
  echo 'subnav links should be activatable for set-active.sh'
  exit 43
fi

if grep -q test2 tmp/subnav-test.xml; then
  echo "test2 should not be included \(it\'s a file\)"
  exit 43
fi

if ! grep -q ' href="/testnav/test3/index.html"' tmp/subnav-test.xml; then
  echo "There should be a link to the index of test3 \(it\'s a directory with an index.html\)"
  exit 43
fi

if ! grep -q '>test3<' tmp/subnav-test.xml; then
  echo "test3 should be the title of the link, since there is no index with a title to use"
  exit 43
fi

if grep -q ' href="/testnav/test4/index.html"' tmp/subnav-test.xml; then
  echo "There should be no link to test4 \(it\'s a directory without an index.html\)"
  exit 43
fi

no_subnav=$(../create-subnav.sh no-subnav)
if test $? -ne 0; then
  echo "no-subnav failed"
  exit 43
fi

if test -n "${no_subnav}"; then
  echo "no_subnav should be empty, was: '${no_subnav}'"
  exit 43
fi
unset no_subnav

molk_test_teardown
