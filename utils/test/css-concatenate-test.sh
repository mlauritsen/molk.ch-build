#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup

dir=tmp/css-concatenate
empty=${dir}/empty
exists=${dir}/exists
ok_dir=${dir}/ok
ok_css=${dir}/ok.css
cp -r resources/css-concatenate tmp

# test KO

if ../css-concatenate.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../css-concatenate.sh non-existing &>> "${MOLK_TEST_LOG}"; then
  echo "should fail on non-existing file"
  exit 43
fi

if ../css-concatenate.sh "${empty}" &>> "${MOLK_TEST_LOG}"; then
  echo "${empty} should fail (empty)"
  exit 43
fi

if ../css-concatenate.sh "${exists}" &>> "${MOLK_TEST_LOG}"; then
  echo "${exists} should fail (css file exists in parent)"
  exit 43
fi

# test OK

if ! ../css-concatenate.sh "${ok_dir}" &>> "${MOLK_TEST_LOG}"; then
  echo "${ok_dir} should pass"
  exit 43
fi

if ! test -f "${ok_css}"; then
  echo "${ok_css} should be created"
  exit 43
fi

if ! grep -q "test_css" "${ok_css}"; then
  echo "${ok_css} should be created"
  exit 43
fi

molk_test_teardown
