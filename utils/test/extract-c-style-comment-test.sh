#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

base=resources/extract-c-style-comment
retained_comment_1=${base}/retained-comment-1.txt
retained_comment_2=${base}/retained-comment-2.txt
retained_comment_3=${base}/retained-comment-3.txt
unretained_comment=${base}/unretained-comment.txt
tmp="${MOLK_TEST_TMP}"/result.txt

# test KO

if ../extract-c-style-comment.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Missing options should fail"
  exit 43
fi

if ../extract-c-style-comment.sh non-exinsting.txt &>> "${MOLK_TEST_LOG}"; then
  echo "Nonexisting file should fail"
  exit 43
fi

# test OK

if ! ../extract-c-style-comment.sh ${retained_comment_1} 1> ${tmp} 2>> "${MOLK_TEST_LOG}"; then
  echo "${retained_comment_1}: should succeed"
  exit 43
elif ! grep -q 'retained_comment_1' ${tmp} &>> "${MOLK_TEST_LOG}"; then
  echo "${retained_comment_1}: comment should be retained"
  exit 43
fi

if ! ../extract-c-style-comment.sh ${retained_comment_2} 1> ${tmp} 2>> "${MOLK_TEST_LOG}"; then
  echo "${retained_comment_2}: should succeed"
  exit 43
elif ! grep -q 'retained_comment_2' ${tmp} &>> "${MOLK_TEST_LOG}"; then
  echo "${retained_comment_2}: comment should be retained"
  exit 43
fi

if ! ../extract-c-style-comment.sh ${retained_comment_3} 1> ${tmp} 2>> "${MOLK_TEST_LOG}"; then
  echo "${retained_comment_3}: should succeed"
  exit 43
elif ! grep -q 'retained_comment_3' ${tmp} &>> "${MOLK_TEST_LOG}"; then
  echo "${retained_comment_3}: comment should be retained"
  exit 43
fi

if ! ../extract-c-style-comment.sh ${unretained_comment} 1> ${tmp} 2>> "${MOLK_TEST_LOG}"; then
  echo "${unretained_comment}: should succeed"
  exit 43
fi

if grep -q 'unretained_comment' ${tmp} &>> "${MOLK_TEST_LOG}"; then
  echo "${unretained_comment}: comment should be retained"
  exit 43
fi

size=$(stat --format=%s ${tmp})
if test ${size} -gt 0; then
  echo "unretained comment should yield 0 bytes, was $size"
  exit 43
fi

molk_test_teardown
