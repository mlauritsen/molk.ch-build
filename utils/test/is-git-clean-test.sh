#!/bin/bash
# Copyright 2009-2016 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# Setup "remote" repo
declare -r remote_repo="${MOLK_TEST_TMP}"/remote.git
git init --bare "${remote_repo}" >> "${MOLK_TEST_LOG}" 2>&1

declare -r first_repo="${MOLK_TEST_TMP}"/first
declare -r first='first.txt'
(# Setup local and push initial commit, verify git repos are in expected states
git clone -- "${remote_repo}" "${first_repo}" >> "${MOLK_TEST_LOG}" 2>&1
cd "${first_repo}"
git config user.name "test"
git config user.email "test@test.test"
git config push.default simple
echo "First" > "${first}"
git add "${first}" >> "${MOLK_TEST_LOG}" 2>&1
git commit -m "first" >> "${MOLK_TEST_LOG}" 2>&1
git push >> "${MOLK_TEST_LOG}" 2>&1

# Working directory is clean
if ! git status | grep -q 'nothing to commit, working tree clean' >> "${MOLK_TEST_LOG}" 2>&1; then
    echo "Expected 'nothing to commit, working directory clean'"
    git status -s
  exit 43
fi

# Local branch is master
if ! git status | grep -q 'On branch master' >> "${MOLK_TEST_LOG}" 2>&1; then
    echo "Expected 'On branch master'"
    git status
  exit 43
fi

# Remote branch is origin/master
if ! git status | grep -q "Your branch is up-to-date with 'origin/master'" >> "${MOLK_TEST_LOG}" 2>&1; then
    echo "Expected: Your branch is up-to-date with 'origin/master'"
    git status
  exit 43
fi

# Nothing to push
if ! git push --dry-run |& grep -q 'Everything up-to-date' >> "${MOLK_TEST_LOG}" 2>&1; then
    echo "Expected 'Everything up-to-date'"
    git push --dry-run
  exit 43
fi

# Nothing to pull
if test -n "$(git pull --dry-run)"; then
    echo "Expected nothing to do from git pull"
    git pull --dry-run
  exit 43
fi
)

if "${MOLK_UTILS}/is-git-clean.sh" >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Should fail on missing argument"
  exit 43
fi

if "${MOLK_UTILS}/is-git-clean.sh" doesNotExist >> "${MOLK_TEST_LOG}" 2>&1; then
  echo "Should fail on non-existing directory"
  exit 43
fi

if "${MOLK_UTILS}/is-git-clean.sh" "${MOLK_TEST_TMP}" >> "${MOLK_TEST_LOG}" 2>&1; then
    echo "Should fail on non-git directory"
    exit 43
fi

(# Accept clean git directory
    clean_repo="${MOLK_TEST_TMP}"/clean
    git clone -- "${remote_repo}" "${clean_repo}" >> "${MOLK_TEST_LOG}" 2>&1
    cd "${clean_repo}"
    git config user.name "test"
    git config user.email "test@test.test"
    git config push.default simple
    cd - >> "${MOLK_TEST_LOG}" 2>&1

    if ! "${MOLK_UTILS}/is-git-clean.sh" "${clean_repo}" >> "${MOLK_TEST_LOG}" 2>&1; then
	echo "Should accept clean git directory"
	exit 43
    fi
)

(# Fail on untracked files
    untracked_repo="${MOLK_TEST_TMP}"/untracked
    git clone -- "${remote_repo}" "${untracked_repo}" >> "${MOLK_TEST_LOG}" 2>&1
    cd "${untracked_repo}"
    git config user.name "test"
    git config user.email "test@test.test"
    git config push.default simple

    file=untracked.txt
    echo "This file is not tracked" > "${file}"
    cd - >> "${MOLK_TEST_LOG}" 2>&1

    if "${MOLK_UTILS}/is-git-clean.sh" "${untracked_repo}" >> "${MOLK_TEST_LOG}" 2>&1; then
	echo "Should fail on untracked files"
	exit 43
    fi

    if ! "${MOLK_UTILS}/is-git-clean.sh" "${untracked_repo}" |& grep -q "${file}"; then
	echo "Should include untracked file name in error message"
	exit 43
    fi
)

(# Fail on unstaged changes
    unstaged_repo="${MOLK_TEST_TMP}"/unstaged
    git clone -- "${remote_repo}" "${unstaged_repo}" >> "${MOLK_TEST_LOG}" 2>&1
    cd "${unstaged_repo}"
    git config user.name "test"
    git config user.email "test@test.test"
    git config push.default simple

    echo "modifying tracked file" >> "${first}"
    cd - >> "${MOLK_TEST_LOG}" 2>&1

    if "${MOLK_UTILS}/is-git-clean.sh" "${unstaged_repo}" >> "${MOLK_TEST_LOG}" 2>&1; then
	echo "Should fail on unstaged files"
	exit 43
    fi

    if ! "${MOLK_UTILS}/is-git-clean.sh" "${unstaged_repo}" |& grep -q "${first}"; then
	echo "Should include unstaged file name in error message"
	exit 43
    fi
)

(# Fail on indexed (uncommitted) files
    indexed_repo="${MOLK_TEST_TMP}"/indexed
    git clone -- "${remote_repo}" "${indexed_repo}" >> "${MOLK_TEST_LOG}" 2>&1
    cd "${indexed_repo}"
    git config user.name "test"
    git config user.email "test@test.test"
    git config push.default simple

    file=indexed.txt
    echo "This file is indexed" > "${file}"
    git add "${file}"
    cd - >> "${MOLK_TEST_LOG}" 2>&1

    if "${MOLK_UTILS}/is-git-clean.sh" "${indexed_repo}" >> "${MOLK_TEST_LOG}" 2>&1; then
	echo "Should fail on uncommitted (indexed) files"
	exit 43
    fi

    if ! "${MOLK_UTILS}/is-git-clean.sh" "${indexed_repo}" |& grep -q "${file}"; then
	echo "Should include uncommitted (indexed) file name in error message"
	exit 43
    fi
)

(# Fail on unpushed commits
    unpushed_repo="${MOLK_TEST_TMP}"/unpushed
    git clone -- "${remote_repo}" "${unpushed_repo}" >> "${MOLK_TEST_LOG}" 2>&1
    cd "${unpushed_repo}"
    git config user.name "test"
    git config user.email "test@test.test"
    git config push.default simple

    file=unpushed.txt
    echo unpushed > "${file}"
    git add "${file}" 
    git commit -m "unpushed" >> "${MOLK_TEST_LOG}" 2>&1
    cd - >> "${MOLK_TEST_LOG}" 2>&1

    if "${MOLK_UTILS}/is-git-clean.sh" "${unpushed_repo}" >> "${MOLK_TEST_LOG}" 2>&1; then
	echo "Should fail on unpushed files"
	exit 43
    fi
)

(# Fail on unpulled commits
    unpulled_repo="${MOLK_TEST_TMP}"/unpulled
    (
	git clone -- "${remote_repo}" "${unpulled_repo}" >> "${MOLK_TEST_LOG}" 2>&1
	cd "${unpulled_repo}"
	git config user.name "test"
	git config user.email "test@test.test"
	git config push.default simple
    )

    (
	cd "${first_repo}"
	file=unpulled.txt
	echo unpulled > "${file}"
	git add "${file}" 
	git commit -m "unpulled" >> "${MOLK_TEST_LOG}" 2>&1
	git push >> "${MOLK_TEST_LOG}" 2>&1
    )

    if "${MOLK_UTILS}/is-git-clean.sh" "${unpulled_repo}" >> "${MOLK_TEST_LOG}" 2>&1; then
	echo "Should fail on unpulled files"
	exit 43
    fi
)

molk_test_teardown
