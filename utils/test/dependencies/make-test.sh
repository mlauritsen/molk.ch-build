#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that make is available on the system and superficially works as expected

source ../test-utils.sh

molk_test_setup
molk_check_script_syntax $0

# make should be on the path
which make > "${MOLK_TEST_LOG}"

ACTUAL="$(make --version | head -1 2> ${MOLK_TEST_LOG})"

# Check that it is GNU make
if ! [[ "${ACTUAL}" =~ GNU\ Make ]]; then
    echo "Expected GNU make, was '${ACTUAL}'"
    exit 43
fi

# Check that it is make 4.1
if ! [[ "${ACTUAL}" =~ \ 4\.1 ]]; then
    echo "Expected make 4.1, was '${ACTUAL}'"
    exit 43
fi

# Check make is being used in MOLK_UTILS scripts
if test -z "$(find ${MOLK_UTILS}/.. -name '*makefile*')"; then
    echo "No references to make found in ${MOLK_UTILS}/.."
    exit 43
fi

molk_test_teardown
