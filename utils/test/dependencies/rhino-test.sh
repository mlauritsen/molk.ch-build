#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that rhino is available on the system and superficially works as expected

source ../test-utils.sh

molk_test_setup
molk_check_script_syntax $0

# rhino should be on the path
which rhino > "${MOLK_TEST_LOG}"

# Check that it is rhino 7.52
ACTUAL="$(apt show rhino 2> ${MOLK_TEST_LOG} 1| grep Version 2> ${MOLK_TEST_LOG})"
if ! [[ "${ACTUAL}" =~ Version:\ 1\.7 ]]; then
    echo "Expected rhino 1.7, was '${ACTUAL}'"
    exit 43
fi

# Check rhino is being used in MOLK_UTILS scripts
if ! grep -q rhino "${MOLK_UTILS}"/*.sh; then
    echo "No references to rhino found in ${MOLK_UTILS} scripts"
    exit 43
fi

molk_test_teardown
