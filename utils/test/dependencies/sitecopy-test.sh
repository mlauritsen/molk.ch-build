#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that sitecopy is available on the system and superficially works as expected

source ../test-utils.sh

molk_test_setup
molk_check_script_syntax $0

# sitecopy should be on the path
which sitecopy > "${MOLK_TEST_LOG}"

ACTUAL="$(sitecopy --version | head -1 2> ${MOLK_TEST_LOG})"

# Check that it is sitecopy
if ! [[ "${ACTUAL}" =~ ^sitecopy ]]; then
    echo "Expected sitecopy, was '${ACTUAL}'"
    exit 43
fi

# Check that it is sitecopy 0.16.6
if ! [[ "${ACTUAL}" =~ \ 0\.16\.6 ]]; then
    echo "Expected sitecopy 0.16.6, was '${ACTUAL}'"
    exit 43
fi

# Check sitecopy is being used in MOLK_UTILS scripts
if ! grep -q sitecopy "${MOLK_UTILS}"/*.sh; then
    echo "No references to sitecopy found in ${MOLK_UTILS} scripts"
    exit 43
fi

molk_test_teardown
