#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that curl is available on the system and superficially works as expected

source ../test-utils.sh
molk_check_script_syntax $0
molk_test_setup


# curl should be on the path
which curl > "${MOLK_TEST_LOG}"

# Check that it is curl 7.52
ACTUAL="$(curl --version 2> ${MOLK_TEST_LOG})"
if ! [[ "${ACTUAL}" =~ curl\ 7\.52 ]]; then
    echo "Expected curl 7.52.x, was '${ACTUAL}'"
    exit 43
fi

# Check curl is being used in MOLK_UTILS scripts
if ! grep -q curl "${MOLK_UTILS}"/*.sh; then
    echo "No references to curl found in ${MOLK_UTILS} scripts"
    exit 43
fi

molk_test_teardown
