#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that git is available on the system and superficially works as expected

source ../test-utils.sh

molk_test_setup
molk_check_script_syntax $0

# xmlstarlet should be on the path
which xmlstarlet > "${MOLK_TEST_LOG}"

# Check that it is xmlstarlet 2.11
ACTUAL="$(xmlstarlet --version | head -1 2> ${MOLK_TEST_LOG})"
if ! [[ "${ACTUAL}" =~ ^1\.6\.1$ ]]; then
    echo "Expected xmlstarlet 1.6.1, was '${ACTUAL}'"
    exit 43
fi
unset ACTUAL

# Check xmlstarlet is being used in MOLK_UTILS scripts
if ! grep -q xmlstarlet "${MOLK_UTILS}"/*.sh; then
    echo "No references to xmlstarlet found in ${MOLK_UTILS} scripts"
    exit 43
fi

# Select entity value
SELECT="$(xmlstarlet sel --template --value-of "//test-entity-name[@test-attribute-name]" resources/xml-starlet-test.xml)"
if test "${SELECT}" != 'test-entity-value'; then
    echo "Expected 'test-entity-value', was '${SELECT}'"
    exit 43
fi
unset SELECT

# Select attribute value
SELECT="$(xmlstarlet sel --template --value-of "//test-entity-name/@test-attribute-name" resources/xml-starlet-test.xml)"
if test "${SELECT}" != 'test-attribute-value'; then
    echo "Expected 'test-attribute-value', was '${SELECT}'"
    exit 43
fi
unset SELECT

# Validate document (wellformed...)
if ! xmlstarlet val resources/xml-starlet-test.xml > ${MOLK_TEST_LOG}; then
    echo "Validation failed: $?"
    exit 43
fi

# Update entity value
UPDATE="$(xmlstarlet ed --update "//test-entity-name[@test-attribute-name]" --value "test-updated-value" resources/xml-starlet-test.xml)"
if ! [[ "${UPDATE}" =~ \>test-updated-value\< ]]; then
    echo "Expected 'test-updated-value', was '${UPDATE}'"
    exit 43
fi
unset UPDATE

# Update attribute value
UPDATE="$(xmlstarlet ed --update "//test-entity-name/@test-attribute-name" --value "test-updated-value" resources/xml-starlet-test.xml)"
if ! [[ "${UPDATE}" =~ test-attribute-name=\"test-updated-value\" ]]; then
    echo "Expected 'test-attribute-name=\"test-updated-value\"', was '${UPDATE}'"
    exit 43
fi
unset UPDATE

molk_test_teardown
