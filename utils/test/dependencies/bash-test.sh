#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that bash is available on the system and superficially works as expected

source ../test-utils.sh
molk_check_script_syntax $0

molk_test_setup

# bash should be on the path
which bash > "${MOLK_TEST_LOG}"

ACTUAL="$(bash --version | head -1 2> ${MOLK_TEST_LOG})"

# Check that it is GNU bash
if ! [[ "${ACTUAL}" =~ GNU\ bash ]]; then
    echo "Expected GNU bash, was '${ACTUAL}'"
    exit 43
fi

# Check that it is bash 4.4
if ! [[ "${ACTUAL}" =~ \ 4\.4\. ]]; then
    echo "Expected bash 4.4.x, was '${ACTUAL}'"
    exit 43
fi

# Check bash is being used in MOLK_UTILS scripts
if ! grep -q bash "${MOLK_UTILS}"/*.sh; then
    echo "No references to bash found in ${MOLK_UTILS} scripts"
    exit 43
fi

molk_test_teardown
