#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that emacs is available on the system and superficially works as expected

source ../test-utils.sh
molk_check_script_syntax $0
molk_test_setup

# emacs should be on the path
which emacs > "${MOLK_TEST_LOG}"

ACTUAL="$(emacs --version | head -1 2> ${MOLK_TEST_LOG})"

# Check that it is GNU emacs
if ! [[ "${ACTUAL}" =~ GNU\ Emacs ]]; then
    echo "Expected GNU emacs, was '${ACTUAL}'"
    exit 43
fi

# Check that it is bash 24.5.x
if ! [[ "${ACTUAL}" =~ \ 24\.5\. ]]; then
    echo "Expected bash 24.5.x, was '${ACTUAL}'"
    exit 43
fi

# Check emacs is being used in MOLK_UTILS scripts
if ! grep -q emacs "${MOLK_UTILS}"/*.sh; then
    echo "No references to emacs found in ${MOLK_UTILS} scripts"
    exit 43
fi

molk_test_teardown
