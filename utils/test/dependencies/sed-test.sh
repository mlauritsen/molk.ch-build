#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that sed is available on the system and superficially works as expected

source ../test-utils.sh

molk_test_setup
molk_check_script_syntax $0

# sed should be on the path
which sed > "${MOLK_TEST_LOG}"

# Check that it is GNU sed
ACTUAL="$(sed --version 2> ${MOLK_TEST_LOG})"
if ! [[ "${ACTUAL}" =~ GNU\ sed ]]; then
    echo "Expected GNU sed, was '${ACTUAL}'"
    exit 43
fi

if ! [[ "${ACTUAL}" =~ 4\.4 ]]; then
    echo "Expected version 4.4, was '${ACTUAL}'"
    exit 43
fi
unset ACTUAL

# Check sed is being used in MOLK_UTILS scripts
if ! grep -q sed "${MOLK_UTILS}"/*.sh; then
    echo "No references to sed found in ${MOLK_UTILS} scripts"
    exit 43
fi

REPLACE="$(echo 'abc' | sed 's/b/d/g')"
if test "${REPLACE}" != 'adc'; then
    echo "Should replace 'b' by 'd' to yield 'adc', was ${REPLACE}"
    exit 43
fi
unset REPLACE

WHITESPACE="$(echo 'a   b' | sed 's/[[:blank:]]/X/g')"
if test "${WHITESPACE}" != 'aXXXb'; then
    echo "Should replace whitespace by 'X' to yield 'aXXXb', was ${WHITESPACE}"
    exit 43
fi
unset WHITESPACE

NOT="$(echo 'aba' | sed 's/[^a]/X/g')"
if test "${NOT}" != 'aXa'; then
    echo "Should replace not-'a' by 'X' to yield 'aXa', was ${NOT}"
    exit 43
fi
unset NOT

LINE="$(echo 'abc' | sed 's/^abc$/X/g')"
if test "${LINE}" != 'X'; then
    echo "Should replace entire matched line by 'X', was ${LINE}"
    exit 43
fi
unset LINE

DELETE="$(echo -e 'a\nb' | sed '/b/d')"
if test "${DELETE}" != 'a'; then
    echo "Should delete lines containing 'b', was ${DELETE}"
    exit 43
fi
unset DELETE

molk_test_teardown
