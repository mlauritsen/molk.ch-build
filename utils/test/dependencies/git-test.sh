#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that git is available on the system and superficially works as expected

source ../test-utils.sh
molk_check_script_syntax $0
molk_test_setup

# git should be on the path
which git > "${MOLK_TEST_LOG}"

# Check that it is git 2.11
ACTUAL="$(git --version 2> ${MOLK_TEST_LOG})"
if ! [[ "${ACTUAL}" =~ git\ [a-z]+\ 2.11 ]]; then
    echo "Expected git 2.11, was '${ACTUAL}'"
    exit 43
fi

# Check git is being used in MOLK_UTILS scripts
if ! grep -q git "${MOLK_UTILS}"/*.sh; then
    echo "No references to git found in ${MOLK_UTILS} scripts"
    exit 43
fi

molk_test_teardown
