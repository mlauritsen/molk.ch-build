# Match a line containing just "a" followed by a line containing just "b", and nothing else
BEGIN { print "Begin!"; }
/^a$/ { print $0; found_a = "true"; }
/^b$/ { if (found_a) { print  $0; exit 0; } }
/[^(^a$)(^b$)]/ { if (found_a) { print  $0; } }
END { print "End!"; }
