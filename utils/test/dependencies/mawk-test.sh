#!/bin/bash
# Copyright 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

# Tests that mawk is available and works as expected

source ../test-utils.sh
molk_check_script_syntax $0
molk_test_setup

#mawk should be on the path
which mawk > "${MOLK_TEST_LOG}"

# Check that mawk is mawk 1.3.3
ACTUAL="$(mawk -W version 2> ${MOLK_TEST_LOG})"
EXPECTED='mawk 1.3.3'
if ! [[ "${ACTUAL}" =~ "${EXPECTED}" ]]; then
    echo "Expected '${EXPECTED}', was '${ACTUAL}'"
    exit 43
fi

# Check mawk is being used in MOLK_UTILS scripts
if ! grep -q mawk "${MOLK_UTILS}"/*.sh; then
    echo "No references to mawk found in ${MOLK_UTILS} scripts"
    exit 43
fi

echo "a" | mawk -- '/a/ { exit 42 }' || RC=$?
if test $RC -ne 42; then
    echo "return code should be propagated when matching"
    exit 43
fi
unset RC

if ! echo "b" | mawk -- '/a/ { print $0; exit 42 }' > "${MOLK_TEST_LOG}"; then
    echo "return code is 0 when not matching"
    exit 43
fi

if ! test $(echo "abc" | mawk -- '/abc/ { print $0; exit 0 }') == "abc"; then
    echo "should match string"
    exit 43
fi

if ! test $(echo "abcd" | mawk -- '/bc/ { print $0; exit 0 }') == "abcd"; then
    echo "should match contained string"
    exit 43
fi

if echo "aababba" | mawk -- '/ab*ab*ab*a/ { exit 1 }'; then
    echo "should match * wildcard"
    exit 43
fi

if echo "ababba" | mawk -- '/ab+ab+a/ { exit 1 }'; then
    echo "should match + wildcard"
    exit 43
fi

if test -n "$(echo "ac" | mawk -- '/ab+c/ { print $0; exit 0 }')"; then
    echo "should not match + wildcard"
    exit 43
fi

if test "$(echo "abc" | mawk -- '/a[^d]c/ { print $0; exit 0 }')" != "abc"; then
    echo "should match 'not d'"
    exit 43
fi

if test "$(echo '/*! abc */' | mawk -- '/\/\*! abc \*\// { print $0; exit 0 }')" != '/*! abc */'; then
    echo "should match escaped special chars"
    exit 43
fi

if test $(echo "abc" | mawk -- '/^a/ { print $0; exit 0 }') != "abc"; then
    echo "should match start-of-line (^)"
    exit 43
fi

if test $(echo "abc" | mawk -- '/c$/ { print $0; exit 0 }') != "abc"; then
    echo "should match end-of-line ($)"
    exit 43
fi

if test $(echo "abc" | mawk -- '/^abc$/ { print $0; exit 0 }') != "abc"; then
    echo "should match entire line (^blah$)"
    exit 43
fi

if ! test -n $(echo " abc " | mawk -- '/^abc$/ { print $0; exit 0 }'); then
    echo "should not match entire line (^blah$)"
    exit 43
fi

if $(echo "abc" | mawk -- '/[a-z]*/ { exit 1 }'); then
    echo "should match the a-z character interval"
    exit 43
fi

if $(echo "ABC" | mawk -- '/[A-Z]*/ { exit 1 }'); then
    echo "should match the A-Z character interval"
    exit 43
fi

if $(echo " a b " | mawk -- '/ a b / { exit 1 }'); then
    echo "should match spaces"
    exit 43
fi

if ! echo "a b" | mawk -- '/a[:space:]b/ { exit 1 }'; then
    echo "should not match [:space:]"
    exit 43
fi

FILTERED="$(echo -e 'd\na\nc\nb\nd' | mawk -f resources/mawk-dep-test.awk)"
if ! echo "${FILTERED}" | grep -q 'Begin!'; then
    echo "should include 'Begin!', was: '${FILTERED}'"
    exit 43
elif ! echo "${FILTERED}" | grep -q 'a'; then
    echo "should include 'a', was: '${FILTERED}'"
    exit 43
elif ! echo "${FILTERED}" | grep -q 'b'; then
    echo "should include 'b', was: '${FILTERED}'"
    exit 43
elif ! echo "${FILTERED}" | grep -q 'c'; then
    echo "should include 'c', was: '${FILTERED}'"
    exit 43
elif ! echo "${FILTERED}" | grep -q 'End!'; then
    echo "should include 'End!', was: '${FILTERED}'"
    exit 43
fi
unset FILTERED

echo "tests OK: $0"
