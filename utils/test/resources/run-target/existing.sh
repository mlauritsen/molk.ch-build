#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt
set -o nounset

echo "existing!"

# touch a file to indicate the script ran
touch ${MOLK_TMP}/existing-ran.txt
