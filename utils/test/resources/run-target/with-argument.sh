#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt
set -o nounset

echo "with argument!"

if test $# -ne 1; then
  exit 1
fi

if test $1 != "test"; then
  exit 1
fi
