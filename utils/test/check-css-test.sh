#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup
export MOLK_LIBRARIES=$(pwd)/../../libraries
export MOLK_TMP=$(pwd)/tmp

base=$(pwd)/resources/check-css
errors=${base}/errors.css
warnings=${base}/warnings.css
ok=${base}/ok.css
dataprotocol=${base}/dataprotocol.css
malformed=${base}/malformed.css
library=${base}/lib/errors.css

# test KO

if ../check-css.sh &>> "${MOLK_TEST_LOG}"; then
  echo "no arguments should fail"
  exit 43
fi

if ../check-css.sh $malformed &>> "${MOLK_TEST_LOG}"; then
  echo "${malformed} should not pass"
  exit 43
fi

if ../check-css.sh $errors &>> "${MOLK_TEST_LOG}"; then
  echo "${errors} should not pass"
  exit 43
fi

#make offline css-validator output warnings

#if ../check-css.sh $warnings &>> "${MOLK_TEST_LOG}"; then
#  echo "${warnings} should not pass"
#  exit 43
#fi

if ../check-css.sh $ok $errors &>> "${MOLK_TEST_LOG}"; then
  echo "Validating several files should fail if one is invalid"
  exit 43
fi

# test OK

if ! ../check-css.sh ${ok} &>> "${MOLK_TEST_LOG}"; then
  echo "${ok} should pass"
  exit 43
fi

if ! ../check-css.sh ${library} &>> "${MOLK_TEST_LOG}"; then
  echo "${library} style with errors should pass"
  exit 43
fi

molk_test_teardown
