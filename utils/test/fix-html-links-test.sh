#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup
export MOLK_TMP=$(pwd)/tmp

cp resources/fix-empty-tags.xml tmp
touch tmp/not-writable.xml
chmod u-w tmp/not-writable.xml

# test KO

if ../fix-empty-tags.sh &>> "${MOLK_TEST_LOG}"; then
  echo "should fail without arguments"
  exit 43
fi

if ../fix-empty-tags.sh tmp/not-writable.xml &>> "${MOLK_TEST_LOG}"; then
  echo "Should fail if input is not a writable file"
  exit 43
fi

# test OK
if ! ../fix-empty-tags.sh tmp/fix-empty-tags.xml &>> "${MOLK_TEST_LOG}"; then
  echo "fix-empty-tags.sh should succeed"
  exit 43
fi

if ! grep -q "<empty />" tmp/fix-empty-tags.xml &>> "${MOLK_TEST_LOG}"; then
  echo "'<empty/>' should be reformatted"
  exit 43
fi

if ! grep -q '<emptyWithAttributes b="2" />' tmp/fix-empty-tags.xml &>> "${MOLK_TEST_LOG}"; then
  echo '"<emptyWithAttributes b="2"/>" should be reformatted'
  exit 43
fi

molk_test_teardown
