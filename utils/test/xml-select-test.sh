#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# setup
base=resources/xml-select
xml=${base}/test.xml
xhtml=${base}/test.html
atom=${base}/feed.xml

# test KO

if ../xml-select.sh &>> "${MOLK_TEST_LOG}"; then
  echo "Should not succeed without input"
  exit 43
fi

if ../xml-select.sh a &>> "${MOLK_TEST_LOG}"; then
  echo "Should not succeed without input"
  exit 43
fi

if ../xml-select.sh a b &>> "${MOLK_TEST_LOG}"; then
  echo "Should not succeed without input"
  exit 43
fi

if ../xml-select.sh a b c &>> "${MOLK_TEST_LOG}"; then
  echo "Should not succeed without input"
  exit 43
fi

if ../xml-select.sh non-existing.html copy xhtml '//*' &>> "${MOLK_TEST_LOG}"; then
  echo "Nonexisting input (with ns) should not succeed"
  exit 43
fi

if ../xml-select.sh ${xhtml} copy xxx '//*' &>> "${MOLK_TEST_LOG}"; then
  echo "Nonexisting namespace should not succeed"
  exit 43
fi

if ../xml-select.sh ${xhtml} xxx xhtml '()' &>> "${MOLK_TEST_LOG}"; then
  echo "Invalid select option should not succeed"
  exit 43
fi

if ../xml-select.sh ${xhtml} copy xhtml '()' &>> "${MOLK_TEST_LOG}"; then
  echo "Invalid xpath should not succeed"
  exit 43
fi

# test OK

if ! ../xml-select.sh "${xhtml}" copy xhtml 'xhtml:a'; then
  echo "xpath which does not match anything should succeed"
  exit 43
fi

not_found=$(../xml-select.sh ${xhtml} copy xhtml xhtml:a)
if test "${not_found}" != ""; then
  echo "xpath which does not match anything should output nothing, was '${not_found}'"
  exit 43
fi
unset not_found

found=$(../xml-select.sh ${xhtml} copy xhtml "//xhtml:p[@class='test-class']")
if test $? -ne 0; then
  echo "class='test-class' should be OK"
  exit 43
fi

if ! [[ "${found}" =~ test-class-1 ]]; then
  echo "Output should contain 'test-class-1', was '${found}'"
  exit 43
fi

if ! [[ "${found}" =~ test-class-2 ]]; then
  echo "Output should contain 'test-class-2', was '${found}'"
  exit 43
fi

if [[ "${found}" =~ test-div-1 ]]; then
  echo "Output should not contain 'test-div-1', was '${found}'"
  exit 43
fi

if [[ "${found}" =~ xmlns ]]; then
  echo "Output should not contain 'xmlns', was '${found}'"
  exit 43
fi
unset found

atom_found=$(../xml-select.sh ${atom} copy atom "/atom:feed/atom:id")
if test $? -ne 0; then
  echo "feed should be OK"
  exit 43
fi

if ! [[ "${atom_found}" =~ test-feed-id ]]; then
  echo "Output should contain 'test-feed-id', was '${atom_found}'"
  exit 43
fi
unset atom_found

# Copy data
xml_found=$(../xml-select.sh ${xml} copy "" "//*[@test-attribute-name='test-attribute-value']")
if test $? -ne 0; then
  echo "Copy data should be OK"
  exit 43
fi

if ! [[ "${xml_found}" =~ test-xml-value ]]; then
  echo "Copy data output should contain 'test-xml-value', was '${xml_found}'"
  exit 43
fi
unset xml_found

# Select attribute value
att_value_found=$(../xml-select.sh ${xml} value "" "//*/@test-attribute-name")
if test $? -ne 0; then
  echo "Select attribute value should be OK"
  exit 43
fi

if [[ "${att_value_found}" =~ test-xml-value ]]; then
  echo "Select attribute value output should not contain 'test-xml-value', was '${att_value_found}'"
  exit 43
fi

if ! [[ "${att_value_found}" =~ test-attribute-value ]]; then
  echo "Select attribute value output should contain 'test-attribute-value', was '${att_value_found}'"
  exit 43
fi
unset att_value_found

# Select element value
elm_value_found=$(../xml-select.sh ${xml} value "" "//*[@test-attribute-name='test-attribute-value']")
if test $? -ne 0; then
  echo "Select element value should be OK"
  exit 43
fi

if [[ "${elm_value_found}" =~ test-attribute-value ]]; then
  echo "Select element value output should not contain 'test-attribute-value', was '${elm_value_found}'"
  exit 43
fi

if ! [[ "${elm_value_found}" =~ test-xml-value ]]; then
  echo "Select element value output should contain 'test-xml-value', was '${elm_value_found}'"
  exit 43
fi
unset elm_xml_found

if ! ../xml-select.sh ${xhtml} copy xhtml "//xhtml:p[@class='entity-class']" | grep -q 'Before…After'; then
  echo 'Should replace entity &#x2026; by character …'
  exit 43
fi

molk_test_teardown
