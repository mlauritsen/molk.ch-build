#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt

source test-utils.sh
molk_check_script_syntax
molk_test_setup

# KO

if ../crumbs.sh &> /dev/null; then
  echo "should fail without args"
  exit 43
fi

# OK

if ! ../crumbs.sh "" &> /dev/null; then
  echo "should return OK with empty argument"
  exit 43
fi

if ! ../crumbs.sh "index.html" &> /dev/null; then
  echo "should return OK with index.html argument"
  exit 43
fi

if ! ../crumbs.sh "a" &> /dev/null; then
  echo "should return OK with non-null argument"
  exit 43
fi

# simple cases where there are no crumbs
molk_assert "./" "" "$(../crumbs.sh ./)"
molk_assert "index.html" "" "$(../crumbs.sh index.html)"
molk_assert "./index.html" "" "$(../crumbs.sh ./index.html)"

# variations on a/b/c/d.html
start="<ol id='crumbs'>"
link_a="<li><a href='/a/index.html'>a</a></li>"
link_b="<li><a href='/a/b/index.html'>b</a></li>"
link_c="<li><a href='/a/b/c/index.html'>c</a></li>"
link_d="<li><a href='/a/b/c/d.html'>d</a></li>"
end="</ol>"

expected_a="${start}${link_a}${end}"
molk_assert "a" "${expected_a}" "$(../crumbs.sh a)"
molk_assert "a/" "${expected_a}" "$(../crumbs.sh a)"
molk_assert "a.html" "${expected_a}" "$(../crumbs.sh a)"
molk_assert "a/index.html" "${expected_a}" "$(../crumbs.sh a/index.html)"

expected_ab="${start}${link_a}${link_b}${end}"
molk_assert "a/b" "${expected_ab}" "$(../crumbs.sh a/b)"
molk_assert "a/b/" "${expected_ab}" "$(../crumbs.sh a/b)"
molk_assert "a/b.html" "${expected_ab}" "$(../crumbs.sh a/b)"
molk_assert "a/b/index.html" "${expected_ab}" "$(../crumbs.sh a/b/index.html)"

expected_abc="${start}${link_a}${link_b}${link_c}${end}"
molk_assert "a/b/c" "${expected_abc}" "$(../crumbs.sh a/b/c)"
molk_assert "a/b/c/" "${expected_abc}" "$(../crumbs.sh a/b/c)"
molk_assert "a/b/c.html" "${expected_abc}" "$(../crumbs.sh a/b/c)"
molk_assert "a/b/c/index.html" "${expected_abc}" "$(../crumbs.sh a/b/c/index.html)"

molk_assert "a/b/c/d.html" "${start}${link_a}${link_b}${link_c}${link_d}${end}" "$(../crumbs.sh a/b/c/d.html)"

molk_test_teardown
