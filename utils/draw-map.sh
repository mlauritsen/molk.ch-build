#!/bin/bash
# Copyright 2017, 2018 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/molk-option-tools.sh"
source "${MOLK_UTILS}/html-tools.sh"

# generate site-map.html listing all pages under the provided directory

if test $# -ne 1; then
    echo -e "\nUsage: $0 path\nwas: $0 $@" >&2
    exit 42
fi

cd "${1}"

declare -r map_file=site-map.html
declare -r map_contents="${MOLK_TMP}/map.xml"

# check whether the map exists
if test -f "${map_file}"; then
    # if the file was not generated, fail
    if test "$(${MOLK_UTILS}/extract-molk-option.sh ${map_file} generated)" != "true"; then
	echo -e "\n\"${map_file}\": Could not generate - ungenerated map file exists" >&2
	exit 42
    fi
    # otherwise, back it up with a timestamp
    mv "${map_file}" "${map_file}-$(date +%Y.%m.%d-%H.%M.%S)"
fi

cp ${MOLK_SRC_FRAGMENTS}/html.tag "${map_file}"
chmod u+w "${map_file}"

if ! ${MOLK_UTILS}/insert-at-line.sh ${tmp_feed_entries} ${tmp_feed} ${next_to_last}; then
    echo -e "\n\"${src_file}\": Failed to insert feed entries into feed" >&2
    exit 42
fi

# TODO this should be a recursive function
current='./'


function molk_list_dir() {
    local root=$1

    # if this is not a directory, or there is no index.html, do nothing
    if ! test -d "${root}" -o ! -f "${root}/index.html"; then
	return 0;
    fi

    echo '<ul>'

    # first list the index
    # TODO $(mlk_create_html_item_link "${classes}" "${sibling}" "${long_title}" "${short_title}")
    echo $(mlk_create_html_item_link '' "${root}/index.html" '' $(molk_get_title "${root}/index.html") "${root}/index.html")

    # list other files
    for file in $(find "${root}" -type f -name '*.html' -maxdepth 1); do
	echo TODO file: $file
    done

    # iterate into directories
    for dir in $(find "${root}" -type d -maxdepth 1); do
	echo TODO dir: $dir
    done
	
	# iterate into directories
done
   
    echo '</ul>'
}
