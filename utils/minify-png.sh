#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of png files

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-pngs\nwas: $0 $*" >&2
  exit 42
fi

# setup
tmp=${MOLK_TMP}/to-be-minified.png
declare -r tmp

for file in "$@"; do
  if ! test -w ${file}; then
    echo -e "\n\"${file}\" must be writable" >&2
    exit 42
  fi

  if ! pngcrush -rem alla -brute -reduce "${file}" "${tmp}"; then
    echo -e "\n\"${file}\": Minification failed" >&2
    exit 42
  fi
  mv "${tmp}" "${file}" || exit 42
done
