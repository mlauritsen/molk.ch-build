#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# make a directory and all files in it readonly
if test $# -ne 1; then
  echo -e "\nUsage: $0 directory-to-make-readonly\nwas: $0 $*" >&2
  exit 42
fi

# setup
directory=$1
declare -r directory

if ! test -d ${directory}; then
  echo -e "\nArgument must be a directory: \"${directory}\"" >&2
  exit 42
fi

find ${directory} -exec chmod u-w {} \;
