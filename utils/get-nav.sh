#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# given the relative directory of a page, output the corresponding nav fragment
if test $# -ne 1; then
  echo -e "\nUsage: $0 relative-dir\nwas: $0 $*" >&2
  exit 42
fi

#setup
declare -r relative_dir=$1
# attention: src-fragments are .html, tmp-fragments are .xml!
declare -r inactive=${MOLK_TMP_FRAGMENTS}/nav-inactive.xml

# determine whether a specific nav fragment applies
if [[ "${relative_dir}" =~ ^([^/]+)/ ]]; then
  nav_fragment="${MOLK_TMP_FRAGMENTS}/nav-${BASH_REMATCH[1]}.xml"

  # fail if the subnav fragment does not exist
  if ! test -r "${nav_fragment}"; then
    echo -e "\n\"${relative_dir}\" implies a non-existing nav: \"${nav_fragment}\"" >&2
    exit 42
  fi

  # output the fragment
  cat "${nav_fragment}"
else
  # otherwise, use the inactive nav fragment
  if ! cat "${inactive}" 2> /dev/null; then
    echo -e "\n\"${inactive}\": inactive nav fragment not found" >&2
    exit 42
  fi
fi
