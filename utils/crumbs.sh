#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# output on stdout an HTML (li) list of directories/links leading to a path
if test $# -ne 1; then
  echo -e "\nUsage: $0 path\nwas: $0 $*" >&2
  exit 42
fi

# input
file=${1}

# if the filename starts with "./", remove it
file=${file#./}

# if the filename is "index.html", do nothing
if test "${file}" = "index.html"; then
  exit 0
fi

# if the filename ends with "/index.html", remove it
file=${file%/index.html}

# if the filename ends with "/", remove it
file=${file%/}
declare -r file

# if the filename is empty, do nothing
if test -z "${file}"; then
  exit 0
fi

# TODO integrate into recursion: simple return case:
# top-level files do not have crumbs
#if test -n "${file##*/*}"; then
#  exit 0
#fi

function mlk_link() {
  local path="$1"
  local text="$2"
  echo -n "<li><a href='${path}'>${text}</a></li>"
}

#TODO simple case: there are no /'s in the rest, so just link to it and finish
#TODO recursive call: there is a /: add a link and recurse
#TODO (no terminating / nor /index.html possible - shaved off above)

function mlk_crumbs() {
  local rest="${1}"
  local path=""
  echo -n "<ol id='crumbs'>"
  until test -z "${rest}"; do
    if test -n "${rest##*/*}"; then # there is no "/" left
      local name=${rest%.html}
      local link="${path}/${rest}"
      if ! [[ "${link}" =~ \.html$ ]]; then
        link+="/index.html"
      fi
      echo -n "$(mlk_link ${link} $name)</ol>"
      return 0
    else                            # there is a "/" remaining
      first="${rest%%/*}"           # everything before the first slash
      rest="${rest#*/}"             # everything after the first slash
      path+="/${first}"             # always relative to all the previous links
      echo -n $(mlk_link "${path}/index.html" "${first}")
    fi
  done
}

function mlk_crumbs_tbd() {
  local rest="${1}"
  local path=""
  echo -n "<ol id='crumbs'>"
  until test -z "${rest}"; do
    if test -n "${rest##*/*}"; then # there is no "/" left
      echo -n "</ol>"
      return 0
    else                            # there is a "/" remaining
      first="${rest%%/*}"           # everything before the first slash
      rest="${rest#*/}"             # everything after the first slash
      path+="/${first}"             # always relative to all the previous links
      echo -n $(mlk_link "$path" "$first")
    fi
  done
}

mlk_crumbs "${file}"
