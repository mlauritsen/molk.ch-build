#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# optimise the size of HTML files
# retain the first comment on the form <!--! ... --> -> <!-- ... -->

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

tmp=${MOLK_TMP}/minify-html.tmp
declare -r tmp

for file in "$@"; do
  # file must be readable
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": Must exist and be writable" >&2
    exit 42
  fi

  # extract the licence comment
  if ! ${MOLK_UTILS}/extract-html-style-comment.sh "${file}" > "${tmp}"; then
    echo -e "\n\"${file}\": Failed to extract licence comment" >&2
    exit 42
  fi

  # minify the file
  if ! tidy --hide-comments yes -quiet -xml -utf8 --preserve-entities 1 -modify ${file}; then
    echo -e "\n\"${file}\": HTML minification failed" >&2
    exit 42
  fi

  # reinsert spaces after end-tags followed by alphabetic characters
  # see also format-html.sh
  if ! sed --in-place --regexp-extended --expression='s/(<\/[[:alpha:]]+>)([[:alpha:]]|\/|\()/\1 \2/g' "${file}"; then
    echo -e "\n\"${file}\": Format-fixing failed" >&2
    exit 42
  fi    

  # reinject the licence comment
  if ! ${MOLK_UTILS}/insert-at-line.sh "${tmp}" "${file}" 1; then
    echo -e "\n\"${file}\": Failed to reinsert licence comment" >&2
    exit 42
  fi
done
