#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be the name of the script to run
if test "${#}" -eq 0; then
  echo -e "\nUsage: $0 script-to-run arguments\nwas: $0 $*" >&2
  exit 42
fi

script="${1}"

if ! test -x "${script}"; then
  echo -e "\n\"${script}\": should exist and be executable" >&2
  exit 42
fi

# setup
mkdir -p ${MOLK_TMP}/targets

# check if target marker exists
marker=${script##*/}
marker=${MOLK_TMP}/targets/${marker%.sh}

if test -e "${marker}"; then
  exit 0
fi

# run the script
shift
START_TIME="${SECONDS}"
if \time --output="${marker}" --format '%e' ${script} ${@}; then
    duration="$((${SECONDS} - ${START_TIME}))"
    if test "${duration}" -gt 60; then
	echo " OK (duration: $((${duration} / 60)) minutes)"
    elif test "${duration}" -gt 1; then
	echo " OK (duration: ${duration} seconds)"
    else
	echo " OK"
    fi
else
    echo -e "\nScript failed: \"${script}\"" >&2
    exit 42
fi
