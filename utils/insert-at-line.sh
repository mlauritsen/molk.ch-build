#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# insert $1 into $2 at line $3

if test $# -ne 3; then
  echo -e "\nUsage: $0 file-to-insert file line-number\nwas: $0 $*" >&2
  exit 42
fi

to_insert=$1
file=$2
line=$3
tmp=${MOLK_TMP}/insert-at-line.tmp
declare -r to_insert file line tmp

if ! test -w $to_insert; then
  echo -e "\n${to_insert}: Must exist and be readable" >&2
  exit 42
fi

if ! test -w $file; then
  echo -e "\n\"${file}\": Must exist and be writable" >&2
  exit 42
fi

if ! test "$line" -ge 0; then
  echo -e "\n${line}: Must be zero or a positive number" >&2
  exit 42
fi

head -${line} ${file} > ${tmp} || exit 42
cat "${to_insert}" >> ${tmp} || exit 42
tail --lines=+$((${line}+1)) ${file} >> ${tmp} || exit 42
mv "${tmp}" "${file}" || exit 42
