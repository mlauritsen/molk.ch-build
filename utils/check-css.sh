#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of css files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# setup
result=${MOLK_TMP}/check-css-result.txt
cd ${MOLK_LIBRARIES}/css-validator/2002/css-validator

for file in "$@"; do
  if ! test -r ${file}; then
   echo -e "\n\"${file}\": not a readable file" >&2
   exit 42
  fi

  # Do not check library styles
  if echo "${file}" | grep -q '/lib/'; then
      continue;
  fi
  
  java -jar css-validator.jar file://${file} > ${result}

  # "Parse Error" means the file was malformed
  if grep -q "Parse Error" ${result}; then
    echo -e "\n\"${file}\": malformed - see ${result}" >&2
    exit 42
  fi

  # "We found the following errors" means there were errors  
  if grep -q "We found the following errors" ${result}; then
    echo -e "\n\"${file}\": errors - see ${result}" >&2
    exit 42
  fi
done
