#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/file-tools.sh"

# Is this a molk page?
function molk_is_molk() {
    if test $# -ne 1; then
	echo -e "\nUsage: molk_is_molk myfile" >&2
	return 42
    fi

    grep -qe "<?molk .*?>" "${1}"
    return
}

# get the value of a molk option (provide defaults when sensible)

# Migration: $(${MOLK_UTILS}/extract-molk-option.sh ${src_file} html-name) -> $(molk_get_option ${src_file} html-name)
function molk_get_option() {
    if test $# -ne 2; then
	echo -e "\nUsage: molk_get_option file option-name" >&2
	return 42
    fi

    local file="${1}"
    molk_file_exists "${file}"

    local name="${2}"
    # absent and present-but-empty options are assigned the value "false"
    local absent=false

    # max one processing instruction line
    if test $(grep -e "<?molk .*?>" "${file}" | wc -l) -gt 1; then
	echo -e "\n'${file}' should contain exactly one <?molk... ?> processing instruction" >&2
	return 42
    fi
    
    local options=$(grep -e "^<?molk .*?>$" "${file}")
    if test -z "${options}"; then
	echo -e "\n'${file}' does not contain <?molk... ?> options (no surrounding text or whitespace allowed)" >&2
	return 42
    fi

    # do not accept multiple occurrences of the same option
    if [[ "${options}" =~ .*${name}=\".*\".*${name}=\".*\".* ]]; then
	echo -e "\n\"${file}\" should contain exactly one occurrence of \"${name}\"" >&2
	return 42
    fi

    # empty options default to $absent
    if [[ "${options}" =~ ${name}=\"\" ]]; then
	echo -n "${absent}"
	return 0
    fi

    if [[ "${options}" =~ .*${name}=\"([^\"]*)\".* ]]; then
	# extract the value
	result=${BASH_REMATCH[1]}

	# ensure it is valid (does not contain ")
	if [[ "${result}" =~ .*${name}=.*\".* ]]; then
	    echo -e "\nInvalid value found: \"${result}\"" >&2
	    return 42
	else
	    echo -n "${result}"
	    return 0
	fi
    fi

    # absent options default to $absent
    echo -n "${absent}"
#    return 0
}

# provide functions to get and process the title of a page (molk or not)

function molk_get_title() {
    if test $# -ne 1; then
	echo -e "\nUsage: molk_get_title myfile" >&2
	return 42
    fi

    if ! test -f "${1}"; then
	echo -e "\n\"${1}\": should be an existing file" >&2
	return 42
    fi

    local name="$(basename ${1%%.html})"
    if ! molk_is_molk "${1}"; then
	echo "${name}"
	return
    fi
    
    local title=$(${MOLK_UTILS}/extract-molk-option.sh "${1}" title)
    if [[ "${title}" == "false" ]]; then
	echo "${name}"
	return
    fi
    echo "${title}"
}

function molk_shorten_title() {
    if test $# -ne 1; then
	echo -e "\nUsage: molk_shorten_title mytitle\nWas: $0 $@" >&2
	return 42
    fi
    local short_name="${1}"

    # trim leading and trailing spaces
    short_name=$(echo "${short_name}" | sed 's/^[[:space:]]*//' | sed 's/[[:space:]]*$//')

    # expand escaped chars (e.g. if the name contains &#x2026;, expand to "…")
    # if escaped chars get chopped, they make no sense
    short_name=$(printf %b "$(echo "${short_name}" | sed 's/\(\&\#x\)\([0-9][0-9][0-9][0-9]\)\(\;\)/\\u\2/g')")

    if test "${#short_name}" -gt 20; then
	short_name=$(echo "${short_name}" | sed  --expression='s/[AaEeIiOoUuYy]//g')
    fi
    
    if test "${#short_name}" -gt 20; then
	echo "$(echo -e ${short_name:0:19})…"
    else
	echo "${short_name}"
    fi
}

# Extract the molk-page feed type (default is 'update')
molk_get_feed_type() {
    if test $# -ne 1; then
	echo -e "\nUsage: molk_get_feed_type myfile" >&2
	return 42
    fi

    if ! molk_is_molk "${1}"; then
	echo "'molk_get_feed_type' only makes sense for molk pages, was ${1}" >&2
	return 42
    fi

    local feed_value=$(${MOLK_UTILS}/extract-molk-option.sh "${1}" feed)
    if test "${feed_value}" = "" || test "${feed_value}" = "false"; then
	feed_value="update"
    fi
    
    echo "${feed_value}"
    return
}
