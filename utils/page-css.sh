#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# include the by-page css resource if it exists
if test $# -ne 1; then
  echo -e "\nUsage: $0 absolute-filename\nwas: $0 $*" >&2
  exit 42
fi

# input
file=$1
declare -r file

if ! test -f ${file}; then
  echo -e "\n\"${file}\" does not exist" >&2
  exit 42
fi

base=$(basename ${file})
no_extension=${base%%.html}
dir=${file%${base}}

if test -f ${dir}/${no_extension}.css; then
  echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"${no_extension}.css\" />"
fi
