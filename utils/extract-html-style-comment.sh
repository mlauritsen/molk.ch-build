#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# Extract the first HTML-style special comment <!--! ... --> to stdout

if test $# -ne 1; then
  echo -e "\nUsage: $0 file\nwas: $0 $*" >&2
  exit 42
fi

file=$1
declare -r file

if ! test -f $file; then
  echo -e "\n\"${file}\": Does not exist" >&2
  exit 42
fi

mawk -f ${MOLK_UTILS}/resources/extract-html-style-comment.awk "${file}" \
  | sed --expression='1s/<\!--\!/<\!--/' \
  | sed --expression='1 { /^$/d }'
