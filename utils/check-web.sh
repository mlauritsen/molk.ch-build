#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# Check that a directory is the root of a web page where no internal problems exist
# (missing images/links/css files etc.)

# input should be a directory
if test $# = 0 || test $# -gt 2; then
  echo -e "\nUsage: $0 home-directory [port]\nwas: $0 $*" >&2
  exit 42
fi

if test $# = 2; then
    port=$2
else
    port='8024'
fi

# setup
declare -r home=$1
declare -ri port
declare -r result=${MOLK_TMP}/webcheck
declare -r url=http://localhost:$port

rm -rf "${result}"
mkdir "${result}"

if ! test -d "${home}"; then
  echo -e "\n\"${home}\": should be an existing directory" >&2
  exit 42
fi

# make $home available on $url
http-server "${home}" -p "${port}" --silent & declare -ri server_id=$! # > /dev/null 2>&1
sleep 1s

# generate webcheck report in $result
if ! webcheck --quiet --base-only --wait 0 --output=$result $url &> /dev/null; then
  echo -e "\n\"${home}\": error while running webcheck" >&2
  exit 42
fi

# kill the web server
if ! kill $server_id; then
  echo -e "\nFailed to shutdown HTTP server (port $port, pid $server_id)" >&2
  exit 42
fi

# check if there were 404s in the result
declare -r badlinks=${result}/badlinks.html

# check that the root could be retrieved
if grep -E "class=\"internal\".*error reading HTTP response: ''" ${badlinks}; then
  echo -e "\n\"${home}\": could not retrieve root. See: ${badlinks}" >&2
  exit 42
fi

# If there are bad script links, fail
if grep -E "class=\"internal\".*title: Invalid Script Source" ${badlinks}; then
  echo -e "\n\"${home}\": contains bad script sources. See: ${badlinks}" >&2
  exit 42
fi

# If there are bad image links, fail
if grep -E "class=\"internal\".*title: Invalid Image Linker" ${badlinks}; then
  echo -e "\n\"${home}\": contains bad image links. See: ${badlinks}" >&2
  exit 42
fi

# If there are bad CSS links, fail
if grep -E "[^0-9]404 .*\.css</a>$" ${badlinks}; then
  echo -e "\n\"${home}\": contains bad CSS links. See: ${badlinks}" >&2
  exit 42
fi

# If there are bad links, log a warning
if grep -E "[^0-9]404 " ${badlinks}; then
  echo "Warning: \"${home}\" contains bad links. See: ${badlinks}" > "${MOLK_LOG}"
#  echo -e "\n\"${home}\": contains bad links. See: ${badlinks}" >&2
#  exit 42
fi
