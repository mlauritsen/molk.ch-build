#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# in the $1 directory tree, ensure that all directories containing .html files also contain an index.html
if test $# -ne 1; then
  echo -e "\nUsage: $0 directory\nwas: $0 $*" >&2
  exit 42
fi

directory="$1"
declare -r directory

if ! test -d ${directory}; then
  echo -e "\nArgument must be a directory - was: \"${directory}\"" >&2
  exit 42
fi

directories=($(find ${directory} -name "*.html" -printf "%h\n" | sort -u))
if test ${#directories[@]} -eq 0; then
  exit 0
fi

echo -n "Checking for index files in ${#directories[@]} directories: "
for html_dir in "${directories[@]}"; do
  if ! test -f ${html_dir}/index.html; then
    echo -e "\n\"${html_dir}\": contains .html files but no index.html" >&2
    exit 42
  fi
done
echo "OK"
