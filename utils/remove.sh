#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# delete the argument directory
# echo the number of files deleted (if any)

# check
if test $# -ne 1; then
  echo -e "\nUsage: $0 directory-to-remove\nwas: $0 $*" >&2
  exit 42
fi

# setup
dir="${1}"
declare -r dir

# if the directory does not exist, do nothing
if ! test -d "${dir}"; then
  exit 0
fi

echo -n "Removing $(find ${dir} -print | wc -l) files from ${dir}: "

${MOLK_UTILS}/writable.sh "${dir}" && \
rm -rf "${dir}" && \
echo "OK"
