#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/molk-option-tools.sh"

# set back_href and back_name so a link to the "back" page can be constructed
function molk_back_link() {
    if test $# -ne 1; then
	echo -e "\nUsage: $0 path\nwas: $0 $*" >&2
	return 42
    fi

    # input
    local path=$1

    # output
    back_href=''
    back_name=''

    # if the path starts with "./", remove it
    path=${path#./}

    # if the path ends with "/index.html", remove it
    path=${path%index.html}

    # if the path ends with "/", remove it
    path=${path%/}
    declare -r path

    local parent
    # if the path is empty, this is the root, which has no back link
    if test -z "${path}"; then
	return 0
	# top-level non-index files link to index.html
    elif [[ ${path} =~ ^[^/]+\.html$ ]]; then
	back_href='index.html'
	parent='index.html'
	# non-top-level non-index files link to their local index file
    elif [[ ${path} =~ (.*)/.*\.html$ ]]; then
	back_href='index.html'
	parent="${BASH_REMATCH[1]}/index.html"
	# non-top-level index files link to the parent index file
    else
	back_href='../index.html'
	parent="${path}/../index.html"
    fi

    # parent is relative to MOLK_SRC
    parent="${MOLK_CONTENT}/${parent}"

    # get the title of the parent
    back_name=$(molk_get_title "${parent}")

    if test $? -ne 0; then
	echo -e "\n\"${path}\": Error while retrieving title from parent: '${parent}'" >&2
	return 42
    fi
}
