#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/molk-option-tools.sh"
source "${MOLK_UTILS}/html-tools.sh"

# Sibling Navigation (sibnav)

# output on stdout a prettified HTML (li) list of sibling pages in the
# directory of the arg file.

# these pages will represent other topics closely related to the current page
# see also "subnav" (which lists main topics under the current tab)

if test $# -ne 1; then
  echo -e "\nUsage: $0 <<directory>>/<<file>>.html\nwas: $0 $*" >&2
  exit 42
fi

# arg must be an existing file
if ! test -f "${1}"; then
  echo -e "\nInput should be a file: \"${1}\"" >&2
  exit 42
fi

# non-empty, include "/" and have a .html extension
if ! [[ "${1}" =~ (.+)/([^/]+\.html)$ ]]; then
  echo -e "\nInput should be a file in a directory, with an .html extension: \"${1}\"" >&2
  exit 42
fi
declare -r dir="${BASH_REMATCH[1]}"
declare -r file="${BASH_REMATCH[2]}"
#declare -r name="${file%%.html}"

# ls will sort the siblings alphabetically by filename
# by title would be better, but at least this will be consistent
siblings=($(ls ${dir}))
sibnav=""
sibnav_exists=false

if test ${#siblings[@]} -gt 0; then
    for sibling in "${siblings[@]}"; do
	ref="${dir}/${sibling}"
	# if it is:
	# 1. a file (directories and their contents are children, not siblings)
	# 2. an html file
	# 3. not index.html (which is a parent, not a sibling)
	if [[ -f "${ref}" && "${sibling}" != "index.html" && "${ref}" =~ \.html$ ]]; then
	    # do not include pages with feed type "meta" or "delta"
	    if page_feed_type=$(molk_get_feed_type "${ref}" 2> /dev/null); then
               if test "${page_feed_type}" = "meta" || test "${page_feed_type}" = "delta" ; then
		   continue;
               fi
	    fi
            unset page_feed_type

	    long_title=$(molk_get_title "${ref}")
	    short_title=$(molk_shorten_title "${long_title}")
	    classes=''
	    if test "${file}" = "${sibling}"; then
		classes='active'
	    else
		sibnav_exists=true
	    fi
	    sibnav="${sibnav}"$(molk_create_html_item_link "${classes}" "${sibling}" "${long_title}" "${short_title}")
	    # if it is a directory and there is an html index file
#	elif [[ -d "${ref}" && -f "${ref}/index.html" ]]; then
#	    long_title=$(molk_get_title "${ref}/index.html")
#	    if test "${long_title}" == "index"; then
#		long_title="${sibling}"
#	    fi
#	    short_title=$(molk_shorten_title "${long_title}")
#
#	    sibnav="${sibnav}"$(molk_create_html_item_link '' "${sibling}/index.html" "${long_title}" "${short_title}")
#	    sibnav_exists=true
	fi
    done
fi

# if any sibnav entries were found, write them to stdout, otherwise do nothing
if test "${sibnav_exists}" == "true"; then
  echo -e '<ul id="sibnav">' "${sibnav}" '\n</ul>'
fi
