#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

if test $# -ne 1; then
  echo -e "\nUsage: $0 html-file\nwas: $0 $*" >&2
  exit 42
fi

declare -r file=$1

if ! test -w $file; then
  echo -e "\n\"$file\": Should be a writable file" >&2
  exit 42
fi

# href="http://molk.ch/..." -> href="/..."
sed --in-place 's/\([^ ]\)\/>/\1 \/>/g' $file > ${tmp} && \
mv ${tmp} ${file}
