#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# optimise the size of CSS files
# retain the first comment on the form /*! ... */ -> /* ... */

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

tmp=${MOLK_TMP}/minify-css.tmp
comment=${MOLK_TMP}/comment.txt
declare -r tmp comment

for file in "$@"; do
  # file must be readable
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": Does not exist" >&2
    exit 42
  fi

  # extract the licence comment
  if ! ${MOLK_UTILS}/extract-c-style-comment.sh "${file}" > "${comment}"; then
    echo -e "\n\"${file}\": Failed to extract licence comment" >&2
    exit 42
  fi

  # minify the file
  if ! csstidy ${file} --silent=true --discard_invalid_properties=true --template=highest ${tmp}; then
    echo -e "\n\"${file}\": Formatting failed" >&2
    exit 42
  fi
  mv ${tmp} ${file}


  # reinject the licence comment
  if ! ${MOLK_UTILS}/insert-at-line.sh "${comment}" "${file}" 0; then
    echo -e "\n\"${file}\": Failed to reinsert licence comment" >&2
    exit 42
  fi
done
