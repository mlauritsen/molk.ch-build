#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of atom feed files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-feed-files\nwas: $0 $*" >&2
  exit 42
fi

if test "$(${MOLK_UTILS}/is-online.sh)" != "true"; then
  echo -e "\nOffline mode: Skipping online feed checks" >&2
  exit 0
fi

# ignore error code when grep doesn't find any
without_ignored=($(grep --files-without-match MOLK-TODO "${@}" || true))

# if there are no files left, do nothing
if test ${#without_ignored[@]} -eq 0; then
  exit 0
fi

# POST to https://validator.w3.org/feed/check.cgi

declare -r CHECK_RESULT="${MOLK_TMP}/check-feed-online-result.html"
declare -r CHECK_ERROR="${MOLK_TMP}/check-feed-online-error.txt"

# validate against the specification relaxng schema
# http://www.asahi-net.or.jp/~eb2m-mrt/atomextensions/atom.rng
if (xmlstarlet val --err --net --list-bad --relaxng ${MOLK_UTILS}/resources/atom.rng ${without_ignored[@]} |& grep -v "Unimplemented block at " ); then
  echo -e "\nInvalid atom feed" >&2
  exit 42
fi

# run feedvalidator (http://feedvalidator.org)
for file in "${without_ignored[@]}"; do
  if ! curl 'https://validator.w3.org/feed/check.cgi' --data-urlencode "rawdata=$(cat ${file})" > "${CHECK_RESULT}" 2> "${CHECK_ERROR}"; then
    echo -e "\nCould not execute online feed check - see ${CHECK_ERROR}" >&2
    exit 42
  fi

  if ! grep -q 'Congratulations!' "${CHECK_RESULT}"; then
    echo -e "\nOnline feed check failed with errors - see ${CHECK_RESULT}" >&2
    exit 42
  fi

  if grep -q 'Recommendations' "${CHECK_RESULT}"; then
    if  grep -v 'SelfDoesntMatchLocation' "${CHECK_RESULT}" | grep -q '<span class="message">'; then
      echo -e "\nOnline feed check failed with warnings (ignoring SelfDoesntMatchLocation) - see ${CHECK_RESULT}" >&2
      exit 42
    fi
  fi
done
