#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/molk-option-tools.sh"
source "${MOLK_UTILS}/html-tools.sh"

# output on stdout a prettified HTML (li) list of directories in the arg directory
# these directories will represent other second-level topics under the same main topic (tab)
# see also "sibnav" (which, at any level, reflects other pages at the same level)

if test $# -ne 1; then
  echo -e "\nUsage: $0 directory\nwas: $0 $*" >&2
  exit 42
fi

# input: the name of a directory in ${MOLK_CONTENT}
declare -r nav_dir_name=$1

# $nav_dir_name must be non-empty and not contain "/"
if ! [[ "${nav_dir_name}" =~ ^[^/]+$ ]]; then
  echo -e "\nInvalid nav directory: \"${nav_dir_name}\"" >&2
  exit 42
fi

directory=${MOLK_CONTENT}/$nav_dir_name
# fail if no directory was passed
if ! test -d "${directory}"; then
  echo -e "\n\"${nav_dir_name}\" must be an existing directory - was \"${directory}\"" >&2
  exit 42
fi
declare -r directory

files=($(ls ${directory}))
subnav=""
subnav_exists=false

if test ${#files[@]} -gt 0; then
  for file in "${files[@]}"; do
      if test -d "${directory}/${file}"; then
	  long_title='index'
	  if test -e "${directory}/${file}/index.html"; then
	      long_title=$(molk_get_title "${directory}/${file}/index.html")
	  else
	      continue;
	  fi
	  if test "${long_title}" == "index"; then
	      long_title="${file}"
	  fi
	  short_title=$(molk_shorten_title "${long_title}")
	  subnav="${subnav}"$(molk_create_html_item_link 'activatable' "/${nav_dir_name}/${file}/index.html" "${long_title}" "${short_title}")
	  subnav_exists=true
    fi
  done
fi

# if any subnav entries were found, write them to stdout, otherwise do nothing
if test "${subnav_exists}" = "true"; then
  echo -e '<ul id="subnav">' "${subnav}" '\n</ul>'
fi
