#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# DEPRECATED - use molk-option-tools.sh!

if test $# -ne 2; then
  echo -e "\nUsage: $0 file option\nwas: $0 $*" >&2
  exit 42
fi

input=$1
option=$2
# absent and present-but-empty options are assigned the value "false"
absent=false
declare -r input option absent

if ! test -f $input; then
  echo -e "\n\"$input\": should be an existing file" >&2
  exit 42
fi

# the file should have a molk processing instruction
# which should be on one line with nothing else on it
instructions=$(grep -e "<?molk .*?>" ${input})
if test -z "${instructions}"; then
  echo -e "\n${input} does not contain <?molk... ?> instructions" >&2
  exit 42
fi

# do not accept multiple occurrences of the same option
if [[ "${instructions}" =~ .*${option}=\".*\".*${option}=\".*\".* ]]; then
  echo -e "\n\"${input}\" should contain exactly one occurrence of \"${option}\"" >&2
  exit 42
fi

# empty options default to $absent
if [[ "${instructions}" =~ ${option}=\"\" ]]; then
  echo -n "${absent}"
  exit 0
fi

if [[ "${instructions}" =~ .*${option}=\"([^\"]*)\".* ]]; then
  # extract the value
  result=${BASH_REMATCH[1]}

  # ensure it is valid (does not contain ")
  if [[ "${result}" =~ .*${option}=.*\".* ]]; then
    echo -e "\nInvalid value found: \"${result}\"" >&2
    exit 42
  else
    echo -n "${result}"
    exit 0
  fi
fi

# absent options default to $absent
echo -n "${absent}"
exit 0
