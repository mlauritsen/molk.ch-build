#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be the name of a script (with .sh extension, without any path info) to run
if test $# -ne 1; then
  echo -e "\nUsage: $0 <<script.sh>>\nwas: $0 $*" >&2
  exit 42
fi

declare -r SCRIPT="${MOLK_SRC}/hooks/${1}"

# If the script does not exist, do nothing
if ! test -f "${SCRIPT}"; then
    exit 0
fi

# Script must be readable and executable
if ! test -r "${SCRIPT}"; then
  echo -e "${SCRIPT} is not readable" >&2
  exit 42
fi

if ! test -x "${SCRIPT}"; then
  echo -e "${SCRIPT} is not executable" >&2
  exit 42
fi

# Run the script (propagating the return code)
"${SCRIPT}"
