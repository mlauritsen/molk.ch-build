#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# brute force image metadata cleaning: exiftool -all= file.jpg

# input should be an image file
if test $# -ne 1; then
  echo -e "\nUsage: $0 image-filename\nwas: $0 $*" >&2
  exit 42
fi

declare -r file="${1}"

# extension check
if [[ ! "${file}" =~ \.(jpg|png)$ ]]; then
    echo -e "\n\"${file}\": should have .png or .jpg extension" >&2
    exit 42
fi

# content check
type="$(file --mime --brief "${file}" | cut -d';' -f1)"
if test "${type}" != "image/png" && test "${type}" != "image/jpg" && test "${type}" != "image/jpeg"; then
    echo -e "\n\"${file}\": Contents should be MIME image/png or image/jpg, was ${type}" >&2
    exit 42
fi
unset type

# permission check
if ! test -r ${file}; then
    echo -e "\n\"${file}\": not a readable file" >&2
    exit 42
fi

# Extract metadata
declare -r metadata="${MOLK_TMP}/metadata.txt"
exiftool -a -u -G:1:2:3:4 "${file}" | sort > "${metadata}"

# Avoid "Orientation" Metadata, it makes image rendering unpredictable
if grep -iq " orientation" "${metadata}"; then
    echo -e "\n\"${file}\": Should not contain ' Orientation' metadata - see ${metadata}" >&2
    exit 42
fi

# Avoid nonstandard Metadata groups
declare -r unexpected="${MOLK_TMP}/unexpected.txt"
egrep -v '^\[(Composite:Image|ExifTool:|File:Image|System:Image|System:Time|PNG:Image:Main)' "${metadata}" > "${unexpected}" || true
if test -s "${unexpected}"; then
    echo -e "\n\"${file}\": Contains unexpected metadata - see ${unexpected}" >&2
    exit 42
fi
