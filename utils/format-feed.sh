#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of atom feed files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-feed-files\nwas: $0 $*" >&2
  exit 42
fi

# it's OK not to find any
without_ignored=($(grep --files-without-match MOLK-TODO $@ || true))

# if there are no files left, do nothing
if test ${#without_ignored[@]} -eq 0; then
  exit 0
fi

for file in "$@"; do
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": does not exist" >&2
    exit 42
  fi

  if ! tidy -quiet -indent -xml -utf8 -modify ${file}; then
    echo -e "\n\"${file}\": Formatting failed" >&2
    exit 42
  fi
done
