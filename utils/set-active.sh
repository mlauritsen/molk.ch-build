#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# highlight the active page elements (utils, nav, subnav)
# the reference dir for util / nav / sub-nav is $MOLK_CONTENT

if test $# -ne 1; then
  echo -e "\nUsage: $0 relative-url\nwas: $0 $*" >&2
  exit 42
fi

# the page for which the active elements should be set
page=$1
# page should not begin with "/"
# (for convenience - $page is relative to the site base, so the "/" is redundant)
page=${page#/}
# page should not end with "/"
page=${page%/}
declare -r page

# file is the path to $page in the file system
filename=$(basename "${page}")
file="${MOLK_TRANSFORM_PAGE}/${filename}"
unset filename
declare -r page file

# constants
utils_id=utils
nav_id=nav
subnav_id=subnav
activatable_class=activatable
active_class=active
xml_options=--pf
declare -r utils_id nav_id subnav_id activatable_class active

# the HTML file to be modified must be in ${file}
if ! test -f "${file}"; then
  echo "\"${file}\" not found"
  exit 42
fi

# find active nav (directory at the root level)
if [[ "${page}" =~ ^([^/]+).* ]]; then
  active_nav=${BASH_REMATCH[1]}

  # if $active_nav is not a directory, do nothing (neither nav nor subnav)
  if test -n "${active_nav}" && ! test -d "${MOLK_CONTENT}/${active_nav}"; then
    active_nav=""
  fi
fi

# if a nav is active, look for an active subnav
if test -n "${active_nav}"; then
  if [[ "${page}" =~ ^[^/]+/([^/]+).* ]]; then
    active_subnav=${BASH_REMATCH[1]}

    # if $active_subnav was not set, or is not a directory, do nothing
    if test -n "${active_subnav}" && test -d "${MOLK_CONTENT}/${active_nav}/${active_subnav}"; then
      active_subnav_xpath="//*[@id='${subnav_id}']//*[@href='/${active_nav}/${active_subnav}/index.html']/ancestor::*[@class='${activatable_class}']/@class"
      ${MOLK_UTILS}/update-xml.sh "${file}" "${active_subnav_xpath}" "${active_class}"

      # if no active subnav was set, fail
      if ! grep -q "class=\"${active_class}\"" "${file}"; then
        echo -e "\n\"${page}\": Subnav \"${MOLK_CONTENT}/${active_nav}/${active_subnav}\" is a directory but was not set as active" >&2
        exit 42
      fi
    fi
  fi
fi

# if no nav was active, look for an active util
if test -z "${active_nav}"; then
  # set active utils (.html files at the root level)
  if [[ "${page}" =~ ^([^/]+\.html) ]]; then
    active_util=${BASH_REMATCH[1]}
    active_util_xpath="//*[@id=\"${utils_id}\"]//*[@href='/${active_util}']/ancestor::*[@class=\"${activatable_class}\"]/@class"
    ${MOLK_UTILS}/update-xml.sh "${file}" "${active_util_xpath}" "${active_class}"
  fi
fi

# remove remaining utils $activatable_class'es
inactive_util_xpath="//*[@id=\"${utils_id}\"]//*[@class=\"${activatable_class}\"]/@class"
${MOLK_UTILS}/delete-xml.sh "${file}" "${inactive_util_xpath}"

# remove remaining subnavs $activatable_class'es
inactive_subnav_xpath="//*[@id=\"${subnav_id}\"]//*[@class=\"${activatable_class}\"]/@class"
${MOLK_UTILS}/delete-xml.sh "${file}" "${inactive_subnav_xpath}"
