# Extract the first C-style special comment: /*! ... */
/^ *\/\*\!.*\*\/ *$/ { print $0; exit 0 }
/^ *\/\*\!/ { echo = "true" }
echo { result = result $0"\n" }
/^ *\*\/ *$/ {
    if (echo) {
	print result;
	exit 0
    }
}
