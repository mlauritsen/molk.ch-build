# add missing newline at the EOF
$ {
  /^$/! {
    h
    s/.*//g
    H
    g
  }
}
