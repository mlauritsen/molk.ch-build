#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# optimise the size of feed files

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

tmp=${MOLK_TMP}/minify-feed.tmp
declare -r tmp

for file in "$@"; do
  # file must be readable
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": Must exist and be writable" >&2
    exit 42
  fi

  # minify the file
  if ! tidy --hide-comments yes -quiet -xml -utf8 -modify ${file}; then
    echo -e "\n\"${file}\": Feed minification failed" >&2
    exit 42
  fi
done
