#!/bin/bash
# Copyright 2017 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of absolute filenames of pages containing JS tests to run
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-absolute-files\nwas: $0 $*" >&2
  exit 42
fi

declare -r TEST_JS_LOG="${MOLK_TMP}/test-js.log"
declare -r FILE="${MOLK_TMP}/test-file.html"
for file in "$@"; do
  if ! test -f "${file}"; then
    echo -e "\nFile \"${file}\" does not exist" >&2
    exit 42
  fi

  if ! [[ "${file}" = /* ]]; then
      echo -e "\nFile \"${file}\" is not absolute" >&2
      exit 42
  fi
  url="file://${file}"

  if ! curl --silent "${url}" --output "${FILE}"; then
    echo -e "\nURL \"${url}\" does not exist" >&2
    exit 42
  fi

  # Only run tests if the page includes qunit.js as a script
  if grep -qE '<script .*src=".*qunit.js"></script>' "${FILE}"; then
      #file:///home/mlauritsen/projects/current/molk.ch/src/content/projects/molk.ch-js/tag-filter/unit-tests.html
      if ! phantomjs "${MOLK_LIBRARIES}/qunit-phantomjs-runner/runner.js" "${url}" > "${TEST_JS_LOG}"; then
	  echo -e "\nJavascript tests in \"${url}\" failed - see ${TEST_JS_LOG}" >&2
	  exit 42
      else
	  rm "${TEST_JS_LOG}"
      fi
  else
      echo "qunit.js not included - ignoring ${url}" > "${MOLK_LOG}"
  fi
  rm "${FILE}"
done
