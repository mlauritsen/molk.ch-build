#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of XHTML files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

# it's OK not to find any
without_ignored=($(grep --files-without-match MOLK-TODO $@ || true))

# check that none of the files include closing start tags - it's OK not to find any
with_closing_start_tag=$(grep --files-with-match "[^ ]/>" $@ || true)
if test "${with_closing_start_tag}" != ""; then
  echo -e "\nThe following files contained closing start tags: \"${with_closing_start_tag}\"" >&2
  exit 42
fi

# if there are no files left, do nothing
if test ${#without_ignored[@]} -eq 0; then
  exit 0
fi

# ensure that there are no style attributes - it's OK not to find any
with_style_attributes=$(grep --files-with-match ' style="' ${without_ignored[@]} || true)
if test "${with_style_attributes}" != ""; then
  echo -e "\nThe following files contained a style attribute: \"${with_style_attributes}\"" >&2
  exit 42
fi

# ensure that there are no style elements - it's OK not to find any
with_style_element=$(grep --files-with-match '<style.*>' ${without_ignored[@]} || true)
if test "${with_style_element}" != ""; then
  echo -e "\nThe following files contained a non-empty style element: \"${with_style_element}\"" >&2
  exit 42
fi

# ensure that there are no non-empty script elements - it's OK not to find any
with_non_empty_script_element=$(grep --files-with-match '<script.*>.\+</script>' ${without_ignored[@]} || true)
if test "${with_non_empty_script_element}" != ""; then
  echo -e "\nThe following files contained a non-empty script element: \"${with_non_empty_script_element}\"" >&2
  exit 42
fi

for file in "${without_ignored[@]}"; do
    
    # do not declare DTD / DOCTYPE
    if grep -q DOCTYPE "${file}"; then
	echo -e "\n\"${file}\": should not have DOCTYPE declaration" >&2
	exit 42
    fi

    # documents should contain xmlns="http://www.w3.org/1999/xhtml"
    if ! grep -q 'xmlns="http://www.w3.org/1999/xhtml"' "${file}"; then
	echo -e "\n\"${file}\": should contain 'xmlns=' default XHTML namespace declaration" >&2
	exit 42
    fi

    # documents should contain xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    if ! grep -q 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' "${file}"; then
	echo -e "\n\"${file}\": should contain 'xsi' XHTML namespace declaration" >&2
	exit 42
    fi

    # documents should contain xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"
    if ! grep -q 'xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/2002/08/xhtml/xhtml1-strict.xsd"' "${file}"; then
	echo -e "\n\"${file}\": should contain 'schemaLocation' XHTML schema declaration" >&2
	exit 42
    fi

    # ensure that there is max 1 h1 element - it's OK not to find any
    h1_count=$(grep --count '<h1[^>]*>' ${file} || true)
    if test ${h1_count} -gt 1; then
	echo -e "\n\"${file}\": contains more than one h1 element" >&2
	exit 42
    fi
    unset h1_count

  # ensure that there are no h5 elements - it's OK not to find any
  h5_count=$(grep --count '<h5[^>]*>' ${file} || true)
  if test ${h5_count} -gt 0; then
    echo -e "\n\"${file}\": contains ${h5_count} h5 elements" >&2
    exit 42
  fi
  unset h5_count

  # ensure that there are no h6 elements - it's OK not to find any
  h6_count=$(grep --count '<h6[^>]*>' ${file} || true)
  if test ${h6_count} -gt 0; then
    echo -e "\n\"${file}\": contains ${h6_count} h6 elements" >&2
    exit 42
  fi
  unset h6_count

  # should not contain '"<tag' - it's OK not to find any
  quote_start_tag=$(grep --count -E '"<[^/]' ${file} || true)
  if test ${quote_start_tag} -gt 0; then
    echo -e "\n\"${file}\": contains ${quote_start_tag} '\"<tag' occurrences" >&2
    exit 42
  fi
  unset quote_start_tag

  # should not contain '</tag>"' - it's OK not to find any
  end_tag_quote=$(grep --count -E '</[[:alpha:]]+>"' ${file} || true)
  if test ${end_tag_quote} -gt 0; then
    echo -e "\n\"${file}\": contains ${end_tag_quote} '</tag>\"' occurrences" >&2
    exit 42
  fi
  unset end_tag_quote

  # should contain a summary
  if ! (${MOLK_UTILS}/xml-select.sh ${file} copy xhtml "//xhtml:html//*[contains(@class,'summary')]" | grep -q summary); then
    echo -e "\n\"${file}\": should contain at least one element with the 'summary' class" >&2
    exit 42
  fi

  # H2, H3 and H4 elements should have an id so they can be linked to
  headers_missing_ids_xpath='(//xhtml:body//xhtml:h2 | //xhtml:body//xhtml:h3 | //xhtml:body//xhtml:h4)[count(@id)=0]'
  headers_missing_ids_count=$(${MOLK_UTILS}/xml-select.sh ${file} copy xhtml "count(${headers_missing_ids_xpath})")
  if test "${headers_missing_ids_count}" -gt 0; then
    headers_missing_ids=$(${MOLK_UTILS}/xml-select.sh ${file} copy xhtml "${headers_missing_ids_xpath}")
    echo -e "\n\"${file}\": contains ${headers_missing_ids_count} headers without ids (which are needed so the headers can be linked to): ${headers_missing_ids}" >&2
    exit 42
  fi
  unset headers_missing_ids_xpath
  unset headers_missing_ids_count

  # tables should have a summary attribute describing the content
  tables_missing_summaries_xpath='//xhtml:body//xhtml:table[count(@summary)=0]'
  tables_missing_summaries_count=$(${MOLK_UTILS}/xml-select.sh ${file} copy xhtml "count(${tables_missing_summaries_xpath})")
  if test ${tables_missing_summaries_count} -gt 0; then
    tables_missing_summaries=$(${MOLK_UTILS}/xml-select.sh ${file} copy xhtml "${tables_missing_summaries_xpath}")
    echo -e "\n\"${file}\": contains ${tables_missing_summaries_count} tables without summary attributes: ${tables_missing_summaries}" >&2
    exit 42
  fi
  unset tables_missing_summaries_xpath
  unset tables_missing_summaries_count

  # headers should not end in a period
  header_period_regexp="\. *</[hH][1234]>"
  if grep -qE "${header_period_regexp}" "${file}"; then
    echo -e "\n\"${file}\": contains headers ending in a period ('.')" >&2
    grep -E "${header_period_regexp}" "${file}" >&2
    exit 42
  fi
  unset header_period_regexp

  # headers should start with a capital letter
  header_capital_regexp="<[hH][1234][^>]*>[^A-Z0-9]"
  if grep -qE "${header_capital_regexp}" "${file}"; then
    echo -e "\n\"${file}\": contains headers which do not start with a capital letter" >&2
    grep -E "${header_capital_regexp}" "${file}" >&2
    exit 42
  fi
  unset header_capital_regexp

  # punctuation checks 2026 = dotdotdot in unicode
  punctuation_regexp="[^\.\!\)?:;>$(printf %b '\u2026')] *"
  
  # paragraphs should end in punctuation
  p_punctuation_regexp="${punctuation_regexp}</p>"
  if grep "</p>"  "${file}" | grep -qE "${p_punctuation_regexp}"; then
    echo -e "\n\"${file}\": contains paragraphs that do not end in punctuation" >&2
    grep "</p>"  "${file}" | grep -E "${p_punctuation_regexp}" >&2
    exit 42
  fi
  unset p_punctuation_regexp

  # DDs should end in punctuation
  dd_punctuation_regexp="${punctuation_regexp}</dd>"
  if grep "</dd>"  "${file}" | grep -vE "^[[:space:]]*</dd>[[:space:]]*$" | grep -qE "${dd_punctuation_regexp}"; then
    echo -e "\n\"${file}\": contains DDs that do not end in punctuation" >&2
    grep "</dd>"  "${file}" | grep -vE "^[[:space:]]*</dd>[[:space:]]*$" | grep -E "${dd_punctuation_regexp}" >&2
    exit 42
  fi
  unset dd_punctuation_regexp

  unset punctuation_regexp
  
  # paragraphs should start with a capital letter
  p_capital_regexp="<[pP]( [^>]*)?>[^A-Z0-9]"
  if grep -qE "${p_capital_regexp}" "${file}"; then
    echo -e "\n\"${file}\": contains paragraphs which do not start with a capital letter" >&2
    grep -E "${p_capital_regexp}" "${file}" >&2
    exit 42
  fi
  unset p_capital_regexp

  # index.html should always be explicitly linked to (not "abc/" or "abc")
  redirected_index_regexp='href="/?[^#"/\.][^"/\.]+/?"'
  if grep -qE "${redirected_index_regexp}" "${file}"; then
    echo -e "\n\"${file}\": contains redirected index links" >&2
    grep -E "${redirected_index_regexp}" "${file}" >&2
    exit 42
  fi
  unset redirected_index_regexp

  # href attributes should never be empty
  if grep -E 'href=""' "${file}"; then
    echo -e "\n\"${file}\": contains empty href attributes" >&2
    exit 42
  fi

  # anchors in href attributes should never be empty
  if grep -E 'href="[^"]*#"' "${file}"; then
    echo -e "\n\"${file}\": contains empty link anchors" >&2
    exit 42
  fi

done

# validate against the XHTML w3c Schema
output_rc=0
output=$(xmlstarlet val --err --list-bad --xsd "${MOLK_LIBRARIES}/xml-validation/xhtml1-strict.xsd" ${without_ignored[@]} 2>&1) || output_rc="${?}"

if test "${output_rc}" -ne 0; then
  echo -e "\nStrict XHTML1 validation failed with return code ${output_rc}: '${output}'" >&2
  exit 42
fi
