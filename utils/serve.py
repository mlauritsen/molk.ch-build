#!/usr/bin/python3

import http.server
import socketserver
import sys

PORT = 8042

Handler = http.server.SimpleHTTPRequestHandler
if len(sys.argv) > 1 and sys.argv[1] == "-xhtml":
    Handler.extensions_map.update({'.html': 'application/xhtml+xml', });

httpd = socketserver.TCPServer(("", PORT), Handler)

print("Serving at port", PORT)
httpd.serve_forever()
