#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of css files

if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-css-files\nwas: $0 $*" >&2
  exit 42
fi

# setup
tmp=${MOLK_TMP}/embed-images.css
error=${MOLK_TMP}/embed-images-error.txt
declare -r tmp

for file in "$@"; do
  if ! test -w ${file}; then
    echo -e "\n\"${file}\" must be writable" >&2
    exit 42
  fi

  if ! java -jar ${MOLK_LIBRARIES}/cssembed/cssembed-0.3.2.jar --charset UTF-8 --verbose "${file}" -o "${tmp}" &> ${error}; then
    echo -e "\n\"${file}\": Image embedding failed - see ${error}" >&2
    exit 42
  fi
  mv "${tmp}" "${file}" || exit 42
done
