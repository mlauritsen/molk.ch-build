#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# given a file and an xpath, delete the indicated node(s) from the file

if test $# -ne 2; then
  echo -e "\nUsage: $0 file xpath\nwas: $0 $*" >&2
  exit 42
fi

# setup
file=$1
xpath=$2
tmp=${MOLK_TMP}/delete-xml.tmp
declare -r file xpath tmp

xmlstarlet ed --pf \
  --delete "${xpath}" \
  "${file}" 1> "${file}".tmp \
  2>/dev/null \
&& mv "${file}".tmp "${file}"
