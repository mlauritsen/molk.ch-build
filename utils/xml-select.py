#!/usr/bin/env python
import sys

from lxml.etree import parse;
from lxml.etree import tostring;
from sys import stdin;

root = parse(stdin);
selected = root.xpath('//xhtml:html/xhtml:body/node()', namespaces={'xhtml': 'http://www.w3.org/1999/xhtml'})
# print(len(selected))
print(tostring(selected[1]))
