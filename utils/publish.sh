#!/bin/bash
# Copyright 2009-2016 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# Check configuration

if test -z "${MOLK_FTP_USERNAME:-}"; then
    echo -e "\nNo ftp username provided in MOLK_FTP_USERNAME" >&2
    exit 42
fi

if test -z "${MOLK_FTP_PASSWORD:-}"; then
    echo -e "\nNo ftp password provided in MOLK_FTP_PASSWORD" >&2
    exit 42
fi

if test -z "${MOLK_FTP_SERVER:-}"; then
    echo -e "\nNo ftp servername provided in MOLK_FTP_SERVER" >&2
    exit 42
fi

if test -z "${MOLK_FTP_REMOTE_DIR:-}"; then
    echo -e "\nNo remote directory provided in MOLK_FTP_REMOTE_DIR" >&2
    exit 42
fi

# Setup
declare -r publish_dir="${MOLK_TMP}"/publish
rm -rf "${publish_dir-DoesNotExist}"
mkdir "${publish_dir}"

# Sitecopy storepath (local copy of remote site)
declare -r repo_dir="${publish_dir}"/repo
mkdir "${repo_dir}"
chmod u+rwx,go-rwx "${repo_dir}"

# Create local sitecopy workspace (where sitecopy expects you to edit the files)
declare -r workspace_dir="${publish_dir}"/workspace
mkdir "${workspace_dir}"
cd "${workspace_dir}"

# Configuration
declare -r rc_file="${publish_dir}"/rcfile
(cat <<EOF
site "${MOLK_DOMAIN}"
username "${MOLK_FTP_USERNAME}"
password "${MOLK_FTP_PASSWORD}"
server "${MOLK_FTP_SERVER}"
remote "${MOLK_FTP_REMOTE_DIR}"
local "${workspace_dir}"
state checksum
EOF
) > "${rc_file}"
chmod u+rw,go-rwx "${rc_file}"

#     sitecopy is a handful, do it manually
echo 'sitecopy is a handful, do it manually'
cat <<EOF
sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --fetch "${MOLK_DOMAIN}"
sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --synch "${MOLK_DOMAIN}"
rm -rf "${workspace_dir-DoesNotExist}"'/*'
cp -r "${MOLK_DEST}"/* "${workspace_dir-DoesNotExist}"
sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --list "${MOLK_DOMAIN}"
sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --update "${MOLK_DOMAIN}"
EOF
exit 0

# Fetch remote files into sitecopy storepath
if ! sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --fetch "${MOLK_DOMAIN}" > /dev/null; then
  echo -e "\nCould not fetch remote files" >&2
  exit 42
fi

# Fetch local copy of remote files into workspace
if ! sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --synch "${MOLK_DOMAIN}" > /dev/null; then
  echo -e "\nCould not create local copy in ${workspace_dir}" >&2
  exit 42
fi


# Delete old contents from workspace
if ! rm -rf "${workspace_dir-DoesNotExist}"'/*'; then
  echo -e "\nCould not delete old contents from ${workspace_dir}" >&2
  exit 42
fi

# Copy new contents to workspace
if ! cp -r "${MOLK_DEST}"/* "${workspace_dir-DoesNotExist}"; then
  echo -e "\nCould not copy new contents from ${MOLK_DEST} to ${workspace_dir}" >&2
  exit 42
fi

# List changes to be published in the build log
sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --list "${MOLK_DOMAIN}" > "${MOLK_LOG}" && true

# Publish the new contents
if ! sitecopy --rcfile="${rc_file}" --storepath="${repo_dir}" --update "${MOLK_DOMAIN}" > /dev/null; then
  echo -e "\nCould not publish new contents from ${workspace_dir}" >&2
  exit 42
fi
