#!/bin/bash
# Copyright 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://www.molk.ch/licence.txt
set -o nounset
set -o errexit
trap 'echo "Error on line ${LINENO}: exited with status ${?}" >&2' ERR

# Write a log message to stderr
molk_log() {
    if test $# -eq 0; then
	echo "MOLK-LOG $0: Ignoring empty log entry" >&2
	return
    fi
    echo "MOLK-LOG $0: $@" >&2
}

# Write a warning to stderr
molk_warn() {
    molk_log 'WARN - '"$@"
}

# Write an error to stderr and terminate with rc 42
molk_error() {
    molk_log 'ERROR - '"$@"
    exit 42
}
