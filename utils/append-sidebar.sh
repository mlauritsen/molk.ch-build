#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input: the name of an HTML file in ${MOLK_TRANSFORM_PAGE}

# given a partially constructed HTML file, append the sidebar
# and insert the appropriate subnav:
# - files in the root dir do not have a subnav
# - files in direct subdirs of the root dir which do not contain subdirs
#   (=> generated subnav fragment is empty) do not have a subnav
# - all other files have a subnav

if test $# -ne 3; then
  echo -e "\nUsage: $0 src-file relative-dir filename\nwas: $0 $*" >&2
  exit 42
fi

declare -r src_file=$1
declare -r relative_dir=$2
declare -r filename=$3
declare -r file=${MOLK_TRANSFORM_PAGE}/${filename}
declare -r tmp_file="${file}.tmp"
declare -r sidebar_fragment_orig=${MOLK_TMP_FRAGMENTS}/sidebar.xml
declare -r sidebar_fragment=${MOLK_TMP}/sidebar.xml
declare -r subnav_token='<div id="subnav"\/>'
declare -r subnav_parent_id='subnav-parent'
declare -r sibnav_token='<div id="sibnav"\/>'

# validation
if ! test -w "${file}"; then
  echo -e "\n\"${file}\": must exist and be readable" >&2
  exit 42
fi

if ! test -r "${sidebar_fragment_orig}"; then
  echo -e "\n\"${sidebar_fragment_orig}\" must be a readable file" >&2
  exit 42
fi

if ! grep -q "${subnav_token}" "${sidebar_fragment_orig}"; then
  echo -e "\n\"${sidebar_fragment_orig}\" must contain the subnav token: \"${subnav_token}\"" >&2
  exit 42
fi

# make a working copy of the sidebar fragment
cp "${sidebar_fragment_orig}" "${sidebar_fragment}" || \
  { echo -e "Could not copy ${sidebar_fragment_orig}"; exit 42; }

subnav_token_regexp="^[[:space:]]*${subnav_token}[[:space:]]*$"

# determine whether a subnav should be appended
#(relative-dir is a subdir, and the subnav fragment is not empty)
[[ "${relative_dir}" =~ ^([^/]+)/ ]] && subnav_fragment=${MOLK_TMP_FRAGMENTS}/subnav-"${BASH_REMATCH[1]}".xml;

if test "${?}" -eq 0 && test -s "${subnav_fragment}"; then
#if  && test -s ${MOLK_TMP_FRAGMENTS}/subnav-"${BASH_REMATCH[1]}".xml; then
#  subnav_fragment=${MOLK_TMP_FRAGMENTS}/subnav-"${BASH_REMATCH[1]}".xml

  # insert the subnav fragment into the sidebar
  sed --in-place "/${subnav_token_regexp}/r ${subnav_fragment}" "${sidebar_fragment}"

  unset subnav_fragment

# fail if relative-dir is a sub-sub-dir and the subnav fragment does not exist
elif [[ "${relative_dir}" =~ ^[^/]+/[^/]+ ]] && ! test -r "${subnav_fragment}"; then
    echo -e "\n\"${file}\": \"${relative_dir}\" implies a non-existing subnav: \"${subnav_fragment}\"" >&2
    exit 42
else # remove the subnav-parent element
  if ! ${MOLK_UTILS}/delete-xml.sh "${sidebar_fragment}" '//*[@id="subnav-parent"]'; then
    echo -e "\n\"${sidebar_fragment}\": Failed to delete subnav-parent element" >&2
    exit 42
  fi

  # remove the <?xml header if it was inserted
  sed --in-place --expression "/<\?xml /d" "${sidebar_fragment}"
fi

# remove the subnav token
if ! sed --in-place "/${subnav_token_regexp}/d" "${sidebar_fragment}"; then
  echo -e "\n\"${file}\": error while removing subnav token in '${sidebar_fragment}'" >&2
  exit 42
fi

# insert the sibnav at the token
sibnav_token_regexp="^[[:space:]]*${sibnav_token}[[:space:]]*$"
sibnav_fragment="${MOLK_TMP_FRAGMENTS}/sibnav.xml"
${MOLK_UTILS}/create-sibnav.sh "${src_file}" > "${sibnav_fragment}"
sed --in-place "/${sibnav_token_regexp}/r ${sibnav_fragment}" "${sidebar_fragment}"

# remove the sibnav token
if ! sed --in-place "/${sibnav_token_regexp}/d" "${sidebar_fragment}"; then
  echo -e "\n\"${file}\": error while removing sibnav token in '${sidebar_fragment}'" >&2
  exit 42
fi
unset sibnav_fragment
unset sibnav_token_regexp

# append the sidebar fragment to the input file
cat "${sidebar_fragment}" >> "${file}"

# remove the temporary sidebar
rm -f "${sidebar_fragment}" || \
  { echo "Could not remove ${sidebar_fragment}"; exit 42; }
