#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# input should be a list of JavaScript files
if test $# -eq 0; then
  echo -e "\nUsage: $0 list-of-files\nwas: $0 $*" >&2
  exit 42
fi

for file in "$@"; do
  if ! test -r ${file}; then
    echo -e "\n\"${file}\": does not exist" >&2
    exit 42
  fi

  result=${MOLK_TMP}/format-js-result.txt
  if ! emacs -batch "${file}" --load "${MOLK_UTILS}/resources/format-js.el" --no-splash > "${MOLK_LOG}" 2>&1; then
    echo -e "\n\"${file}\": Formatting failed" >&2
    exit 42
  fi

  # cleanup whitespace
  if ! ${MOLK_UTILS}/clean-whitespace.sh ${file}; then
    echo -e "\n\"${file}\": Whitespace-cleaning failed" >&2
    exit 42
  fi
done
