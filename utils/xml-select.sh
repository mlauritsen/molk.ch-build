#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# output the xml subtree(s) matching the xpath to stdout
# examples: 
# xpath="//xhtml:html/xhtml:body/*[@class='summary']"
# xpath="//atom:feed/atom:updated"
# Fail if the xpath is invalid
# Answer "" if the xpath does not match anything

if test $# -ne 4; then
  echo -e "\nUsage: $0 input-file '[copy|value|]' '[xhtml|atom|]' xpath\nwas: $0 $*" >&2
  exit 42
fi

declare -r file=$1
declare -r xpath=$4

# $2 Select method: Copy vs. Value
case $2 in
    copy)
	option='--copy-of'
	;;
    value)
	option='--value-of'
	;;
    *)
	echo -e "\nUnknown select option: $2" >&2
	exit 42
	;;      
esac
declare -r option

# $3 Namespace: XHTML or Atom
case $3 in
    '')
	ns=
		  ;;
    xhtml)
	ns='-N xhtml=http://www.w3.org/1999/xhtml'
	;;
    atom)
	ns='-N atom=http://www.w3.org/2005/Atom'
	;;
    *)
	echo -e "\nUnknown namespace: $3" >&2
	exit 42
	;;      
esac
declare -r ns

if ! test -r ${file}; then
  echo -e "\n\"${file}\": Should be a readable file" >&2
  exit 42
fi

output_rc=0

output=$(xmlstarlet select ${ns} --template "${option}" "${xpath}" "${file}" 2>>"${MOLK_LOG}") || output_rc=$(echo $?)

# Fail if the xpath is invalid
if test "${output_rc}" -eq 4; then
  echo -e "\n\"${file}\": XML select failed - invalid xpath: ${xpath}" >&2
  exit 42
fi

# Answer "" if the xpath does not match anything
if test "${output_rc}" -eq 1; then
  exit 0
fi

# Fail on all other RCs
if test "${output_rc}" -ne 0; then
  echo -e "\n\"${file}\": XML select failed with return code ${output_rc}: ${xpath}" >&2
  exit 42
fi

echo "${output}" \
  | sed --expression='s/[[:blank:]]*xmlns="[^"]*"//g' \
  | sed --expression='s/[[:blank:]]*xmlns:xsi="[^"]*"//g'
