#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# check

if test $# -ne 1; then
  echo -e "\nUsage: $0 directory\nwas: $0 $*" >&2
  exit 42
fi

dir=$1
declare -r dir

# argument should be a directory
if ! test -d "${dir}"; then
  echo -e "\n\"${dir}\": Must be a directory" >&2
  exit 42
fi

# if dir contains no css files, bail
if test $(find "${dir}" -name "*.css" | wc -l) -eq 0; then
  echo -e "\n\"${dir}\": Does not contain any css files" >&2
  exit 42
fi

# destination is parent dir/dirname.css
dest=${dir%/}.css

# if the destination file exists, bail
if test -f "${dest}"; then
  echo -e "\n\"${dest}\": File exists - cannot concatenate \"${dir}\"" >&2
  exit 42
fi

# concatenate css files in dir into destination css file
cat "${dir}"/*.css > "${dest}" || exit 42
