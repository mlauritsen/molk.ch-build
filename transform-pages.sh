#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

pages=($(cd ${MOLK_CONTENT} && find . -type f -name "*.html"))

# validation
if test ${#pages[*]} == 0; then
  echo "No pages found in ${MOLK_CONTENT}"
  exit 0
fi

echo -n "Transforming pages"

# iterate over the pages
for page in "${pages[@]}"; do
  # if the filename starts with "./", remove it
  page=${page#./}

  # ignore pages containing MOLK-TODO
  if ! grep -q MOLK-TODO ${MOLK_CONTENT}/${page}; then

    # transform pages containing "<?molk "
    if grep -q "<?molk " ${MOLK_CONTENT}/${page}; then
      if ! ${MOLK_UTILS}/transform-page.sh ${MOLK_CONTENT}/${page} ${MOLK_DEST}/${page}; then
        echo -e "\n\"${page}\": Error while transforming" >&2
        exit 42
      fi
    else
      # simply copy pages which do not contain "<?molk ",
      cp "${MOLK_CONTENT}/${page}" "${MOLK_DEST}/${page}"
    fi

    # page was handled succesfully
    echo -n "."
  else
    # page was ignored (ignored pages are listed by name in ()'s)
    echo -n "(${page})"
  fi
done
