#!/bin/bash
source "${MOLK_BUILD_UTILS}/core-utils.sh"

# build statistics
echo "Build statistics:"

build_scripts=$(ls -l ${MOLK_BUILD}/*.sh | wc -l)
printf "  target scripts:   %3s\n" ${build_scripts}
unset build_scripts

util_scripts=$(ls -l ${MOLK_BUILD_UTILS}/*.sh | wc -l)
printf "  utility scripts:  %3s\n" ${util_scripts}
unset util_scripts

test_scripts=$(ls -l ${MOLK_BUILD_UTILS}/test/*.sh | wc -l)
printf "  test scripts:     %3s\n" ${test_scripts}
unset test_scripts

assertions=$(grep "exit[[:space:]]*43" ${MOLK_BUILD_UTILS}/test/*.sh | wc -l)
printf "  test assertions:  %3s\n" ${assertions}
unset assertions

todos=$(grep " TODO " $(find ${MOLK_BUILD}) | grep -v "~:" | grep -v "/libraries/" | grep -v statistics.sh | wc -l)
printf "  TODOs:            %3s\n" ${todos}
unset todos
