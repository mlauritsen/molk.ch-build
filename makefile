# creates the site from the source content
# must be invoked from the build directory!

# setup
export MOLK_UTILS          := $(MOLK_BUILD)/utils
export MOLK_LOG            := $(MOLK_TMP)/molk.log
export MOLK_UTILS          := $(MOLK_BUILD)/utils
export MOLK_SRC_FRAGMENTS  := $(MOLK_SRC)/fragments
export MOLK_TMP_FRAGMENTS  := $(MOLK_TMP)/fragments
export MOLK_CONTENT        := $(MOLK_SRC)/content
export MOLK_LIBRARIES      := $(MOLK_BUILD)/libraries
export MOLK_TRANSFORM_PAGE := $(MOLK_TMP)/transform-page
export MOLK_URL            := http://$(MOLK_DOMAIN)

.PHONY: setup format-src check-src copy-static style transform-pages-setup initialise-feeds transform-pages finalise-feeds minify-dest check-dest build deploy upload mirror diff clean debug statistics

# 'build' life cycle target:
#  1. setup
#  2. format-src
#  3. check-src
#  4. copy-resources
#  5. style
#  6. transform-pages-setup
#  7. initialise-feeds
#  8. transform-pages
#  9. finalise-feeds
# 10. minify-dest
# 11. check-dest
# (12. deploy - must be invoked separately)
build: setup format-src check-src copy-static style transform-pages-setup initialise-feeds transform-pages finalise-feeds minify-dest check-dest

# check env configuration, create directories etc.
setup:
	@$(MOLK_UTILS)/run-target.sh $(MOLK_BUILD)/setup.sh

# format source for readability
format-src: setup
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/format-src.sh

# check the source files (HTML, feeds, JS, CSS, etc.)
check-src: format-src
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/check-src.sh

# copy resources (non-HMTL files in $MOLK_CONTENT) to $MOLK_DEST
copy-static: check-src
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/copy-static.sh

# join and copy the stylesheets, embedding images
style: copy-static
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/style.sh

# setup the MOLK_TMP/transform-page directory
transform-pages-setup: style ${MOLK_SRC}/fragments/*.html
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/transform-pages-setup.sh

# create the feed files in MOLK_DEST, populating them with entries
initialise-feeds: transform-pages-setup
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/initialise-feeds.sh

# transform the pages in MOLK_CONTENT
transform-pages: initialise-feeds
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/transform-pages.sh

# finalise the feeds in MOLK_DEST
finalise-feeds: transform-pages
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/finalise-feeds.sh

# minify destination to optimise bandwidth consumption
minify-dest: finalise-feeds
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/minify-dest.sh

# check the destination files (HTML, feeds, JS, CSS, etc.)
check-dest: minify-dest
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/check-dest.sh


# Deploy a previously generated site
deploy: setup
	@${MOLK_UTILS}/run-target.sh ${MOLK_BUILD}/deploy.sh

# generate statistics about the build and the site
statistics:
	@${MOLK_BUILD}/statistics.sh

debug:
	@echo "Variables: ${.VARIABLES}"
	@echo "Build:       ${MOLK_BUILD}"
	@echo "Temporary:   ${MOLK_TMP}"
	@echo "Source:      ${MOLK_SRC}"
	@echo "Destination: ${MOLK_DEST}"

clean:
	@${MOLK_UTILS}/remove.sh "${MOLK_DEST}"
	@${MOLK_UTILS}/remove.sh "${MOLK_TMP}"
