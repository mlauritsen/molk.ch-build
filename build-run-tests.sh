#!/bin/bash
source "${MOLK_BUILD_UTILS}/core-utils.sh"
MOLK_TEST_LOG=/tmp/molk-test.log

# run the test scripts in the current directory

scripts=($(ls ./*-test.sh))

# setup
echo "Running tests in \"$(pwd)\":"

declare -a failed=()

# remove tmp to let the tests run clean
if ! rm -rf tmp; then
    echo "Could not remove directory"
    exit 43
fi

# write legend and progress reference

echo 'Legend: "." and ":": OK, "F": Failed'
declare -i counter=0
for script in "${scripts[@]}"; do
    counter="$(($counter+1))"
    if test "${counter}" -ge 10; then
	echo -n "|"
	counter=0
    else
	echo -n "-"
    fi
done
if test "${counter}" -gt 0; then
    echo -n "|"
fi
echo ""

# run tests

counter=0
declare -i START_TIME="${SECONDS}"
for script in "${scripts[@]}"; do
    # running test
    counter="$(($counter+1))"
    if ./${script} &>> "${MOLK_TEST_LOG}"; then
	# the test passed
	if test "${counter}" -ge 10; then
	    echo -n ":"
	    counter=0
	else
	    echo -n "."
	fi
    else
	# the test failed
	failed[${#failed[*]}]="${script}"
	echo -n "F"

	# remove tmp to let the next test run clean
	if ! rm -rf tmp; then
	    echo "Could not remove directory"
	    exit 43
	fi
    fi
done

# print summary
declare -i DURATION="$(((${SECONDS} - ${START_TIME}) / 60))"
echo -en "\nRan ${#scripts[@]} test scripts in ${DURATION} minutes - "

if test ${#failed[*]} -gt 0; then
    echo "${#failed[*]} scripts failed: ${failed[@]}"
fi

if test ${#failed[*]} -eq 0; then
    echo "OK!"
else
    exit 43
fi
