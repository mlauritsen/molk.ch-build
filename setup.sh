#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt

# Checks that all generation-settings are present
# Deployment-specific settings are not checked here,
# since that is a separate operation (see utils/deploy.sh)

if test "${MOLK_BUILD:-isunset}" == "isunset"; then
  echo -e "\nMOLK_BUILD must be set to the molk.ch-build root directory" >&2
  exit 42
fi

# If MOLK_BUILD is present, so is MOLK_UTILS
source "${MOLK_UTILS}/core-utils.sh"

if test "${MOLK_DEST:-isunset}" == "isunset"; then
  echo -e "\nMOLK_DEST must be set to the site destination directory" >&2
  exit 42
fi

if test "${MOLK_DOMAIN:-isunset}" == "isunset"; then
  echo -e "\nMOLK_DOMAIN must be set to the site domain name, e.g. 'molk.ch'" >&2
  exit 42
fi

if test "${MOLK_SRC:-isunset}" == "isunset"; then
  echo -e "\nMOLK_SRC must be set to the site source directory" >&2
  exit 42
fi

if test "${MOLK_TMP:-isunset}" == "isunset"; then
  echo -e "\nMOLK_TMP must be set to directory where temporary files can be written" >&2
  exit 42
fi

if test "$(${MOLK_UTILS}/is-online.sh)" != "true"; then
  echo -e "\nRunning in offline mode!" >&2
fi

mkdir -p "${MOLK_TMP}"
mkdir -p "${MOLK_DEST}"

# Delete target markers so each task will run exactly once each time the build runs
TARGETS=${MOLK_TMP}/targets/*
if ls ${TARGETS} 2> /dev/null; then
    rm ${TARGETS}
fi

# Log and build info
echo    "${MOLK_DOMAIN}"' build: '"$(date)" | tee "${MOLK_LOG}"
echo    '  Build:       '"${MOLK_BUILD}"    | tee "${MOLK_LOG}"
echo    '  Log:         '"${MOLK_LOG}"      | tee "${MOLK_LOG}"
echo    '  Source:      '"${MOLK_SRC}"      | tee "${MOLK_LOG}"
echo    '  Destination: '"${MOLK_DEST}"     | tee "${MOLK_LOG}"
echo    '  TMP:         '"${MOLK_TMP}"      | tee "${MOLK_LOG}"
echo -n '  Setup:      '                    | tee "${MOLK_LOG}"

# Run pre-build hook
${MOLK_UTILS}/run-hook.sh pre-build.sh

echo ' OK' > "${MOLK_LOG}"
