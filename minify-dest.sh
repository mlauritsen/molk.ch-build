#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source "${MOLK_UTILS}/selectors.sh"

# minify $MOLK_DEST files
declare -ar html_files=($(find ${MOLK_DEST} -type f -name "*.html"))
declare -ar css_files=($(find ${MOLK_DEST} -type f -name "*.css"))
declare -ar js_files=($(select_molk_js "${MOLK_DEST}"))
#declare -ar js_files=($(find ${MOLK_DEST} -type f -name "*.js"))
declare -ar feed_files=($(find ${MOLK_DEST} -type f -name feed.xml))
total=$((${#html_files[@]} + ${#css_files[@]} + ${#js_files[@]} + ${#feed_files[@]}))
echo -n "Minifying ${total} destination files"

# HTML files
if test ${#html_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/minify-html.sh ${html_files[@]}; then
    echo -e "\nFailed to minify pages" >&2
    exit 42
  fi
  echo -n "."
fi

# CSS files
if test ${#css_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/minify-css.sh ${css_files[@]}; then
    echo -e "\nFailed to minify CSS files" >&2
    exit 42
  fi
  echo -n "."
fi

# JS files
if test ${#js_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/minify-js.sh ${js_files[@]}; then
    echo -e "\nFailed to minify JavaScript files" >&2
    exit 42
  fi
  echo -n "."
fi

# feed files
if test ${#feed_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/minify-feed.sh ${feed_files[@]}; then
    echo -e "\nFailed to minify feed files" >&2
    exit 42
  fi
  echo -n "."
fi
