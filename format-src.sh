#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"
source ${MOLK_UTILS}/selectors.sh

# format $MOLK_SRC files
declare -ar html_files=($(find ${MOLK_CONTENT} -type f -name "*.html"))
declare -ar fragments=($(ls --format=single-column ${MOLK_SRC_FRAGMENTS}/*.html 2> /dev/null))
declare -ar css_files=($(find ${MOLK_SRC} -type f -name "*.css"))
declare -ar js_files=($(select_molk_js "${MOLK_SRC}"))
#js_files=($(find ${MOLK_SRC} -type f -name "*.js"))
declare -ar feed_files=($(find ${MOLK_CONTENT} -type f -name feed.xml))
total=$((${#html_files[@]} + ${#fragments[@]} + ${#css_files[@]} + ${#js_files[@]} + ${#feed_files[@]}))
echo -n "Formatting ${total} source files"

# format HTML pages
if test ${#html_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/format-html.sh ${html_files[@]}; then
    echo -e "\nSource page formatting failed" >&2
    exit 42
  fi
  echo -n "."
fi

# format fragments
if test ${#fragments[@]} -ge 1; then
  if ! ${MOLK_UTILS}/format-html.sh ${fragments[@]}; then
    echo -e "\nFragment formatting failed" >&2
    exit 42
  fi
  echo -n "."
fi

# format CSS files
if test ${#css_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/format-css.sh ${css_files[@]}; then
    echo -e "\nCSS formatting failed" >&2
    exit 42
  fi
  echo -n "."
fi

# format JavaScript files
if test ${#js_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/format-js.sh ${js_files[@]}; then
    echo -e "\nJavaScript formatting failed" >&2
    exit 42
  fi
  echo -n "."
fi

# format feed files
if test ${#feed_files[@]} -ge 1; then
  if ! ${MOLK_UTILS}/format-feed.sh ${feed_files[@]}; then
    echo -e "\nFeed formatting failed" >&2
    exit 42
  fi
  echo -n "."
fi
