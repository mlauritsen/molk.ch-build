#!/bin/bash
source "${MOLK_BUILD_UTILS}/core-utils.sh"

# TODO check that MOLK_BUILD_VERSION is set

if test "${MOLK_BUILD_VERSION:-isunset}" == "isunset"; then
  echo -e "\nMOLK_BUILD_VERSION is mandatory" >&2
  exit 42
fi

# setup

base=molk.ch-build-${MOLK_BUILD_VERSION}
archived=${base}.tar
compressed=${archived}.bz2
declare -r base archived compressed

# check

if test -f "${archived}"; then
  echo -e "\n\"${archived} already exists" >&2
  exit 42
fi

if test -f "${compressed}"; then
  echo -e "\n\"${compressed} already exists" >&2
  exit 42
fi

# generate file list

files=($(find . -type f | grep -vE "(/libraries)|(/tbd)"))
echo "Packaging ${#files[@]} files"

# archive

tar --create --verify --file "$archived" ${files[@]}
if test $? -ne 0; then
  echo -e "\nArchiving failed" >&2
  exit 42
fi
echo "Created backup: $(ls -hl ${archived})"

# compress

bzip2 --verbose "${archived}"
bzip2 --test "${compressed}"
if test $? -ne 0; then
  echo -e "\nCompressing failed" >&2
  exit 42
fi
echo "Compressed backup: $(ls -hl ${compressed})"

# result

if test "${MOLK_BUILD_VERBOSE}" == "true"; then
  tar jtf "${compressed}"
fi

size=$(($(stat --format=%s ${compressed}) / 1000))
echo "Build version ${MOLK_BUILD_VERSION} packaged (${size} kb)"
