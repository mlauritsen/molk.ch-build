#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
set -o nounset
set -o errexit

# invoke the molk.ch-build makefile
# e.g. ./build.sh build-run-tests

# check parameters
if test $# -eq 0; then
  echo -e "\nUsage: $0 <targets>\nwas: $0 $*" >&2
  exit 42
fi

time make -f build.makefile ${@}
