#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# setup
src_dir=${MOLK_SRC}/style
tmp_dir=${MOLK_TMP}/style
dest_dir=${MOLK_DEST}/style
base=$(ls -l ${src_dir}/*.css 2> /dev/null | wc -l)
dirs=$(ls -l ${src_dir} 2> /dev/null | grep "^d" | wc -l)
total=$((${base} + ${dirs}))
declare -r src_dir tmp_dir dest_dir base dirs total

if test "${total}" -eq 0; then
  echo 'No stylesheets to process'
  exit 0
fi

echo -n "Adding ${total} stylesheets: "

mkdir -p ${dest_dir}

# copy all style files to tmp
if ! cp -r "${src_dir}" "${MOLK_TMP}"; then
  echo -e "\n\"${src_dir}\": Could not copy CSS files to \"${MOLK_TMP}\"" >&2
  exit 42
fi

# embed images
css_files=($(find "${tmp_dir}" -name "*.css" | grep -v "/lib/"))
if ! ${MOLK_UTILS}/css-embed-images.sh ${css_files[@]}; then
  echo -e "\nFailed to embed CSS images" >&2
  exit 42
fi

# concatenate subdir css - it's OK not to find any
subdirs=($(cd "${tmp_dir}"; ls -1F | grep "/$" | grep -v "lib/" || true))
if test "${#subdirs[@]}" -gt 0; then
  for dir in ${subdirs[@]}; do
    if ! ${MOLK_UTILS}/css-concatenate.sh "${tmp_dir}/${dir}"; then
      echo -e "\n\"${dir}\": Failed to concatenate CSS files" >&2
      exit 42
    fi
  done
fi

# copy library css files to dest
if ! cp -r "${tmp_dir}"/lib/ "${dest_dir}"; then
  echo -e "\n\"${tmp_dir}/lib/\": Could not copy library CSS files to \"${dest_dir}\"" >&2
  exit 42
fi

# copy basedir css files to dest
if ! cp -r "${tmp_dir}"/*.css "${dest_dir}"; then
  echo -e "\n\"${tmp_dir}\": Could not copy CSS files to \"${dest_dir}\"" >&2
  exit 42
fi
