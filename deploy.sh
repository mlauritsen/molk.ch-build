#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# Check that git is clean

if ! ("${MOLK_UTILS}"/is-git-clean.sh "${MOLK_BUILD}"); then
    echo -e "\nmolk.ch-build repo is not clean: ${MOLK_BUILD}" >&2
    exit 42
fi

if ! ("${MOLK_UTILS}"/is-git-clean.sh "${MOLK_SRC}"); then
    echo -e "\nmolk.ch repo is not clean: ${MOLK_SRC}" >&2
    exit 42
fi

# Publish via ftp
if ! ("${MOLK_UTILS}"/publish.sh); then
    echo -e "\nPublish failed" >&2
    exit 42
fi

# Tag the published state in git
if ! ("${MOLK_UTILS}"/git-tag.sh "${MOLK_BUILD}"); then
    echo -e "\nmolk.ch-build tagging failed" >&2
    exit 42
fi

if ! ("${MOLK_UTILS}"/git-tag.sh "${MOLK_SRC}"); then
    echo -e "\nmolk.ch tagging failed" >&2
    exit 42
fi

#     sitecopy is a handful, do it manually
echo 'sitecopy is a handful, do it manually'
exit 0

# Run webcheck on the live site
declare -r MOLK_WEBCHECK="${MOLK_TMP}/molk.ch-webcheck"
mkdir "${MOLK_WEBCHECK}"
echo "Running: webcheck --quiet --output=${MOLK_WEBCHECK} ${MOLK_URL}"
if ! webcheck --quiet --output="${MOLK_WEBCHECK}" "${MOLK_URL}" &> /dev/null; then
  echo -e "\nError while running webcheck on ${MOLK_URL} into ${MOLK_WEBCHECK}" >&2
  exit 42
fi

echo "Webcheck results for ${MOLK_URL} in ${MOLK_WEBCHECK}"
