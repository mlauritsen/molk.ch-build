#!/bin/bash
source "${MOLK_BUILD_UTILS}/core-utils.sh"

# setup

target_scripts=($(ls -1 ${MOLK_BUILD}/*.sh | grep -v "build/build-"))
util_scripts=($(ls -1 ${MOLK_BUILD_UTILS}/*.sh | grep -v "build/build-"))
scripts=(${target_scripts[@]} ${util_scripts[@]})

target_test_scripts=($(ls ${MOLK_BUILD}/test/*.sh))
util_test_scripts=($(ls ${MOLK_BUILD_UTILS}/test/*.sh))
test_scripts=(${target_test_scripts[@]} ${util_test_scripts[@]})
unset target_test_scripts util_test_scripts

all_scripts=(${scripts[@]} ${test_scripts[@]})
declare -r scripts test_scripts all_scripts

result=0
declare -i result

function molk_message() {
  local message="$1"
  if test "${MOLK_BUILD_VERBOSE}" == "true"; then
    echo -e "${message}"
  fi
}

# checks that apply to target scripts
for script in ${target_scripts[@]}; do
  # only use exit codes 0 and 42
  invalid_exit_code=$(grep -e "^[[:blank:]]*exit[[:blank:]]" ${script} | grep -ve "^[[:blank:]]*exit 42$" | grep -ve "^[[:blank:]]*exit 0$")
  if test "${invalid_exit_code}" != ""; then
    molk_message "\"${script}\": Only use exit codes 0 and 42: \"${invalid_exit_code}\""
    result=$(($result + 1))
  fi
  unset invalid_exit_code

  # check that the script is used somewhere in the makefile
  filename=$(basename ${script})
  if ! grep -q "${filename}" ${MOLK_BUILD}/makefile && ! grep -q "${filename}" ${MOLK_BUILD}/build.makefile; then
    molk_message "\"${script}\": Is not used in makefile"
    result=$(($result + 1))
  fi
  unset filename
done
unset target_scripts

# checks that apply to util scripts
for script in ${util_scripts[@]}; do
  # should check #arguments
  if ! grep -q "test \$#" ${script}; then
    molk_message "\"${script}\": should check #arguments"
    result=$(($result + 1))
  fi

  # should contain usage description
  if ! grep -qE 'echo -e "\\nUsage: \$0 .*\\nwas: \$0 \$\*" >&2' ${script}; then
    molk_message "\"${script}\": should contain usage description"
    result=$(($result + 1))
  fi

  # only use exit codes 0 or 42
  not_0_or_42=$(grep -e "^[[:blank:]]*exit[[:blank:]]" ${script} | grep -vE "^[[:blank:]]*exit ((42)|0)$")
  if test "${not_0_or_42}" != ""; then
    molk_message "\"${script}\": Only use exit codes 0 or 42: \"${not_0_or_42}\""
    result=$(($result + 1))
  fi
  unset not_0_or_42

  # check that the script is used somewhere in the build
  filename=$(basename ${script})
  if ! grep -q "${filename}" ${MOLK_BUILD_UTILS}/*.sh ${MOLK_BUILD}/*.sh ${MOLK_BUILD}/makefile; then
    molk_message "\"${script}\": Does not seem to be used anywhere?"
    result=$(($result + 1))
  fi
  unset filename
done
unset util_scripts

# each script should have a test script
for script in ${scripts[@]}; do
  filename=$(basename ${script})

  # ignore build targets
  if ! [[ "${filename}" =~ ^build[\.-].* ]]; then
    # the test script should exist
    test_script=${filename%.sh}-test.sh
    if ! test -f "${script%${filename}}test/${test_script}"; then
      molk_message "\"${script}\": Test script (\"${test_script}\") does not exist?"
      result=$(($result + 1))
    fi  
    unset test_script
  fi
  unset filename
done

# checks that apply to test scripts
for script in ${test_scripts[@]}; do
  filename=$(basename ${script})

  # test script names should end in "-test.sh"
  if ! [[ "${filename}" =~ ^.*-test.sh$ ]]; then
    molk_message "\"${script}\": Test script name should end in \"-test.sh\""
    result=$(($result + 1))
  fi

  # the tested script should exist
  tested_script=${filename%-test.sh}.sh
  if ! test -f "${script%${filename}}../${tested_script}"; then
    molk_message "\"${script}\": Tested script (\"${tested_script}\") does not exist?"
    result=$(($result + 1))
  fi

  # only use exit code 43
  not_43=$(grep -E "^[[:blank:]]*exit[[:blank:]]" ${script} | grep -vE "^[[:blank:]]*exit 43$")
  if test "${not_43}" != ""; then
    molk_message "\"${script}\": Only use exit code 43: \"${not_43}\""
    result=$(($result + 1))
  fi
  unset not_43

  # scripts that create tmp should fail if it already exists
  if grep -qE "mkdir .*tmp" ${script} && ! grep -q "if ! mkdir tmp; then" ${script}; then
    molk_message "\"${script}\": Should properly create tmp: 'if ! mkdir tmp; then'"
    result=$(($result + 1))
  fi
  if grep -E "^[[:blank:]]*mkdir .*tmp[[:blank:]]*$" ${script}; then
    molk_message "\"${script}\": Should fail if tmp creation fails"
    result=$(($result + 1))
  fi

  # scripts that remove tmp should fail if the remove does
  if grep -qE "rm .*tmp" ${script} && ! grep -q "if ! rm -rf tmp; then" ${script}; then
    molk_message "\"${script}\": Should properly remove tmp: 'if ! rm -rf tmp; then'"
    result=$(($result + 1))
  fi
  if grep -E "^[[:blank:]]*rm .*tmp[[:blank:]]*$" ${script}; then
    molk_message "\"${script}\": Should fail if tmp remove fails"
    result=$(($result + 1))
  fi


  # Scripts that create tmp should remove it
  if grep -q "if ! mkdir tmp; then" ${script} && ! grep -q "if ! rm -rf tmp; then" ${script}; then
    molk_message "\"${script}\": tmp is created - it should also be removed"
    result=$(($result + 1))
  fi

  # tests that do not create tmp should not remove it
  if ! grep -q "if ! mkdir tmp; then" ${script} && grep -q "if ! rm -rf tmp; then" ${script}; then
    molk_message "\"${script}\": tmp is not created - it should not be removed"
    result=$(($result + 1))
  fi

  unset filename
done

# checks that apply to all scripts
for script in ${all_scripts[@]}; do
  # first line in script must be "#!/bin/bash"
  if ! ( head -1 ${script} | grep -q "#!/bin/bash"); then
    molk_message "\"${script}\": first line should be \"#!/bin/bash\""
    result=$(($result + 1))
  fi

  # must contain "set -o nounset"
  if ! grep -q "set -o nounset" ${script}; then
    molk_message "\"${script}\": must contain \"set -o nounset\""
    result=$(($result + 1))
  fi

  # must include copyright comment
  if ! grep -qE "^# Copyright .* Morten Lauritsen Khodabocus, info@molk.ch$" ${script}; then
    molk_message "\"${script}\": must include copyright comment"
    result=$(($result + 1))
  fi

  # must include licence comment
  if ! grep -qE "^# Licensed under the GPL: http://molk.ch/licence.txt$" ${script}; then
    molk_message "\"${script}\": must include licence comment"
    result=$(($result + 1))
  fi

  # never exit explicitly without an explicit exit code
  if grep -e "^[[:blank:]]*exit$" ${script}; then
    molk_message "\"${script}\": always specify an explicit exit code"
    result=$(($result + 1))
  fi

  # echo arguments should always be quoted
  unquoted_echos=$(grep -E "^[[:blank:]]*echo" ${script} | grep -vE "echo( -.*)* \"" | grep -vE "echo( -.*)* '" | wc -l)
  if test $unquoted_echos -gt 0; then
    molk_message "\"${script}\": contains ${unquoted_echos} unquoted echo's"
    result=$(($result + $unquoted_echos))
  fi
  unset unquoted_echos

  # error messages should be written on a new line
  errors_on_new_line=$(grep -E "echo .* >&2" ${script} | grep -vE 'echo -e "\\n.*" >&2' | wc -l)
  if test $errors_on_new_line -ne 0; then
    molk_message "\"${script}\": ${errors_on_new_line} error messages are not written on a new line"
    result=$(($result + $errors_on_new_line))
  fi
  unset errors_on_new_line

  # do not redirect stderr using 2>1
  two_into_one=$(grep -c "2[[:blank:]]*>[[:blank:]]*1" ${script})
  if test $two_into_one -gt 0; then
    molk_message "\"${script}\": contains 2>1 stderr redirection"
    result=$(($result + $two_into_one))
  fi
  unset two_into_one
done

# build util resources
resources=($(ls ${MOLK_BUILD_UTILS}/resources))
for resource in ${resources[@]}; do
  if test "${resource}" != "tbd"; then
    fullname=${MOLK_BUILD_UTILS}/${resource}

    # resource should not be a directory
    if test -d ${fullname}; then
      molk_message "\"${resource}\": Avoid subdirectories in build/utils/resources"
      result=$(($result + 1))

    # resource is used in util scripts
    elif ! grep -q "${resource}" ${MOLK_BUILD_UTILS}/*.sh; then
      molk_message "\"${resource}\": Is not used in util scripts"
      result=$(($result + 1))
    fi
    unset fullname
  fi
done
unset resources

# build util test resources
resources=($(ls ${MOLK_BUILD_UTILS}/test/resources))
for resource in ${resources[@]}; do
  if test "${resource}" != "tbd"; then
    fullname=${MOLK_BUILD_UTILS}/test/${resource}

    # test resource is used in util test scripts
    if ! grep -q "${resource}" ${MOLK_BUILD_UTILS}/test/*.sh; then
      molk_message "\"${resource}\": Is not used in util test scripts"
      result=$(($result + 1))
    fi
    unset fullname
  fi
done
unset resources

# check build dir for clutter
clutter=$(ls ${MOLK_BUILD} -1 --ignore="*.sh" --ignore="*makefile" --ignore="documentation" --ignore="libraries" --ignore="tbd" --ignore="test" --ignore="utils")
if test "${clutter}" != ""; then
  count=$(echo "${clutter}" | wc -l)
  molk_message "${count} clutter files found in ${MOLK_BUILD}:\n${clutter}"
  result=$(($result + $count))
  unset count
fi
unset clutter

# check build test dir for clutter
clutter=$(ls ${MOLK_BUILD_TEST} -1 --ignore="*-test.sh" --ignore="tbd" --ignore="resources")
if test "${clutter}" != ""; then
  count=$(echo "${clutter}" | wc -l)
  molk_message "${count} clutter files found in ${MOLK_BUILD_TEST}:\n${clutter}"
  result=$(($result + $count))
  unset count
fi
unset clutter

# check utils dir for clutter
clutter=$(ls ${MOLK_BUILD_UTILS} -1 --ignore="*.sh" --ignore="tbd" --ignore="test" --ignore="resources")
if test "${clutter}" != ""; then
  count=$(echo "${clutter}" | wc -l)
  molk_message "${count} clutter files found in ${MOLK_BUILD_UTILS}:\n${clutter}"
  result=$(($result + $count))
  unset count
fi
unset clutter

# check utils test dir for clutter
clutter=$(ls ${MOLK_BUILD_UTILS_TEST} -1 --ignore="*-test.sh" --ignore="tbd" --ignore="resources")
if test "${clutter}" != ""; then
  count=$(echo "${clutter}" | wc -l)
  molk_message "${count} clutter files found in ${MOLK_BUILD_UTILS_TEST}:\n${clutter}"
  result=$(($result + $count))
  unset count
fi
unset clutter

# count TODOs in build files
todos=$(grep "TODO" $(find .) | grep -vE "(MOLK-TODO)|(/libraries/)|(/tbd/)|(build-.*\.sh)")
if test "${todos}" != ""; then
  count=$(echo "${todos}" | wc -l)
  molk_message "${count} todos found in build files:\n${todos}"
  result=$(($result + $count))
  unset count
fi
unset todos

# check that build contains no /tbd/-files
tbds=($(find ${MOLK_BUILD} -path "*/tbd/*"))
if test ${#tbds[@]} -gt 0; then
  molk_message "${#tbds[@]} tbd-backup files found"
  result=$(($result + ${#tbds[@]}))
fi
unset tbds

# check that build contains no ~-files
backups=($(find ${MOLK_BUILD} -name "*~"))
if test ${#backups[@]} -gt 0; then
  molk_message "${#backups[@]} ~-backup files found"
  result=$(($result + ${#backups[@]}))
fi
unset backups

if test $result -gt 0; then
  echo -e "check-build failed: ${result} errors found" >&2
  exit 42
fi
