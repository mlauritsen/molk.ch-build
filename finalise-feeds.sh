#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

feeds=($(cd ${MOLK_CONTENT} && find . -type f -name "feed.xml"))

# validation
if test ${#feeds[*]} == 0; then
  echo "No feeds found in ${MOLK_CONTENT}"
  exit 0
fi

echo -n "Finalizing feeds"

# iterate over the feeds
for feed in "${feeds[@]}"; do
  # if the filename starts with "./", remove it
  feed=${feed#./}

  # ignore feeds containing MOLK-TODO
  if ! grep -q MOLK-TODO ${MOLK_CONTENT}/${feed}; then

    # finalize feeds containing "<?molk "
    if grep -q "<?molk " ${MOLK_CONTENT}/${feed}; then
      if ! ${MOLK_UTILS}/finalise-feed.sh ${MOLK_CONTENT}/${feed}; then
        echo -e "\n\"${feed}\": Error while finalizing" >&2
        exit 42
      fi
    fi
    # feed was handled succesfully
    echo -n "."
  else
    # feed was ignored (ignored feeds are listed by name in ()'s)
    echo -n "(${feed})"
  fi
done
