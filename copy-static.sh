#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# copy static resources (non-HTML files in $MOLK_CONTENT) to $MOLK_DEST
files=($(find "${MOLK_CONTENT}" | grep -v "\.html$" | grep -v "~$"))
if test $((${#files[@]})) -le 1; then
  echo "No static files found..."
  exit 0
fi
echo -n "Copying ${#files[@]} static files: "

# copy all files, including unwanted ones
cp -r ${MOLK_CONTENT}/* "${MOLK_DEST}"

# also copy dot-files from the root dir
if ls ${MOLK_CONTENT}/.[^.]* 1>/dev/null 2>&1; then
    cp ${MOLK_CONTENT}/.[^.]* "${MOLK_DEST}"
fi

# remove .html files, which will be processed separately
${MOLK_UTILS}/writable.sh ${MOLK_DEST}
html_files=($(find "${MOLK_DEST}" -name "*.html"))
rm -r ${html_files[@]}

# remove emacs backup files
backup_files=($(find "${MOLK_DEST}" -name "*~"))
if test ${#backup_files[@]} -ge 1; then
    echo -n "(deleting ${#backup_files[@]} emacs backup files)"
    rm -r ${backup_files[@]}
fi

# remove links
find "${MOLK_DEST}" -type l -exec unlink \{} \;
