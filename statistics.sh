#!/bin/bash
# Copyright 2009, 2010 Morten Lauritsen Khodabocus, info@molk.ch
# Licensed under the GPL: http://molk.ch/licence.txt
source "${MOLK_UTILS}/core-utils.sh"

# site statistics
echo -e "\nSite statistics:"

echo -n "  running in "
if test "$(${MOLK_UTILS}/is-online.sh)" == "true"; then
  echo -n "online"
else
  echo -n "offline"
fi
echo " mode."

html=$(find "${MOLK_CONTENT}" -name "*.html" | wc -l)
printf "  HTML files:       %3s\n" "${html}"
unset html

pages=$(echo | grep -l "<?molk" $(find "${MOLK_CONTENT}" -name "*.html") | wc -l)
printf "  MOLK pages:       %3s\n" "${pages}"
unset pages

feeds=$(find "${MOLK_CONTENT}" -name feed.xml | wc -l)
printf "  feeds:            %3s\n" "${feeds}"
unset feeds

js=$(find "${MOLK_SRC}" -name "*.js" | wc -l)
printf "  JavaScript files: %3s\n" "${js}"
unset js

css=$(find "${MOLK_SRC}" -name "*.css" | wc -l)
printf "  CSS files:        %3s\n" "${css}"
unset css

todos=$(echo | grep -i "molk-todo" $(find "${MOLK_SRC}" | egrep "(\.html$|\.css$|\.js$|\.xml$)") | wc -l)
printf "  todos:            %3s\n" "${todos}"
unset todos
